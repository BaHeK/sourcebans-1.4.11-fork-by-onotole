<?php

// ###################[ Servers ]##################################################################
// Make sure they are allowed here oO
CheckAdminAccess( ADMIN_OWNER|ADMIN_LIST_SERVERS|ADMIN_ADD_SERVER|ADMIN_EDIT_SERVERS|ADMIN_DELETE_SERVERS );

$sbViewParams['breadCrumbs'][] = t('breadcrumbs', 'Server management');

if(!isset($_GET['o'])) {
    // ====================[ ADMIN SIDE MENU START ] ===================
    $serverTabMenu = new CTabsMenu();
    if ($userbank->HasAccess(ADMIN_OWNER | ADMIN_LIST_SERVERS)) {
        $serverTabMenu->addMenuItem(t('admin/leftmenu', "List servers"), 0);
    }
    if ($userbank->HasAccess(ADMIN_OWNER | ADMIN_ADD_SERVER)) {
        $serverTabMenu->addMenuItem(t('admin/leftmenu', "Add new server"), 1);
    }
    $serverTabMenu->outputMenu();
    // ====================[ ADMIN SIDE MENU END ] ===================

    include __DIR__ . DIRECTORY_SEPARATOR . "admin.servers.php";
    $sbViewParams['pageTitle'] = t('admin', 'Server Management');
    $sbViewParams['title'] = t('admin', 'Server Management');
} elseif($_GET['o'] == 'edit') {
    $serverTabMenu = new CTabsMenu();
    $serverTabMenu->addMenuItem(t('admin/leftmenu', "Back"), 0,"", "javascript:history.go(-1);", true);
    $serverTabMenu->outputMenu();		

    include __DIR__ . DIRECTORY_SEPARATOR . "admin.edit.server.php";
    $sbViewParams['pageTitle'] = t('admin', 'Edit Server');
    $sbViewParams['title'] = t('admin', 'Edit Server');
} elseif($_GET['o'] == 'rcon') {
    $serverTabMenu = new CTabsMenu();
    $serverTabMenu->addMenuItem(t('admin/leftmenu', "Back"), 0,"", "javascript:history.go(-1);", true);
    $serverTabMenu->outputMenu();

    include __DIR__ . DIRECTORY_SEPARATOR . "admin.rcon.php";
    $sbViewParams['pageTitle'] = t('admin', 'Server RCON');
    $sbViewParams['title'] = t('admin', 'Server RCON');
} elseif($_GET['o'] == 'dbsetup') {
    $serverTabMenu = new CTabsMenu();
    $serverTabMenu->addMenuItem(t('admin/leftmenu', "Back"), 0,"", "javascript:history.go(-1);", true);
    $serverTabMenu->outputMenu();

    include __DIR__ . DIRECTORY_SEPARATOR . "admin.servers.db.php";
    $sbViewParams['pageTitle'] = t('admin', 'Database Config');
    $sbViewParams['title'] = t('admin', 'Database Config');
} elseif($_GET['o'] == 'admincheck') {
    $serverTabMenu = new CTabsMenu();
    $serverTabMenu->addMenuItem(t('admin/leftmenu', "Back"), 0,"", "javascript:history.go(-1);", true);
    $serverTabMenu->outputMenu();

    include __DIR__ . DIRECTORY_SEPARATOR . "admin.srvadmins.php";
    $sbViewParams['pageTitle'] = t('admin', 'Server Admins');
    $sbViewParams['title'] = t('admin', 'Server Admins');
}