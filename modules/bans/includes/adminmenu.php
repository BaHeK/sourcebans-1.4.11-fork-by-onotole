<?php
global $userbank;
return array(
    array(
        'label' => t('modules/bans/main', 'Bans'),
        'url' => 'index.php?m=bans&p=admin',
        'visible' => $userbank->HasAccess(ADMIN_OWNER|ADMIN_ADD_BAN|ADMIN_EDIT_OWN_BANS|ADMIN_EDIT_GROUP_BANS|ADMIN_EDIT_ALL_BANS|ADMIN_BAN_PROTESTS|ADMIN_BAN_SUBMISSIONS)
    ),
);