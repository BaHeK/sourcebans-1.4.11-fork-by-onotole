<?php
/**
 * =============================================================================
 * Submit ban
 *
 * @author SteamFriends Development Team
 * @version 1.0.0
 * @copyright SourceBans (C)2007 SteamFriends.com.  All rights reserved.
 * @package SourceBans
 * @link http://www.sourcebans.net
 *
 * @version $Id: page.submit.php 253 2009-03-31 12:51:01Z peace-maker $
 * =============================================================================
 */
global $userbank, $theme;
if(!defined("IN_SB")){echo "You should not be here. Only follow links!";die();}
if($GLOBALS['config']['config.enablesubmit']!="1")
{
	CreateRedBox(
        t('modules/bans/submit', 'Error'),
        t('modules/bans/submit', 'This page is disabled. You should not be here.')
   );
	PageDie();
}
if (!isset($_POST['subban']) || $_POST['subban'] != 1) {
	$SteamID = "";
	$BanIP = "";
	$PlayerName = "";
	$BanReason = "";
	$SubmitterName = "";
	$Email = "";
	$SID = -1;
} else {
	$SteamID = trim(htmlspecialchars($_POST['SteamID']));
	$BanIP = trim(htmlspecialchars($_POST['BanIP']));
	$PlayerName = htmlspecialchars($_POST['PlayerName']);
	$BanReason = htmlspecialchars($_POST['BanReason']);
	$SubmitterName = htmlspecialchars($_POST['SubmitName']);
	$Email = trim(htmlspecialchars($_POST['EmailAddr']));
	$SID = (int)$_POST['server'];
	$validsubmit = true;
	$errors = "";
	if((strlen($SteamID)!=0 && $SteamID != "STEAM_0:") && !validate_steam($SteamID)) {
		$errors .= '* '.t('modules/bans/submit', 'Please type a valid STEAM ID.').'<br>';
		$validsubmit = false;
	}
	if(strlen($BanIP)!=0 && !validate_ip($BanIP)) {
		$errors .= '* '.t('modules/bans/submit', 'Please type a valid IP-address.').'<br>';
		$validsubmit = false;
	}
	if (strlen($PlayerName) == 0) {
		$errors .= '* '.t('modules/bans/submit', 'You must include a player name').'<br>';
		$validsubmit = false;
	}
	if (strlen($BanReason) == 0) {
		$errors .= '* '.t('modules/bans/submit', 'You must include comments').'<br>';
		$validsubmit = false;
	}
	if (!check_email($Email)) {
		$errors .= '* '.t('modules/bans/submit', 'You must include a valid email address').'<br>';
		$validsubmit = false;
	}
	if($SID == -1) {
		$errors .= '* Please select a server.<br>';
		$validsubmit = false;
	}
	if(!empty($_FILES['demo_file']['name'])) {
        $exts = array("zip", "rar", "dem", "7z", "bz2", "gz");
		if(!CheckExt($_FILES['demo_file']['name'], $exts))
		{
			$errors .= '* '.t('modules/bans/submit', 'A demo can only be a [[validexts]] filetype.', array('[[validexts]]' => implode(', ', $exts))).'<br>';
			$validsubmit = false;
		}
	}
	$checkres = $GLOBALS['db']->Execute("SELECT length FROM ".DB_PREFIX."_bans WHERE authid = ? AND RemoveType IS NULL", array($SteamID));
	$numcheck = $checkres->RecordCount();
	if($numcheck == 1 && $checkres->fields['length'] == 0)
	{
		$errors .= '* '.t('modules/bans/submit', 'The player is already banned permanent.').'<br>';
		$validsubmit = false;
	}


	if (!$validsubmit) {
        CreateRedBox(t('modules/bans/submit', 'Error'), $errors);
    }

    if ($validsubmit) {
		$filename = md5($SteamID.time());
		//echo SB_DEMOS.$filename;
		$demo = move_uploaded_file($_FILES['demo_file']['tmp_name'],SB_DEMOS.$filename);
		if($demo || empty($_FILES['demo_file']['name'])) {
			if($SID!=0) {
                $res = $GLOBALS['db']->GetRow("SELECT ip, port FROM ".DB_PREFIX."_servers WHERE sid = $SID");
                
                $mailserver = t("mail/submit", "Error Connecting ([[serverip]]:[[serverport]])", array(
                    '[[serverip]]' => $res[0],
                    '[[serverport]]' => $res[1],
                ));
				
                $query = QueryInstance($res[0],$res[1]);
                if($query) {
                    $info = $query->GetInfo();
                    if (!empty($info['HostName'])) {
                        $mailserver = t("mail/submit", "[[hostname]] ([[serverip]]:[[serverport]])", array(
                            '[[hostname]]' => $info['HostName'],
                            '[[serverip]]' => $res[0],
                            '[[serverport]]' => $res[1],
                        ));
                    }
                }
				$modid = $GLOBALS['db']->GetRow(
                    "SELECT
                        m.mid
                    FROM
                        `".DB_PREFIX."_servers` as s
                    LEFT JOIN 
                        `".DB_PREFIX."_mods` as m
                    ON
                        m.mid = s.modid
                    WHERE
                        s.sid = '".$SID."';"
                );
			} else {
				$mailserver = t('modules/bans/submit', 'Other server');
				$modid[0] = 0;
			}
			if ($SteamID == "STEAM_0:") {
                $SteamID = "";
            }
            $pre = $GLOBALS['db']->Prepare("INSERT INTO ".DB_PREFIX."_submissions(submitted,SteamId,name,email,ModID,reason,ip,subname,sip,archiv,server) VALUES (UNIX_TIMESTAMP(),?,?,?,?,?,?,?,?,0,?)");
			$GLOBALS['db']->Execute($pre,array(
                $SteamID,
                $PlayerName,
                $Email,
                $modid[0],
                $BanReason,
                $_SERVER['REMOTE_ADDR'],
                $SubmitterName,
                $BanIP,
                $SID
            ));
			$subid = (int)$GLOBALS['db']->Insert_ID();

			if (!empty($_FILES['demo_file']['name'])) {
                $GLOBALS['db']->Execute("INSERT INTO " . DB_PREFIX . "_demos(demid,demtype,filename,origname) VALUES (?, 'S', ?, ?)", array(
                    $subid,
                    $filename,
                    $_FILES['demo_file']['name']
                ));
            }
            //$SteamID = "";
			//$BanIP = "";
			//$PlayerName = "";
			//$BanReason = "";
			//$SubmitterName = "";
			//$Email = "";
			//$SID = -1;

			$admins = $userbank->GetAllAdmins();
			$requri = substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], ".php")-5);
            
            $adminMails = array();
			foreach($admins AS $admin) {
				$adminMails[$admin['email']] = $admin['user'];
            }
            
            if ($userbank->HasAccess(ADMIN_OWNER | ADMIN_BAN_SUBMISSIONS, $admin['aid']) && $userbank->HasAccess(ADMIN_NOTIFY_SUB, $admin['aid'])) {
                if(empty($_FILES['demo_file']['name'])) {
                    $demolink = t('modules/bans/submit', 'no');
                } else {
                    $demolink = t('modules/bans/submit', 'yes: ([[demolink]])', array(
                        '[[demolink]]' => 'http://' . $_SERVER['HTTP_HOST'] . $requri . 'getdemo.php?type=S&id='.$subid
                    ));
                }

                $message = t('modules/bans/submit', 'Hello [[username]]', array('[[username]]' => $admin['user'])) . PHP_EOL;
                $message .= t('modules/bans/submit', 'A new ban submission has been posted on your SourceBans page.') . PHP_EOL;
                $message .= t(
                    'mail/submit',
                    "Player: [[player]] ([[steamid]])\nDemo: [[demo]]\nServer: [[server]]\nReason: [[reason]]",
                    array(
                        '[[player]]' => $PlayerName,
                        '[[steamid]]' => $SteamID,
                        '[[demo]]' => $demolink,
                        '[[server]]' => $mailserver,
                        '[[reason]]' => $UnbanReason,
                    )
                ) . PHP_EOL;
                $message .= t('modules/bans/submit', 'Click the link below to view the current ban submissions.') . PHP_EOL;
                $message .= "http://" . $_SERVER['HTTP_HOST'] . $requri . "index.php?p=admin&c=bans#^2";

                $mailSubject = t('modules/bans/submit', '[SourceBans] Ban Submission Added');

                $userbank->sendMail($adminMails, $mailSubject, $message);
            }
            
			CreateGreenBox(
                t('modules/bans/submit', 'Successful'),
                t('modules/bans/submit', 'Your submission has been added into the database, and will be reviewed by one of our admins')
            );
		}
		else
		{
			CreateRedBox(
                t('modules/bans/submit', 'Error'),
                t('modules/bans/submit', 'There was an error uploading your demo to the server. Please try again later.')
            );
			new CSystemLog(
                "e",
                t('log', 'Demo Upload Failed'),
                t('log', 'A demo failed to upload for a submission from ([[email]])', array(
                    '[[email]]' => $Email
                ))
            );
		}
	}
}

//$mod_list = $GLOBALS['db']->GetAssoc("SELECT mid,name FROM ".DB_PREFIX."_mods WHERE `mid` > 0 AND `enabled`= 1 ORDER BY mid ");

//serverlist
$server_list = $GLOBALS['db']->Execute("SELECT sid, ip, port FROM `" . DB_PREFIX . "_servers` WHERE enabled = 1 ORDER BY modid, sid");
$servers = array();
while (!$server_list->EOF) {
	$info = array();
	$query = QueryInstance($server_list->fields[1],$server_list->fields[2]);
    $info['hostname'] = t('modules/bans/submit', 'Error Connecting ([[serverip]]:[[serverport]])', array(
        '[[serverip]]' => $server_list->fields[1],
        '[[serverport]]' => $server_list->fields[2]
    ));
    if($query) {
        $info2 = $query->GetInfo();
        if (!empty($info2['HostName'])) {
            $info['hostname'] = $info2['HostName'];
        }
    }
	$info['sid'] = $server_list->fields[0];
	array_push($servers,$info);
	$server_list->MoveNext();
}

$sbViewParams['pageTitle'] = t('modules/bans/submit', 'Submit a ban');
$sbViewParams['title'] =  t('modules/bans/submit', 'Submit a ban');
$sbViewParams['breadCrumbs'][] = t('breadcrumbs', 'Submit a ban');

$theme->assign('STEAMID',		$SteamID==""?"STEAM_0:":$SteamID);
$theme->assign('ban_ip',			$BanIP);
$theme->assign('ban_reason',	$BanReason);
$theme->assign('player_name',	$PlayerName);
$theme->assign('subplayer_name',$SubmitterName);
$theme->assign('player_email',	$Email);
$theme->assign('server_list',		$servers);
$theme->assign('server_selected',	$SID);

$theme->display('page_submitban.tpl');
