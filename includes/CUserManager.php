<?php
/**
 * =============================================================================
 * Main user handler
 * 
 * @author SteamFriends Development Team
 * @version 1.0.0
 * @copyright SourceBans (C)2007 SteamFriends.com.  All rights reserved.
 * @package SourceBans
 * @link http://www.sourcebans.net
 * 
 * @version $Id: CUserManager.php 182 2008-12-18 19:12:19Z smithxxl $
 * =============================================================================
 */

class CUserManager
{
	private $aid = -1;
	private $admins = array();
	
    
    
	/**
	 * Class constructor
	 *
	 * @param $aid the current user's aid
	 * @param $password the current user's password
	 * @return noreturn.
	 */
	public function __construct($aid, $password)
	{
		if ($this->CheckLogin($password, $aid)) {
            $this->aid = $aid;
            $this->GetUserArray($aid);
        } else {
            $this->aid = -1;
        }
    }
	
	
	/**
	 * Gets all user details from the database, saves them into
	 * the admin array 'cache', and then returns the array
	 *
	 * @param $aid the ID of admin to get info for.
	 * @return array.
	 */
	public function GetUserArray($aid=-2)
	{
		if ($aid == -2) {
            $aid = $this->aid;
        }
        // Invalid aid
		if ($aid < 0 || empty($aid)) {
            return 0;
        }

        $aid = intval($aid);
		// We already got the data from the DB, and its saved in the manager
		if (isset($this->admins[$aid]) && !empty($this->admins[$aid])) {
            return $this->admins[$aid];
        }
        // Not in the manager, so we need to get them from DB
        $sql = "SELECT 
                    `adm`.`user` `user`,
                    `adm`.`authid` `authid`,
                    `adm`.`password` `password`,
                    `adm`.`gid` `gid`,
                    `adm`.`email` `email`,
                    `adm`.`validate` `validate`,
                    `adm`.`extraflags` `extraflags`, 
					`adm`.`immunity` `admimmunity`,
                    `adm`.`srv_password` `srv_password`,
                    `adm`.`srv_group` `srv_group`,
                    `adm`.`srv_flags` `srv_flags`,
                    `adm`.`lastvisit` `lastvisit`,
                    `adm`.`expired` `expired`,
                    `sg`.`immunity` `sgimmunity`,
                    `sg`.`flags` `sgflags`,
                    `wg`.`flags` `wgflags`,
                    `wg`.`name` `wgname`
				FROM 
                    `" . DB_PREFIX . "_admins` AS `adm`
				LEFT JOIN 
                    `" . DB_PREFIX . "_groups` AS `wg`
                ON 
                    `adm`.`gid` = `wg`.`gid`
				LEFT JOIN 
                    `" . DB_PREFIX . "_srvgroups` AS `sg`
                ON 
                    `adm`.`srv_group` = `sg`.`name`
				WHERE
                    `adm`.`aid` = {$aid}";
		$res = $GLOBALS['db']->GetRow($sql);
		
		if (!$res) {
            return 0;
        }  // ohnoes some type of db error
		
		$user = array();	
		//$user['user'] = stripslashes($res[0]);
		$user['aid'] = $aid; //immediately obvious
		$user['user'] = $res['user'];
		$user['authid'] = $res['authid'];	
		$user['password'] = $res['password'];
		$user['gid'] = $res['gid'];
		$user['email'] = $res['email'];
		$user['validate'] = $res['validate'];
		$user['extraflags'] = (intval($res['extraflags']) | intval($res['wgflags']));

		if (intval($res['admimmunity']) > intval($res['sgimmunity'])) {
            $user['srv_immunity'] = intval($res['admimmunity']);
        } else {
            $user['srv_immunity'] = intval($res['sgimmunity']);
        }

        $user['srv_password'] = $res['srv_password'];
		$user['srv_groups'] = $res['srv_group'];
		$user['srv_flags'] = $res['srv_flags'] . $res['sgflags'];
		$user['group_name'] = $res['wgname'];
		$user['lastvisit'] = $res['lastvisit'];
        
		$user['expired'] = $res['expired'];
        
        $time = time();
        
        if($res['expired'] == 0) {
            $user['expDate'] = t('admin', 'Never');
            $user['daysLeft'] = t('admin', 'Forever');
        } else {
            if($res['expired'] > $time) {
                $secondsLeft = $res['expired'] - $time;
                $user['daysLeft'] = SecondsToString($secondsLeft, true, false);
                $user['expDate'] = date('d.m.Y', $res['expired']);
            } else {
                $user['expDate'] = t('admin', 'Expired');
                $user['daysLeft'] = t('admin', 'Expired');
            }
        }
        
		$this->admins[$aid] = $user;
		return $user;
	}

	
	/**
	 * Will check to see if an admin has any of the flags given
	 *
	 * @param $flags The flags to check for.
	 * @param $aid The user to check flags for.
	 * @return boolean.
	 */
	public function HasAccess($flags, $aid=-2)
	{
		if ($aid == -2) {
            $aid = $this->aid;
        }

        if (empty($flags) || $aid <= 0) {
            return false;
        }

        $aid = (int)$aid;
		if(is_numeric($flags)) {
			if (!isset($this->admins[$aid])) {
                $this->GetUserArray($aid);
            }
            return ($this->admins[$aid]['extraflags'] & $flags) != 0 ? true : false;
		} else  {
			if (!isset($this->admins[$aid])) {
                $this->GetUserArray($aid);
            }
            for($i=0;$i<strlen($this->admins[$aid]['srv_flags']);$i++) {
				for($a=0;$a<strlen($flags);$a++) {
					if (strstr($this->admins[$aid]['srv_flags'][$i], $flags[$a])) {
                        return true;
                    }
                }
			}
		}
	}
	
	
	/**
	 * Gets a 'property' from the user array eg. 'authid'
	 *
	 * @param $aid the ID of admin to get info for.
	 * @return mixed.
	 */
	public function GetProperty($name, $aid=-2)
	{
		if ($aid == -2) {
            $aid = $this->aid;
        }
        if (empty($name) || $aid < 0) {
            return false;
        }
        $aid = (int)$aid;	
		if (!isset($this->admins[$aid])) {
            $this->GetUserArray($aid);
        }

        return $this->admins[$aid][$name];
	}
	

	/**
	 * Will test the user's login stuff to check if they havnt changed their 
	 * cookies or something along those lines.
	 *
	 * @param $password The admins password.
	 * @param $aid the admins aid
	 * @return boolean.
	 */
	public function CheckLogin($password, $aid)
	{
		$aid = (int)$aid;

		if (empty($password)) {
            return false;
        }
        if (!isset($this->admins[$aid])) {
            $this->GetUserArray($aid);
        }

        if ($password == $this->admins[$aid]['password']) {
            $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_admins` SET `lastvisit` = UNIX_TIMESTAMP() WHERE `aid` = '$aid'");
            return true;
        } else {
            return false;
        }
    }
	
	
	public function login($aid, $password, $save = true)
	{
	    if($this->CheckLogin($this->encrypt_password($password), $aid)) {
	        if($save) {
	            //Sets cookies
	            setcookie("aid", $aid, time()+LOGIN_COOKIE_LIFETIME);
	            setcookie("password", $this->encrypt_password($password), time()+LOGIN_COOKIE_LIFETIME);
	            setcookie("user", isset($_SESSION['user']['user'])?$_SESSION['user']['user']:null, time()+LOGIN_COOKIE_LIFETIME);
	        } else {
	        	setcookie("aid", $aid);
	            setcookie("password", $this->encrypt_password($password));
	            setcookie("user", $_SESSION['user']['user']);
	        }
	        return true;
	    } else {
	        return false;
	    }
	}
	
	
	
	/**
	 * Encrypts a password.
	 *
	 * @param $password password to encrypt.
	 * @return string.
	 */
	public function encrypt_password($password, $salt=SB_SALT)
	{
		return sha1(sha1($salt . $password));
	}
	
	public function is_logged_in()
	{
		if ($this->aid != -1) {
            return true;
        } else {
            return false;
        }
    }
	
	
	public function is_admin($aid=-2)
	{
		if ($aid == -2) {
            $aid = $this->aid;
        }

        if ($this->HasAccess(ALL_WEB, $aid)) {
            return true;
        } else {
            return false;
        }
    }
	
	
	public function GetAid()
	{
		return $this->aid;
	}
	
	
	public function GetAllAdmins()
	{
		$res = $GLOBALS['db']->GetAll("SELECT aid FROM " . DB_PREFIX . "_admins");
		foreach ($res AS $admin) {
            $this->GetUserArray($admin['aid']);
        }
        return $this->admins;
	}
	
	
	public function GetAdmin($aid=-2)
	{
		if ($aid == -2) {
            $aid = $this->aid;
        }
        if ($aid < 0) {
            return false;
        }

        $aid = (int)$aid;
		
		if (!isset($this->admins[$aid])) {
            $this->GetUserArray($aid);
        }
        return $this->admins[$aid];
	}
	
	
	public function AddAdmin(
        $name,
        $steam,
        $password,
        $email,
        $web_group,
        $web_flags,
        $srv_group,
        $srv_flags,
        $immunity,
        $srv_password
    )
	{
        $sql = "INSERT INTO 
                ".DB_PREFIX."_admins (
                    `user`,
                    `authid`,
                    `password`,
                    `gid`,
                    `email`,
                    `extraflags`,
                    `immunity`,
                    `srv_group`,
                    `srv_flags`,
                    `srv_password`
                )
                VALUES (?,?,?,?,?,?,?,?,?,?)";
		$add_admin = $GLOBALS['db']->Prepare($sql);
		$GLOBALS['db']->Execute($add_admin, array(
            $name,
            $steam,
            $this->encrypt_password($password),
            $web_group,
            $email,
            $web_flags,
            $immunity,
            $srv_group,
            $srv_flags,
            $srv_password
        ));
		return ($add_admin) ? (int)$GLOBALS['db']->Insert_ID() : -1;
	}
    
    public function sendMail($to, $subject, $message) {
        require_once INCLUDES_PATH . 'PHPMailer'.DIRECTORY_SEPARATOR.'PHPMailerAutoload.php';
        $mail = new PHPMailer;
        $mail->CharSet = 'UTF-8';
        $mail->Encoding = 'base64';
        $mail->Timeout = 10;
        $fromName = !empty($GLOBALS['config']['config.email.from.name']) ? $GLOBALS['config']['config.email.from.name'] : 'SourceBans';
        $fromEmail = !empty($GLOBALS['config']['config.email.from.email'])
                ?
            $GLOBALS['config']['config.email.from.email']
                :
            'noreply@' . filter_input(INPUT_SERVER, 'SERVER_NAME');
        
        $mail->setFrom($fromEmail, $fromName);
        
        if(isset($GLOBALS['config']['config.email.transport']) && $GLOBALS['config']['config.email.transport'] == 'smtp') {
            $mail->isSMTP();
            $mail->Host = $GLOBALS['config']['config.email.smtp.host'];
            $mail->Port = !empty($GLOBALS['config']['config.email.smtp.port']) ? intval($GLOBALS['config']['config.email.smtp.port']) : 25;
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = !empty($GLOBALS['config']['config.email.smtp.secure']) ? intval($GLOBALS['config']['config.email.smtp.secure']) : '';
            if (defined('DEVELOPER_MODE')) {
                $mail->SMTPDebug = 3;
            }
            $mail->Username = $GLOBALS['config']['config.email.smtp.login'];
            $mail->Password = $GLOBALS['config']['config.email.smtp.password'];
            
            //exit('<pre>'.print_r($mail, true).'</pre>');
        } else {
            $mail->isMail();
        }
        
        $emailsForLog = array();
        
        if(is_array($to)) {
            $pass = 0;
            foreach($to as $email => $name) {
                if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $email = $name;
                    $name = '';
                }
                if($pass === 0) {
                    $mail->addAddress($email, $name);
                } else {
                    $mail->addReplyTo($email, $name);
                }
                $emailsForLog[] = $email;
                $pass++;
            }
        } else {
            $mail->addAddress($to);
            $emailsForLog = $to;
        }
        
        $mail->Subject = $subject;
        $mail->Body = $message;

        $logmsg = t(
            'log',
            '<b>Transport:</b> [[transport]]<br /><b>Destination:</b> [[addresses]]<br /><b>Subject:</b> [[subject]]<br /><b>Message:</b> [[message]]<br /><b>Error:</b> [[error]]',
            array(
                '[[addresses]]' => is_array($emailsForLog) ? implode(', ', $emailsForLog) : $emailsForLog,
                '[[subject]]' => $subject,
                '[[message]]' => $message,
                '[[transport]]' => $GLOBALS['config']['config.email.transport'],
                '[[error]]' => !empty($mail->ErrorInfo) ? $mail->ErrorInfo : ''
            )
        );
        $log = new CSystemLog(
            'm',
            t('log', 'Email sent'),
            $logmsg
        );
        
        if (!$mail->send() || !empty($mail->ErrorInfo)) {
            
            $logmsg = t(
                'log',
                'An error occurred while sending an email to the address "[[address]]". Error: [[error]]',
                array(
                    '[[address]]' => is_array($emailsForLog) ? implode(', ', $emailsForLog) : $emailsForLog,
                    '[[error]]' => $mail->ErrorInfo
                )
            );
            $log = new CSystemLog(
                'e',
                t('log', 'Email was not sent'),
                $logmsg
            );
            return false;
        }
        return true;
    }
}
