<?php

return array(
    t('banreasons', 'Hacking') => array(
        t('banreasons', 'Aimbot'),
        t('banreasons', 'Antirecoil'),
        t('banreasons', 'Wallhack'),
        t('banreasons', 'Spinhack'),
        t('banreasons', 'Multi-Hack'),
        t('banreasons', 'No Smoke'),
        t('banreasons', 'No Flash'),
    ),
    t('banreasons', 'Behavior') => array(
        t('banreasons', 'Team Killing'),
        t('banreasons', 'Team Flashing'),
        t('banreasons', 'Spamming Mic/Chat'),
        t('banreasons', 'Inappropriate Spray'),
        t('banreasons', 'Inappropriate Language'),
        t('banreasons', 'Inappropriate Name'),
        t('banreasons', 'Ignoring Admins'),
        t('banreasons', 'Team Stacking'),
    ),
);
