<?php
/**
 * =============================================================================
 * Return the page that we want
 * 
 * @author SteamFriends Development Team
 * @version 1.0.0
 * @copyright SourceBans (C)2007 SteamFriends.com.  All rights reserved.
 * @package SourceBans
 * @link http://www.sourcebans.net
 * 
 * @version $Id: page-builder.php 146 2008-09-08 19:55:00Z peace-maker $
 * =============================================================================
 */

session_start();

$sbViewParams = array();

$p = filter_input(INPUT_GET, 'p');
$module = filter_input(INPUT_GET, 'm');

if($module) {
    $moduleClassName = ucfirst($module) . 'Module';
    $moduleClassFile = SB_MODULES . $module . DIRECTORY_SEPARATOR . $moduleClassName . '.php';
    if(!is_file($moduleClassFile)) {
        $content = pageNotFound();
    } else {
        $xajax->setRequestURI('./index.php?m=' . $module);
        $theme->template_dir .= DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $module;
        include $moduleClassFile;
        $moduleClass = new $moduleClassName;
        $content = $moduleClass->getContent($p);
        $theme->template_dir = SB_THEMES . SB_THEME;
        if(!empty($moduleClass->breadCrumbs)) {
            $sbViewParams['breadCrumbs'] = $moduleClass->breadCrumbs;
        }
        if(!empty($moduleClass->pageTitle)) {
            $sbViewParams['pageTitle'] = $moduleClass->pageTitle;
        }
        if(!empty($moduleClass->title)) {
            $sbViewParams['title'] = $moduleClass->title;
        }
    }
} elseif($p) {
    if($p == 'logout') {
        logout();
        Header("Location: index.php");
        exit;
    }
    $page = TEMPLATES_PATH . "page.{$p}.php";
} else {
    $page = getMainPagePath();
}

if(!$module) {
    if(!is_file($page)) {
        $content = pageNotFound();
    } elseif (!empty($page)) {
        ob_start();
        include $page;
        $content = ob_get_clean();
    } else {
        $content = '';
    }
}

if(!empty($sbViewParams['title'])) {
    RewritePageTitle($sbViewParams['title']);
}
$xajax->processRequests();
include TEMPLATES_PATH . "header.php";
BuildPageTabs();
BuildSubMenu();
BuildContHeader();
BuildBreadcrumbs(!empty($sbViewParams['breadCrumbs']) ? $sbViewParams['breadCrumbs'] : false);
echo $content;
include_once(TEMPLATES_PATH . 'footer.php');
