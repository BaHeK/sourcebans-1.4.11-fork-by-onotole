<?php

global $userbank;

return array(
    array(
        'label'     => t('adminmenu', 'Admins'),
        'url'       => 'index.php?p=admin&amp;c=admins',
        'visible'   => $userbank->HasAccess(ADMIN_OWNER|ADMIN_LIST_ADMINS|ADMIN_ADD_ADMINS|ADMIN_EDIT_ADMINS|ADMIN_DELETE_ADMINS)
    ),
    array(
        'label'   => t('adminmenu', 'Groups'),
        'url'     => 'index.php?p=admin&amp;c=groups',
        'visible' => $userbank->HasAccess(ADMIN_OWNER|ADMIN_LIST_GROUPS|ADMIN_ADD_GROUP|ADMIN_EDIT_GROUPS|ADMIN_DELETE_GROUPS)
    ),
    array(
        'label'     => t('adminmenu', 'Settings'),
        'url'       => 'index.php?p=admin&amp;c=settings',
        'visible'   => $userbank->HasAccess(ADMIN_OWNER | ADMIN_WEB_SETTINGS)
    ),
);