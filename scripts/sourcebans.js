/**
 * =============================================================================
 * JavaScript functions, and AJAX calls
 * 
 * @author SteamFriends Development Team
 * @version 1.0.0
 * @copyright SourceBans (C)2007 SteamFriends.com.  All rights reserved.
 * @package SourceBans
 * @link http://www.sourcebans.net
 * 
 * @version $Id: sourcebans.js 289 2009-07-13 18:35:11Z peace-maker $
 * =============================================================================
 */


var ADMIN_LIST_ADMINS = 	(1<<0);

var ADMIN_ADD_ADMINS = 		(1<<1);

var ADMIN_EDIT_ADMINS = 	(1<<2);
var ADMIN_DELETE_ADMINS = 	(1<<3);

var ADMIN_LIST_SERVERS = 	(1<<4);
var ADMIN_ADD_SERVER = 		(1<<5);
var ADMIN_EDIT_SERVERS = 	(1<<6);
var ADMIN_DELETE_SERVERS = 	(1<<7);

var ADMIN_ADD_BAN = 		(1<<8);
var ADMIN_EDIT_OWN_BANS = 	(1<<10);
var ADMIN_EDIT_GROUP_BANS = (1<<11);
var ADMIN_EDIT_ALL_BANS = 	(1<<12);
var ADMIN_BAN_PROTESTS = 	(1<<13);
var ADMIN_BAN_SUBMISSIONS = (1<<14);
var ADMIN_DELETE_BAN = 		(1<<25);
var ADMIN_UNBAN = 			(1<<26);
var ADMIN_BAN_IMPORT =		(1<<27);
var ADMIN_UNBAN_OWN_BANS =	(1<<30);
var ADMIN_UNBAN_GROUP_BANS =(1<<31);

var ADMIN_NOTIFY_SUB =		(1<<28);
var ADMIN_NOTIFY_PROTEST =	(1<<29);

var ADMIN_LIST_GROUPS = 	(1<<15);
var ADMIN_ADD_GROUP = 		(1<<16);
var ADMIN_EDIT_GROUPS = 	(1<<17);
var ADMIN_DELETE_GROUPS = 	(1<<18);

var ADMIN_WEB_SETTINGS = 	(1<<19);

var ADMIN_LIST_MODS = 		(1<<20);
var ADMIN_ADD_MODS = 		(1<<21);
var ADMIN_EDIT_MODS = 		(1<<22);
var ADMIN_DELETE_MODS = 	(1<<23);

var ADMIN_OWNER = 			(1<<24);

var accordion;

// drag and drop function, make the dialog window movable!
var ns4=document.layers;
var ie4=document.all;
var ns6=document.getElementById&&!document.all;

//NS 4
var dragswitch=0;
var nsx;
var nsy;
var nstemp;

function Swap2ndPane(id, ttype)
{
	var i = 0;
	var i2 = 0;
	if(document.getElementById("utab-" + ttype + id))
	{
		while($(document.getElementById(ttype + i)))
		{
			$(document.getElementById(ttype + i)).setStyle('display', 'none');
			i++;
		}
		while(i2 < 50)
		{
			if($("utab-" + ttype + i2))
			{
				$("utab-" + ttype + i2).removeClass('active');
				$("utab-" + ttype + i2).addClass('nonactive');
			}
			i2++;
		}
		$(document.getElementById("utab-" + ttype + id)).addClass('active');
		$(document.getElementById(ttype + id)).setStyle('display', 'block');
	}
}

function SwapPane(id)
{
	var i = 0;
	var i2 = 0;
	if(document.getElementById("tab-" + id))
	{
		while($(document.getElementById(i)))
		{
			$(document.getElementById(i)).setStyle('display', 'none');
			i++;
		}
		while(i2 < 50)
		{
			if($("tab-" + i2))
			{
				$("tab-" + i2).removeClass('active');
			}
			i2++;
		}
		$(document.getElementById("tab-" + id)).addClass('active');
		$(document.getElementById(id)).setStyle('display', 'block');	
	}
}

function InitAccordion(opener, element, container, num)
{
	// IE6 got no window.addEventListener
	if (window.addEventListener) {
		window.addEventListener("load", function () {
				InitAccordion(opener, element, container, num);
		}, false);
	} else {
		window.attachEvent('onload', function () {
				InitAccordion(opener, element, container, num);
		});
	}

	if(num == null)
		num = -1;
	var ExtendedAccordion = Accordion.extend({
	showAll: function() {
		var obj = {};
		 this.previous = -1;
		this.elements.each(function(el, i){
			obj[i] = {};
			this.fireEvent('onActive', [this.togglers[i], el]);
			for (var fx in this.effects) obj[i][fx] = el[this.effects[fx]];
		}, this);
		return this.start(obj);
	},
	hideAll: function() {
		var obj = {};
		 this.previous = -1;
		this.elements.each(function(el, i){
			obj[i] = {};
			this.fireEvent('onBackground', [this.togglers[i], el]);
			for (var fx in this.effects) obj[i][fx] = 0;
		}, this);
		return this.start(obj);
	}
  }); 

	accordion = new ExtendedAccordion(opener, element, {
		opacity: true,
		alwaysHide: true,
		display: num,
		transition:Fx.Transitions.Quart.easeOut,
		onActive: function(toggler, element){
			toggler.setStyle('cursor', 'pointer');
			toggler.setStyle('background-color', '');
		},
	 
		onBackground: function(toggler, element){
			toggler.setStyle('cursor', 'pointer');
			toggler.setStyle('background-color', '');		
		}
	}, $(container));
	accordion.hideAll();
}

function ScrollRcon()
{
	var objDiv = document.getElementById("rcon");
	objDiv.scrollTop = objDiv.scrollHeight;
	//alert(objDiv.scrollTop);
}

function Shrink(id, time, height)
{
	var myEffects = $(document.getElementById(id)).effects({duration: time, transition:Fx.Transitions.Bounce.easeOut});
	myEffects.start({'height': [height]});
}

function FadeElOut(id, time)
{
	var myEffects = $(id).effects({duration: time, transition:Fx.Transitions.Sine.easeOut});
	myEffects.start({'opacity': [0]});
	var d = id;
	setTimeout("$(document.getElementById('" + d + "')).setStyle('display', 'none');$(document.getElementById('" + d + "')).setOpacity(0);", time);
	
	return;
}
function FadeElIn(id, time)
{
	$(document.getElementById(id)).setStyle('display', 'block');
	var myEffects = $(id).effects({duration: time, transition:Fx.Transitions.Sine.easeIn});
	myEffects.start({'opacity': [1]});
	setTimeout("$(document.getElementById('" + id + "')).setOpacity(1);", time);
	return;
}
function FXShow(id)
{
	$(document.getElementById(id)).setStyle('display', 'block');
}
function FXHide(id)
{
	$(document.getElementById(id)).setStyle('display', 'none');
}

function SlideUp(id)
{
	var slider = new Fx.Slide(id);
	slider.slideOut().chain(
						function(){
							$(id).remove();
						}
		);
}

function BoxToSrvMask()
{	
	var string = "";
	if(document.getElementById('s1'))
	{
		if(document.getElementById('s1').checked)
			string += "a";
		if(document.getElementById('s23').checked)
			string +=  "b";
		if(document.getElementById('s2').checked)
			string += "c";
		if(document.getElementById('s3').checked)
			string += "d";
		if(document.getElementById('s4').checked)
			string += "e";
		if(document.getElementById('s5').checked)
			string += "f";
		if(document.getElementById('s6').checked)
			string += "g";
		if(document.getElementById('s7').checked)
			string += "h";
		if(document.getElementById('s8').checked)
			string += "i";
		if(document.getElementById('s9').checked)
			string += "j";
		if(document.getElementById('s10').checked)
			string += "k";
		if(document.getElementById('s11').checked)
			string += "l";
		if(document.getElementById('s12').checked)
			string += "m";
		if(document.getElementById('s13').checked)
			string += "n";
		if(document.getElementById('s17').checked)
			string += "o";
		if(document.getElementById('s18').checked)
			string += "p";
		if(document.getElementById('s19').checked)
			string += "q";
		if(document.getElementById('s20').checked)
			string += "r";
		if(document.getElementById('s21').checked)
			string += "s";
		if(document.getElementById('s22').checked)
			string += "t";
		if(document.getElementById('s14').checked)
			string += "z";
		if(document.getElementById('immunity').value)
			string += "#" + $('immunity').value;
	}
	return string;
}

function BoxToMask()
{
	var Mask = 0;
	if(document.getElementById('p4'))
	{
		if(document.getElementById('p4').checked)
			Mask |= ADMIN_LIST_ADMINS;
		if(document.getElementById('p5').checked)
			Mask |= ADMIN_ADD_ADMINS;
		if(document.getElementById('p6').checked)
			Mask |= ADMIN_EDIT_ADMINS;
		if(document.getElementById('p7').checked)
			Mask |= ADMIN_DELETE_ADMINS;
			
		if(document.getElementById('p9').checked)
			Mask |= ADMIN_LIST_SERVERS;
		if(document.getElementById('p10').checked)
			Mask |= ADMIN_ADD_SERVER;
		if(document.getElementById('p11').checked)
			Mask |= ADMIN_EDIT_SERVERS;
		if(document.getElementById('p12').checked)
			Mask |= ADMIN_DELETE_SERVERS;
			
		if(document.getElementById('p14').checked)
			Mask |= ADMIN_ADD_BAN;
		if(document.getElementById('p16').checked)
			Mask |= ADMIN_EDIT_OWN_BANS;
		if(document.getElementById('p17').checked)
			Mask |= ADMIN_EDIT_GROUP_BANS;
		if(document.getElementById('p18').checked)
			Mask |= ADMIN_EDIT_ALL_BANS;
		if(document.getElementById('p19').checked)
			Mask |= ADMIN_BAN_PROTESTS;
		if(document.getElementById('p20').checked)
			Mask |= ADMIN_BAN_SUBMISSIONS;
		if(document.getElementById('p38').checked)
			Mask |= ADMIN_UNBAN_OWN_BANS;
		if(document.getElementById('p39').checked)
			Mask |= ADMIN_UNBAN_GROUP_BANS;
		if(document.getElementById('p32').checked)
			Mask |= ADMIN_UNBAN;
		if(document.getElementById('p33').checked)
			Mask |= ADMIN_DELETE_BAN;
		if(document.getElementById('p34').checked)
			Mask |= ADMIN_BAN_IMPORT;

		if(document.getElementById('p36').checked)
			Mask |= ADMIN_NOTIFY_SUB;
		if(document.getElementById('p37').checked)
			Mask |= ADMIN_NOTIFY_PROTEST;

		if(document.getElementById('p22').checked)
			Mask |= ADMIN_LIST_GROUPS;
		if(document.getElementById('p23').checked)
			Mask |= ADMIN_ADD_GROUP;
		if(document.getElementById('p29').checked)
			Mask |= ADMIN_EDIT_GROUPS;
		if(document.getElementById('p25').checked)
			Mask |= ADMIN_DELETE_GROUPS;
			
		if(document.getElementById('p26').checked)
			Mask |= ADMIN_WEB_SETTINGS;
			
		if(document.getElementById('p28').checked)
			Mask |= ADMIN_LIST_MODS;
		if(document.getElementById('p29').checked)
			Mask |= ADMIN_ADD_MODS;
		if(document.getElementById('p30').checked)
			Mask |= ADMIN_EDIT_MODS;
		if(document.getElementById('p31').checked)
			Mask |= ADMIN_DELETE_MODS;
			
		if(document.getElementById('p2').checked)
			Mask |= ADMIN_OWNER;
	}
	return Mask;
}

function search_bans()
{
	var type = "";
	var input = "";
	if($('name').checked) {
		type = "name";
		input = $('nick').value;
	}
	if($('steam_').checked) {
		type = (document.getElementById('steam_match').value == "1" ? "steam" : "steamid");
		input = $('steamid').value;
	}
	if($('ip_').checked) {
		type = "ip";
		input = $('ip').value;
	}
	if($('reason_').checked) {
		type = "reason";
		input = $('ban_reason').value;
	}
	if($('date').checked) {
		type = "date";
		input = $('day').value + "," + $('month').value + "," + $('year').value;
	}
	if($('length_').checked) {
        var length;
		type = "length";
		if($('length').value=="other")
			length = $('other_length').value;
		else
			length = $('length').value;
		input = $('length_type').value + "," + length;
	}
	if($('ban_type_').checked) {
		type = "btype";
		input = $('ban_type').value;
	}
	if($('bancount').checked) {
		type = "bancount";
		input = $('timesbanned').value;
	}
	if($('admin').checked) {
		type = "admin";
		input = $('ban_admin').value;
	}
	if($('where_banned').checked) {
		type = "where_banned";
		input = $('server').value;
	}
	if($('comment_').checked) {
		type = "comment";
		input = $('ban_comment').value;
	}
	if(type!="" && input!="") {
		window.location = "index.php?m=bans&advSearch=" + input + "&advType=" + type;
    }
}
var webSelected = new Array();
var srvSelected = new Array();
function getMultiple(ob, type) {
	if(type==1) {
		while (ob.selectedIndex != -1) 
		{ 
			webSelected.push(ob.options[ob.selectedIndex].value); 
			ob.options[ob.selectedIndex].selected = false; 
		}
	}
	if(type==2) {
		while (ob.selectedIndex != -1) 
		{ 
			srvSelected.push(ob.options[ob.selectedIndex].value); 
			ob.options[ob.selectedIndex].selected = false; 
		}
	}
}
function search_admins()
{
	var type = "";
	var input = "";
	if($('name_').checked) {
		type = "name";
		input = $('nick').value;
	}
	if($('steam_').checked) {
		type = (document.getElementById('steam_match').value == "1" ? "steam" : "steamid");
		input = $('steamid').value;
	}
	if($('admemail_').checked) {
		type = "admemail";
		input = $('admemail').value;
	}
	if($('webgroup_').checked) {
		type = "webgroup";
		input = $('webgroup').value;
	}
	if($('srvadmgroup_').checked) {
		type = "srvadmgroup";
		input = $('srvadmgroup').value;
	}
	if($('srvgroup_').checked) {
		type = "srvgroup";
		input = $('srvgroup').value;
	}
	if($('admwebflags_').checked) {
		type = "admwebflag";
		input = webSelected.toString();
	}
	if($('admsrvflags_').checked) {
		type = "admsrvflag";
		input = srvSelected.toString();
	}
	if($('admin_on_').checked) {
		type = "server";
		input = $('server').value;
	}
	if(type!="" && input!="") {
		window.location = "index.php?p=admin&c=admins&advSearch=" + input + "&advType=" + type;
    }
}

function search_log()
{
	var type = "";
	var input = "";
	if($('admin_').checked)
	{
		type = "admin";
		input = $('admin').value;
	}
	if($('message_').checked)
	{
		type = "message";
		input = $('message').value;
	}
	if($('date_').checked)
	{
		type = "date";
		input = $('day').value + "," + $('month').value + "," + $('year').value + "," + $('fhour').value + "," + $('fminute').value + "," + $('thour').value + "," + $('tminute').value;
	}
	if($('type_').checked)
	{
		type = "type";
		input = $('type').value;
	}
	if(type!="" && input!="")
		window.location = "index.php?p=admin&c=settings&advSearch=" + input + "&advType=" + type + "#^2";
}

function ShowBox(title, msg, color, redir, noclose)
{
	var type = "";
	
	if(color == "red")
		color = "error";
	else if(color == "blue")
		color = "info";
	else if(color == "green")
		color = "ok";
	
	$('dialog-title').setProperty("class", color);
	
	$('dialog-icon').setProperty("class", 'icon-'+color);
	
	$('dialog-title').setHTML(title);
	$('dialog-content-text').setHTML(msg);
	FadeElIn('dialog-placement', 750);
	
	var jsCde = "closeMsg('" + redir + "');";
	$('dialog-control').setHTML("<input name='dialog-close' onclick=\""+jsCde+"\" class='btn ok' onmouseover=\"ButtonOver('dialog-close')\" onmouseout='ButtonOver(\"dialog-close\")' id=\"dialog-close\" value=\""+t('Okay')+"\" type=\"button\">");
	$('dialog-control').setStyle('display', 'block');
	
	if(!noclose)
	{
		if(redir)
			setTimeout("window.location='" + redir + "'",5000);
		else
		{
			setTimeout("FadeElOut('dialog-placement', 750);",5000);
		}
	}
}
function closeMsg(redir)
{
	if(redir.toString().length > 0 && redir != "undefined")
		window.location = redir;
	else
	{
		FadeElOut('dialog-placement', 750);
	}
}

function TabToReload()
{
	var url = window.location.toString();
	var nurl = "window.location = '" + url.replace("#^" + url[url.length-1],"") + "'";
	$('admin_tab_0').setProperty('onclick', nurl);
}

function IsNumeric(sText)
{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;
 
	for (i = 0; i < sText.length && IsNumber == true; i++) 
	{ 
		Char = sText.charAt(i); 
  		if (ValidChars.indexOf(Char) == -1) 
		{
			IsNumber = false;
     	}
  	}
   	return IsNumber;
}

function ButtonOver(el)
{
	if($(el))
	{
		if($(el).hasClass('btn'))
		{
			$(el).removeClass('btn');
			$(el).addClass('btnhvr');
		}
		else
		{
			$(el).removeClass('btnhvr');
			$(el).addClass('btn');
		}
	}
}

function changePage(newPage, type, advSearch, advType)
{		
	nextPage = newPage.options[newPage.selectedIndex].value;
	if(advSearch!="" && advType !="") { 
		var searchlink = "&advSearch="+advSearch+"&advType="+advType; 
	} else { 
		var searchlink =""; 
	}
	 if (nextPage != 0)
	 {
		if(type == "A")
            window.location = "index.php?p=admin&c=admins"+searchlink+"&page="+nextPage;
		if(type == "B")
            window.location = "index.php?m=bans"+searchlink+"&page="+nextPage;
		if(type == "L")
            window.location = "index.php?p=admin&c=settings"+searchlink+"&page="+nextPage+"#^2";
        if(type == "P")
            window.location = "index.php?m=bans&p=admin&ppage="+nextPage+"#^1";
        if(type == "PA")
            window.location = "index.php?m=bans&p=admin&papage="+nextPage+"#^1~p1";
        if(type == "S")
            window.location = "index.php?m=bans&p=admin&spage="+nextPage+"#^2";
        if(type == "SA")
            window.location = "index.php?m=bans&p=admin&sapage="+nextPage+"#^2~s1";
	 }
}

function drag_drop_ns(name)
{
	if(!ns4)
		return;
	temp=eval(name);
	temp.captureEvents(Event.MOUSEDOWN | Event.MOUSEUP);
	temp.onmousedown=gons;
	temp.onmousemove=dragns;
	temp.onmouseup=stopns;
}
function gons(e)
{
	temp.captureEvents(Event.MOUSEMOVE);
	nsx=e.x;
	nsy=e.y;
}
function dragns(e)
{
	if(dragswitch==1) {
		temp.moveBy(e.x-nsx,e.y-nsy);
		return false;
	}
}
function stopns()
{
	temp.releaseEvents(Event.MOUSEMOVE);
}

//IE4 || NS6
function drag_drop(e)
{
	if(ie4&&dragapproved) {
		crossobj.style.left=tempx+event.clientX-offsetx+'px';
		crossobj.style.top=tempy+event.clientY-offsety+'px';
		return false;
	}
	else if(ns6&&dragapproved) {
		crossobj.style.left=tempx+e.clientX-offsetx+'px';
		crossobj.style.top=tempy+e.clientY-offsety+'px';
		return false;
	}
}
function initializiere_drag(e)
{
	crossobj=ns6? document.getElementById("dialog-placement") : document.all["dialog-placement"];
	var firedobj=ns6? e.target : event.srcElement;
	var topelement=ns6? "HTML" : "BODY";

	while (firedobj!=null&&firedobj.tagName!=topelement&&firedobj.id!="dragbar") {
		firedobj=ns6? firedobj.parentNode : firedobj.parentElement;
	}
	if(firedobj!=null&&firedobj.id=="dragbar")
	{
		offsetx=ie4? event.clientX : e.clientX;
		offsety=ie4? event.clientY : e.clientY;
		tempx=parseInt(crossobj.style.left);
		tempy=parseInt(crossobj.style.top);
		dragapproved=true;
		document.onmousemove=drag_drop;
	}

}
document.onmousedown=initializiere_drag;
document.onmouseup=new Function("dragapproved=false");

// Thanks to http://phpjs.org/functions/addslashes:303
function addslashes (str)
{
	return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
}

function t(message, params) {
    if(window.translates !== undefined && !isEmptyObject(translates[message])) {
        message = translates[message];
    }
    if(!isEmptyObject(params)) {
        forEach(params, function(key, val) {
            message = message.replace(key, val);
        });
    }
    return message;
}

function isEmptyObject(obj){
    var name;
    for(name in obj) {
        return false;
    }
    return true;
}

function forEach(data, callback){
  for(var key in data){
    if(data.hasOwnProperty(key)){
      callback(key, data[key]);
    }
  }
}