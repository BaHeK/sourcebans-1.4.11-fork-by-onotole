{if NOT $permission_addadmin}
	{t section="admin/admins" message="Access Denied"}
{else}
	<div id="msg-green" style="display:none;">
		<i><img src="./images/yay.png" alt="Warning" /></i>
		<b>{t section="admin/admins" message="Admin Added"}</b>
		<br />
		{t section="admin/admins" message="The new admin has been successfully added to the system."}<br /><br />
		<i>{t section="admin/admins" message="Redirecting back to admins page"}</i>
	</div>
	
	
	<div id="add-group">
		<h3>{t section="admin/admins" message="Admin Details"}</h3>
		{t section="admin/admins" message="For more information or help regarding a certain subject move your mouse over the question mark."}<br /><br />
		<table width="90%" border="0" style="border-collapse:collapse;" id="group.details" cellpadding="3">
			<tr>
		    	<td valign="top" width="35%">
		    		<div class="rowdesc">
		    			{help_icon title="Admin Login" message="This is the username the admin will use to login-to their admin panel. Also this will identify the admin on any bans they make." translate="admin/admins"}
                        {t section="admin/admins" message="Admin Login"}
		    		</div>
		    	</td>
		    	<td>
		    		<div align="left">
		        		<input type="text" TABINDEX=1 class="submit-fields" id="adminname" name="adminname" />
		      		</div>
		        	<div id="name.msg" class="badentry"></div>
		        </td>
			</tr>
		  	<tr>
		    	<td valign="top">
		    		<div class="rowdesc">
		    			{help_icon title="Steam ID" message="This is the admins 'STEAM' id. This must be set so that admins can use their admin rights ingame." translate="admin/admins"}
                        {t section="admin/admins" message="Admin Steam ID / Community ID"}
		    		</div>
		    	</td>
		    	<td>
		    		<div align="left">
		     			<input type="text" TABINDEX=2 value="STEAM_0:" class="submit-fields" id="steam" name="steam" />
		    		</div>
		    		<div id="steam.msg" class="badentry"></div>
		    	</td>
		  	</tr>
		  	<tr>
		    	<td valign="top">
		    		<div class="rowdesc">
		    			{help_icon title="Admin Email" message="Set the admins e-mail address. This will be used for sending out any automated messages from the system and changing of forgotten passwords. This is only required, if you set webpanel permissions." translate="admin/admins"}
                        {t section="admin/admins" message="Admin Email"}
		    		</div>
		    	</td>
		    	<td>
		    		<div align="left">
		        		<input type="text" TABINDEX=3 class="submit-fields" id="email" name="email" />
		     		</div>
		        	<div id="email.msg" class="badentry"></div>
		        </td>
		  	</tr>
		  	<tr>
		    	<td valign="top">
		    		<div class="rowdesc">
		    			{help_icon title="Password" message="The password the admin will need to access the admin panel. This is only required, if you set webpanel permissions." translate="admin/admins"}
                        {t section="admin/admins" message="Admin Password"}
		    		</div>
		    	</td>
		    	<td>
		    		<div align="left">
		       			<input type="password" TABINDEX=4 class="submit-fields" id="password" name="password" />
		      		</div>
		        	<div id="password.msg" class="badentry"></div>
		        </td>
		  	</tr>
		  	<tr>
		    	<td valign="top">
		    		<div class="rowdesc">
		    			{help_icon title="Password" message="Type the password again to confirm." translate="admin/admins"}
                        {t section="admin/admins" message="Admin Password (confirm)"}
		    		</div>
		    	</td>
		    	<td>
		    		<div align="left">
		        		<input type="password" TABINDEX=5 class="submit-fields" id="password2" name="password2" />
		      		</div>
		        	<div id="password2.msg" class="badentry"></div>
		        </td>
		  	</tr>
		    <tr>
		    	<td valign="top" width="35%">
		    		<div class="rowdesc">
		    			{help_icon title="Server Admin Password" message="If this box is checked, you will need to specify this password in the game server before you can use your admin rights." translate="admin/admins"}
                        {t section="admin/admins" message="Server Password"}
                        <small>
                            <a 
                                href="http://wiki.alliedmods.net/Adding_Admins_%28SourceMod%29#Passwords" 
                                title="{t section="admin/admins" message="SourceMod Password Info"}" 
                                target="_blank">
                                {t section="admin/admins" message="More"}
                            </a>
                        </small>
		    		</div>
		    	</td>
		    	<td>
		    		<div align="left">
		        		<input 
                            type="checkbox" 
                            id="a_useserverpass" 
                            name="a_useserverpass" 
                            TABINDEX=6 onclick="$('a_serverpass').disabled = !$(this).checked;" /> 
                        <input 
                            type="password" 
                            TABINDEX=7 
                            class="submit-fields" 
                            name="a_serverpass" 
                            id="a_serverpass" 
                            disabled="disabled" />
		    		</div>
					<div id="a_serverpass.msg" class="badentry"></div>
		    	</td>
		  	</tr>
		</table>
	
		
		<br />
	
		
		<h3>{t section="admin/admins" message="Admin Access"}</h3>
			<table width="90%" border="0" style="border-collapse:collapse;" id="group.details" cellpadding="3">
		  	<tr>
		    	<td valign="top" width="35%">
		    		<div class="rowdesc">
		    			{help_icon title="Server" message="<b>Server: </b><br>Choose the server, or server group that this admin will be able to administer." translate="admin/admins"}
                        {t section="admin/admins" message="Server Access"}
		    		</div>
		    	</td>
		    	<td>&nbsp;</td>
		  	</tr>
		  	<tr>
			  	<td colspan="2">
			  		<table width="90%" border="0" cellspacing="0" cellpadding="4" align="center">
						{foreach from="$group_list" item="group"}
							<tr>
								<td colspan="2" class="tablerow4">
                                    {$group.name}
                                    <b><i>{t section="admin/admins" message="(Group)"}</i></b>
                                </td>
								<td align="center" class="tablerow4">
                                    <input type="checkbox" id="group[]" name="group[]" value="g{$group.gid}" />
                                </td>
							</tr>
						{/foreach}
					
						{foreach from="$server_list" item="server"}
							<tr class="tablerow1">
								<td colspan="2" class="tablerow1" id="sa{$server.sid}">
                                    <i>
                                        {t section="admin/admins" message="Retrieving Hostname..."} 
                                        {$server.ip}:{$server.port}
                                    </i>
                                </td>
								<td align="center" class="tablerow1">
									<input type="checkbox" name="servers[]" id="servers[]" value="s{$server.sid}" />
						  		</td> 
							</tr>
						{/foreach}
			  		</table>
		  		</td>
			</tr>
		</table>
	
		<br />
		<h3>{t section="admin/admins" message="Admin Permissions"}</h3>
		<table width="90%" border="0" style="border-collapse:collapse;" id="group.details" cellpadding="3">
			<tr>
			    <td valign="top" width="35%">
			    	<div class="rowdesc">
			    		{help_icon  title="Admin Group" message="<b>Custom Permisions: </b><br>Select this to choose custom permissions for this admin.<br><br><b>New Group: </b><br>Select this to choose custom permissions and then save the permissions as a new group.<br><br><b>Groups: </b><br>Select a pre-made group to add the admin to." translate="admin/admins"}
                            {t section="admin/admins" message="Server Admin Group"}
			    	</div>
			    </td>
			    <td>
			    	<div align="left" id="admingroup">
				      	<select TABINDEX=8 onchange="update_server()" name="serverg" id="serverg" class="submit-fields">
					        <option value="-2">{t section="admin/admins" message="Please Select..."}</option>
					        <option value="-3">{t section="admin/admins" message="No Permissions"}</option>
					        <option value="c">{t section="admin/admins" message="Custom Permissions"}</option>
					        <option value="n">{t section="admin/admins" message="New Admin Group"}</option>
					        <optgroup label="{t section="admin/admins" message="Groups"}" style="font-weight:bold;">
						        {foreach from="$server_admin_group_list" item="server_wg"}
									<option value='{$server_wg.id}'>{$server_wg.name}</option>
								{/foreach}
							</optgroup>
				        </select>
			        </div>
			        <div id="server.msg" class="badentry"></div>
				</td>
		  	</tr>
		   	<tr>
		 		<td colspan="2" id="serverperm" valign="top" style="height:5px;overflow:hidden;"></td>
		 	</tr>
		   	<tr>
		    	<td valign="top">
		    		<div class="rowdesc">
		    			{help_icon title="Admin Group" message="<b>Custom Permisions: </b><br>Select this to choose custom permissions for this admin.<br><br><b>New Group: </b><br>Select this to choose custom permissions and then save the permissions as a new group.<br><br><b>Groups: </b><br>Select a pre-made group to add the admin to." translate="admin/admins"}
                        {t section="admin/admins" message="Web Admin Group"}
		    		</div>
		    	</td>
		    	<td>
		    		<div align="left" id="webgroup">
						<select TABINDEX=9 onchange="update_web();" name="webg" id="webg" class="submit-fields">
							<option value="-2">{t section="admin/admins" message="Please Select..."}</option>
							<option value="-3">{t section="admin/admins" message="No Permissions"}</option>
							<option value="c">{t section="admin/admins" message="Custom Permissions"}</option>
							<option value="n">{t section="admin/admins" message="New Admin Group"}</option>
							<optgroup label="{t section="admin/admins" message="Groups"}" style="font-weight:bold;">
								{foreach from="$server_group_list" item="server_g"}
									<option value='{$server_g.gid}'>{$server_g.name}</option>
								{/foreach}
							</optgroup>
						</select>
		        	</div>
		        	<div id="web.msg" class="badentry"></div>
		       	</td>
		  	</tr>
		  	<tr>
		 		<td colspan="2" id="webperm" valign="top" style="height:5px;overflow:hidden;"></td>
		 	</tr>
		  	<tr>
		    	<td>&nbsp;</td>
		    	<td>
			    	{sb_button text="Add Admin" onclick="ProcessAddAdmin();" class="ok" id="aadmin" submit=false translate="admin/admins"}
				      &nbsp;
				    {sb_button text="Back" onclick="history.go(-1)" class="cancel" id="aback" translate="admin/admins"}
		      	</td>
		  	</tr>
		</table>
        {$server_script}
	</div>
    <script>
        {literal}
        function update_server()
        {
            $('serverperm').setHTML('');
            var serverg = document.getElementById('serverg').value, height;
            if(serverg == "c" || serverg == "n") {
        {/literal}
                $('server.msg').setHTML('{t section="admin/admins" message="Please Wait..."}');
                $('server.msg').setStyle('display', 'block');
        {literal}
            }

            if(serverg == "c") {
                height = 580;
            } else if(serverg == "n") {
                height = 590;
            } else {
                $('serverperm').setHTML('');
                height = 1;
            }
            Shrink('serverperm', 1000, height);

            if(serverg == "c" || serverg == "n") {
                setTimeout("xajax_UpdateAdminPermissions(2, document.getElementById('serverg').value)",1000);
            } else {
                $('server.msg').setHTML('');
                $('server.msg').setStyle('display', 'none');
            }
        }
        function update_web()
        {
            $('webperm').setHTML('');

            var webg = document.getElementById('webg').value, height;

            if(webg == "c" || webg == "n") {
        {/literal}
                $('web.msg').setHTML('{t section="admin/admins" message="Please Wait..."}');
                $('web.msg').setStyle('display', 'block');
        {literal}
            }

            if(webg == "c") {
                height = 390;
            } else if(webg == "n") {
                height = 410;
            } else {
                $('webperm').setHTML('');
                height = 1;
            }
            Shrink('webperm', 1000, height);

            if(webg == "c" || webg == "n") {
                setTimeout("xajax_UpdateAdminPermissions(1, document.getElementById('webg').value)",1000);
            } else {
                $('web.msg').setHTML('');
                $('web.msg').setStyle('display', 'none');
            }
        }
        function ProcessAddAdmin()
        {
            var Mask = BoxToMask();
            var srvMask = BoxToSrvMask();
            var server_a_pass = "-1";

            var el = document.getElementsByName('group[]');
            var grp = "";
            for(i=0;i<el.length;i++){
                if(el[i].checked){
                    grp = grp + "," + el[i].value;
                }
            }

            var el = document.getElementsByName('servers[]');
            var svr = "";
            for(i=0;i<el.length;i++){
                if(el[i].checked){
                    svr = svr + "," + el[i].value;
                }
            }

            var serverg = document.getElementById('serverg').value;
            if(serverg == "-3")
            {
                //serverg = "c";
                srvMask = "";
            }
            var webg = document.getElementById('webg').value;
            if(webg == "-3")
            {
                //webg = "c";
                Mask = 0;
            }

            if(document.getElementById('a_useserverpass').checked)
                server_a_pass = document.getElementById('a_serverpass').value;

            if(document.getElementById('webname') && !document.getElementById('servername'))
            xajax_AddAdmin(Mask,srvMask, document.getElementById('adminname').value, //Admin name
                            document.getElementById('steam').value, //Admin Steam
                            document.getElementById('email').value, // Email
                            document.getElementById('password').value,//passwrds
                            document.getElementById('password2').value,
                            serverg, //servergroup
                            webg, 
                            server_a_pass,
                            document.getElementById('webname').value,
                            0,
                            grp,
                            svr); //server / server group
            else if(!document.getElementById('webname') && document.getElementById('servername'))
            xajax_AddAdmin(Mask,srvMask, document.getElementById('adminname').value, //Admin name
                            document.getElementById('steam').value, //Admin Steam
                            document.getElementById('email').value, // Email
                            document.getElementById('password').value,//passwrds
                            document.getElementById('password2').value,
                            serverg, //servergroup
                            webg, 
                            server_a_pass,
                            0,
                            document.getElementById('servername').value,
                            grp,
                            svr);
            else if(document.getElementById('webname') && document.getElementById('servername'))
            xajax_AddAdmin(Mask,srvMask, document.getElementById('adminname').value, //Admin name
                            document.getElementById('steam').value, //Admin Steam
                            document.getElementById('email').value, // Email
                            document.getElementById('password').value,//passwrds
                            document.getElementById('password2').value,
                            serverg, //servergroup
                            webg, 
                            server_a_pass,
                            document.getElementById('webname').value,
                            document.getElementById('servername').value,
                            grp,
                            svr);
            else
            xajax_AddAdmin(Mask,srvMask, document.getElementById('adminname').value, //Admin name
                            document.getElementById('steam').value, //Admin Steam
                            document.getElementById('email').value, // Email
                            document.getElementById('password').value,//passwrds
                            document.getElementById('password2').value,
                            serverg, //servergroup
                            webg, 
                            server_a_pass,
                            0,
                            0,
                            grp,
                            svr);

        }
        {/literal}
    </script>
{/if}
