<h3>{t section="admin" message="Please select an option to administer."}</h3>
<div id="cpanel">
	<ul>
		{if $access_admins}
			<li>
				<a href="index.php?p=admin&amp;c=admins">
				<img src="themes/default/images/admin/admins.png" alt="{t section="admin" message="Admin Settings"}" border="0" /><br />
				{t section="admin" message="Admin Settings"}
		  		</a>
			</li>
		{/if}
		{if $access_servers}
			<li>
				<a href="index.php?p=admin&amp;c=servers">
				<img src="themes/default/images/admin/servers.png" alt="{t section="admin" message="Server Settings"}" border="0" /><br />
				{t section="admin" message="Server Settings"}
		  		</a>
			</li>
		{/if}
		{if $access_bans}
			<li>
				<a href="index.php?p=admin&amp;c=bans">
				<img src="themes/default/images/admin/bans.png" alt="{t section="admin" message="Edit Bans"}" border="0" /><br />
				{t section="admin" message="Bans"}
		  		</a>
			</li>
		{/if}
		{if $access_groups}
			<li>
				<a href="index.php?p=admin&amp;c=groups">
				<img src="themes/default/images/admin/groups.png" alt="{t section="admin" message="Edit Groups"}" border="0" /><br />
				{t section="admin" message="Group Settings"}
		  		</a>
			</li>
		{/if}
		{if $access_settings}
			<li>
				<a href="index.php?p=admin&amp;c=settings">
				<img src="themes/default/images/admin/settings.png" alt="{t section="admin" message="SourceBans Settings"}" border="0" /><br />
				{t section="admin" message="Webpanel Settings"}
		  		</a> 
			</li>
		{/if}
		{if $access_mods}
			<li>
				<a href="index.php?p=admin&amp;c=mods">
				<img src="themes/default/images/admin/mods.png" alt="{t section="admin" message="Mods"}" border="0" /><br />
				{t section="admin" message="Manage Mods"}
		  		</a>
			</li>
		{/if}
	</ul>
</div>	
<br />
<h3>{t section="admin" message="Modules management"}</h3>
<div id="cpanel">
	<ul>
        {if !$modules}
        <li>No modules</li>
        {else}
        {foreach from=$modules item='module'}
        <li>
            <a href="index.php?m={$module.name}&amp;p={$module.adminPage}">
            <img src="{$module.mainImage}" alt="{$module.name|ucfirst}" border="0" /><br />
            {$module.name|ucfirst}
            </a>
        </li>
        {/foreach}
        {/if}
	</ul>
</div>	
<br />

<table width="100%" border="0" cellpadding="3" cellspacing="0">
	<tr>
		<td width="33%" align="center">
            <h3>{t section="admin" message="Version Information"}</h3>
        </td>
		<td width="33%" align="center">
            <h3>{t section="admin" message="Admin Information"}</h3>
        </td>
		<td width="33%" align="center">
            <h3>{t section="admin" message="Ban Information"}</h3>
        </td>
	</tr>
	<tr>
		<td>
            {t section="admin" message="Latest release"}: <strong id="relver">{t section="admin" message="Please Wait..."}</strong>
        </td>
		<td>
            {t section="admin" message="Total admins"}: <strong>{$total_admins}</strong>
        </td>
		<td>
            {t section="admin" message="Total bans"}: <strong>{$total_bans}</strong>
        </td>
	</tr>
	<tr>
		<td>
			{if $sb_svn}
				{t section="admin" message="Latest SVN"}: <strong id="svnrev">{t section="admin" message="Please Wait..."}</strong>
			{/if}		
		</td>
		<td>&nbsp;</td>
		<td>{t section="admin" message="Connection blocks"}: <strong>{$total_blocks}</strong></td>
	</tr>
	<tr>
		<td id="versionmsg">{t section="admin" message="Please Wait..."}</td>
		<td> <strong> </strong></td>
		<td>{t section="admin" message="Total demo size"}: <strong>{$demosize}</td>
	</tr>
	<tr>
		<td width="33%" align="center">
            <h3>{t section="admin" message="Server Information"}</h3>
        </td>
		<td width="33%" align="center">
            <h3>{t section="admin" message="Protest Information"}</h3>
        </td>
		<td width="33%" align="center">
            <h3>{t section="admin" message="Submission Information"}</h3>
        </td>
	</tr>
	<tr>
		<td>
            {t section="admin" message="Total Servers"}: <strong>{$total_servers}</strong>
        </td>
		<td>
            {t section="admin" message="Total protests"}: <strong>{$total_protests}</strong>
        </td>
		<td>
            {t section="admin" message="Total submissions"}: <strong>{$total_submissions}</strong>
        </td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>
            {t section="admin" message="Archived protests"}: <strong>{$archived_protests}</strong>
        </td>
		<td>
            {t section="admin" message="Archived submissions"}: <strong>{$archived_submissions}</strong>
        </td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
<script type="text/javascript">
    xajax_CheckVersion();
</script>