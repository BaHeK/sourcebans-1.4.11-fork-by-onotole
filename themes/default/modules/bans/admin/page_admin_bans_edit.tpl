<form action="" method="post">
	<div id="admin-page-content">
		<div id="0">
			<div id="msg-green" style="display:none;">
				<i><img src="./images/yay.png" alt="Warning" /></i>
				<b>-{t section="modules/bans/admin" message="Ban Updated"}-</b>
				<br />
				-{t section="modules/bans/admin" message="The ban has been updated successfully"}-
                <br />
                <br />
				<i>-{t section="modules/bans/admin" message="Redirecting back to bans page"}-</i>
			</div>
			<div id="add-group">
                <h3>-{t section="modules/bans/admin" message="Ban Details"}-</h3>
                -{t section="modules/bans/admin" message="For more information or help regarding a certain subject move your mouse over the question mark."}-
                <br />
                <br />
                <input type="hidden" name="insert_type" value="add">
                <table width="90%" border="0" style="border-collapse:collapse;" id="group.details" cellpadding="3">
                    <tr>
                        <td valign="top" width="35%">
                            <div class="rowdesc">
                                -{help_icon title="Nickname" message="This is the name of the player that was banned." translate="modules/bans/admin"}-
                                -{t section="modules/bans/admin" message="Nickname"}-
                            </div>
                        </td>
                        <td>
                            <div align="left">
                                <input type="text" class="submit-fields" id="name" name="name" value="-{$ban_name}-" />
                            </div>
                            <div id="name.msg" class="badentry"></div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" width="35%">
                            <div class="rowdesc">
                                -{help_icon title="Ban Type" message="Choose whether to ban by Steam ID or IP address." translate="modules/bans/admin"}-
                                -{t section="modules/bans/admin" message="Ban Type"}-
                            </div>
                        </td>
                        <td>
                            <div align="left">
                                <select id="type" name="type" TABINDEX=2 class="submit-fields">
                                    <option value="0">-{t section="modules/bans/admin" message="Steam ID"}-</option>
                                    <option value="1">-{t section="modules/bans/admin" message="IP Address"}-</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <div class="rowdesc">
                                -{help_icon
                                    title="Steam ID"
                                    message="This is the Steam ID of the player that is banned. You may want to type a Community ID either."
                                    translate="modules/bans/admin"
                                }-
                                -{t section="modules/bans/admin" message="Steam ID"}-
                            </div>
                        </td>
                        <td>
                            <div align="left">
                                <input value="-{$ban_authid}-" type="text" TABINDEX=3 class="submit-fields" id="steam" name="steam" />
                            </div>
                            <div id="steam.msg" class="badentry"></div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" width="35%">
                            <div class="rowdesc">
                                -{help_icon title="IP" message="This is the IP of the player that is banned" translate="modules/bans/admin"}-
                                -{t section="modules/bans/admin" message="IP Address"}-
                            </div>
                        </td>
                        <td>
                            <div align="left">
                                <input value="-{$ban_ip}-" type="text" TABINDEX=3 class="submit-fields" id="ip" name="ip" />
                            </div>
                            <div id="ip.msg" class="badentry"></div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" width="35%">
                            <div class="rowdesc">
                                -{help_icon title="Reason" message="The reason that this player was banned." translate="modules/bans/admin"}-
                                -{t section="modules/bans/admin" message="Reason"}-
                            </div>
                        </td>
                        <td>
                            <div align="left">
                                <select 
                                    id="listReason" 
                                    name="listReason" 
                                    TABINDEX=4 
                                    class="submit-fields" 
                                    onChange="changeReason(this[this.selectedIndex].value);">
                                    <option value="" selected>
                                        -{t section="modules/bans/admin" message=" -- Select Reason -- "}-
                                    </option>
                                    -{$banreasons}-
                                    -{if $customreason}-
                                    <optgroup label="Custom">
                                    -{foreach from="$customreason" item="creason"}-
                                        <option value="-{$creason}-">
                                            -{$creason}-
                                        </option>
                                    -{/foreach}-
                                    </optgroup>
                                    -{/if}-
                                    <option value="other">
                                        -{t section="modules/bans/admin" message="Other Reason"}-
                                    </option>
                                </select>
                                <div id="dreason" style="display:none;">
                                    <textarea class="submit-fields" TABINDEX=4 cols="30" rows="5" id="txtReason" name="txtReason"></textarea>
                                </div>
                            </div>
                            <div id="reason.msg" class="badentry"></div>
                        </td>
                    </tr>			  
                    <tr>
                        <td valign="top" width="35%">
                            <div class="rowdesc">
                                -{help_icon title="Ban Length" message="Select how long you want to ban this person for." translate="modules/bans/admin"}-
                                -{t section="modules/bans/admin" message="Ban Length"}-
                            </div>
                        </td>
                        <td>
                            <div align="left">
                                <select id="banlength" name="banlength" TABINDEX=5 class="submit-fields">
									-{$bantimes}-
                                </select>
                            </div>
                            <div id="length.msg" class="badentry"></div>
                        </td>
                    </tr>
			  
                    <tr>
                        <td valign="top" width="35%">
                            <div class="rowdesc">
                                -{help_icon title="Upload Demo" message="Click here to upload a demo with this ban submission." translate="modules/bans/admin"}-
                                -{t section="modules/bans/admin" message="Upload Demo"}-
                            </div>
                        </td>
                        <td>
                            <div align="left">
                                -{sb_button
                                    text="Upload a Demo"
                                    onclick="childWindow=open('pages/admin.uploaddemo.php','upload','resizable=no,width=300,height=160');"
                                    class="save"
                                    id="uploaddemo"
                                    submit=false
                                    translate="modules/bans/admin"
                                }-
                            </div>
                            <div id="demo.msg" style="color:#CC0000;">
                                -{$ban_demo}-
                            </div>
                        </td>
                    </tr>
			 
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="hidden" name="did" id="did" value="" />
                            <input type="hidden" name="dname" id="dname" value="" /> 
                            -{sb_button text="Save Changes" class="ok" id="editban" submit=true translate="modules/bans/admin"}-
                            &nbsp;
                            -{sb_button text="Back" onclick="history.go(-1)" class="cancel" id="back" submit=false translate="modules/bans/admin"}-
                        </td>
                    </tr>
                </table>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
    var did = 0;
    var dname = "";
    function demo(id, name)
    {
        $('demo.msg').setHTML("-{t section="modules/bans/admin" message="Uploaded"}-: <b>" + name + "</b>");
        $('did').value = id;
        $('dname').value = name;
    }
</script>