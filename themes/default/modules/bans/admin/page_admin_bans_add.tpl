{if NOT $permission_addban}
	Access Denied!
{else}
	<div id="msg-green" style="display:none;">
		<i><img src="./images/yay.png" alt="Success" /></i>
		<b>{t section="modules/bans/admin" message="Ban Added"}</b><br />
		{t section="modules/bans/admin" message="The new ban has been added to the system."}<br /><br />
		<i>{t section="modules/bans/admin" message="Redirecting back to bans page"}</i>
	</div>
	
	<div id="add-group1">
		<h3>{t section="modules/bans/admin" message="Add Ban"}</h3>
		{t section="modules/bans/admin" message="For more information or help regarding a certain subject move your mouse over the question mark."}
        <br />
        <br />
		<table width="90%" style="border-collapse:collapse;" id="group.details" cellpadding="3">
 		<tr>
    		<td valign="top" width="35%">
    			<div class="rowdesc">
    				{help_icon title="Nickname" message="Type the nickname of the person that you are banning." translate="admin/bans"}
                    {t section="modules/bans/admin" message="Nickname"} 
    			</div>
    		</td>
    		<td>
    			<div align="left">
    				<input type="hidden" id="fromsub" value="" />
      				<input type="text" TABINDEX=1 class="submit-fields" id="nickname" name="nickname" />
    			</div>
    			<div id="nick.msg" class="badentry"></div>
    		</td>
  		</tr>
 		  <tr>
    		<td valign="top" width="35%">
    			<div class="rowdesc">
    				{help_icon title="Ban Type" message="Choose whether to ban by Steam ID or IP address." translate="admin/bans"}
                    {t section="modules/bans/admin" message="Ban Type"}
    			</div>
    		</td>
    		<td>
    			<div align="left">
    				<select id="type" name="type" TABINDEX=2 class="submit-fields">
              <option value="0">{t section="modules/bans/admin" message="Steam ID"}</option>
              <option value="1">{t section="modules/bans/admin" message="IP Address"}</option>
            </select>
    			</div>
    		</td>
 		  </tr>
  		<tr>
    		<td valign="top">
    			<div class="rowdesc">
    				{help_icon title="Steam ID / Community ID" message="The Steam ID or Community ID of the person to ban." translate="admin/bans"}
                    {t section="modules/bans/admin" message="Steam ID / Community ID"}
    			</div>
    		</td>
    		<td>
    			<div align="left">
      			<input type="text" TABINDEX=3 class="submit-fields" id="steam" name="steam" />
    			</div>
    			<div id="steam.msg" class="badentry"></div>
    		</td>
  		</tr>
 		  <tr>
    		<td valign="top" width="35%">
    			<div class="rowdesc">
    				{help_icon title="IP Address" message="Type the IP address of the person you want to ban." translate="admin/bans"}
                    {t section="modules/bans/admin" message="IP Address"}
    			</div>
    		</td>
    		<td>
    			<div align="left">
      			<input type="text" TABINDEX=3 class="submit-fields" id="ip" name="ip" />
    			</div>
    			<div id="ip.msg" class="badentry"></div>
    		</td>
  		</tr>
 		  <tr>
    		<td valign="top" width="35%">
    			<div class="rowdesc">
    				{help_icon title="Ban Reason" message="Explain in detail, why this ban is being made." translate="admin/bans"}
                    {t section="modules/bans/admin" message="Ban Reason"}
    			</div>
    		</td>
    		<td>
    			<div align="left">
    				<select 
                        id="listReason" 
                        name="listReason" 
                        TABINDEX=4 
                        class="submit-fields" 
                        onChange="changeReason(this[this.selectedIndex].value);">
                        {$banreasons}
                        {if $customreason}
                        <optgroup label="{t section="modules/bans/admin" message="Custom"}">
                        {foreach from=$customreason item="creason"}
                            <option value="{$creason}">{$creason}</option>
                        {/foreach}
                        </optgroup>
                        {/if}
                        <option value="other">{t section="modules/bans/admin" message="Other Reason"}</option>
                    </select>
                    <div id="dreason" style="display:none;">
     					<textarea class="submit-fields" TABINDEX=4 cols="30" rows="5" id="txtReason" name="txtReason"></textarea>
     				</div>
    			</div>
    			<div id="reason.msg" class="badentry"></div>
    		</td>
      </tr>
      <tr>
    		<td valign="top" width="35%">
    			<div class="rowdesc">
    				{help_icon title="Ban Length" message="Select how long you want to ban this person for." translate="admin/bans"}
                    {t section="modules/bans/admin" message="Ban Length"}
    			</div>
    		</td>
    		<td>
    			<div align="left">
      				<select id="banlength" TABINDEX=5 class="submit-fields">
						{$bantimes}
					</select>
    			</div>
    			<div id="length.msg" ></div>
    		</td>
  		</tr>
  		
  		
  		<tr>
    		<td valign="top" width="35%">
    			<div class="rowdesc">
    				{help_icon title="Upload Demo" message="Click here to upload a demo with this ban submission." translate="admin/bans"}
                    {t section="modules/bans/admin" message="Upload Demo"}
    			</div>
    		</td>
    		<td>
    			<div align="left">
    				{sb_button 
                        text="Upload a demo" 
                        onclick="childWindow=open('pages/admin.uploaddemo.php','upload','resizable=no,width=300,height=160');" 
                        class="save" 
                        id="udemo" 
                        submit=false
                         translate="admin/bans"
                    }
    			</div>
    			<div id="demo.msg" style="color:#CC0000;"></div>
    		</td>
  		</tr>
  		<tr>
    		<td>&nbsp;</td>
	    	<td>
	      		{sb_button text="Add Ban" onclick="ProcessBan();" class="ok" id="aban" submit=false translate="admin/bans"}
				      &nbsp;
				{sb_button text="Back" onclick="history.go(-1)" class="cancel" id="aback" translate="admin/bans"}
	      	</td>
  		</tr>
	</table>
</div>
{/if}
