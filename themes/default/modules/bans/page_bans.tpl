{if $comment}
<h3>{$commenttype}</h3>
<table width="90%" align="center" border="0" style="border-collapse:collapse;" id="group.details" cellpadding="3">
    <tr>
        <td valign="top">
            <div class="rowdesc">
                {help_icon title="Comment Text" message="Type the text you would like to say." translate="modules/bans/main"}
                {t section="modules/bans/main" message="Comment Text"}
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div align="left">
                <textarea
                    rows="10"
                    cols="60"
                    class="submit-fields"
                    style="width:500px;"
                    id="commenttext"
                    name="commenttext">{$commenttext}</textarea>
            </div>
            <div id="commenttext.msg" class="badentry"></div>
        </td>
    </tr>
    <tr>
        <td>
            <input type="hidden" name="bid" id="bid" value="{$comment}">
            <input type="hidden" name="ctype" id="ctype" value="{$ctype}">
            {if $cid != ""}
                <input type="hidden" name="cid" id="cid" value="{$cid}">
            {else}
                <input type="hidden" name="cid" id="cid" value="-1">
            {/if}
            <input type="hidden" name="page" id="page" value="{$page}">
            {sb_button
                text=$commenttype
                onclick="ProcessComment();"
                class="ok"
                id="acom"
                submit=false
                translate="modules/bans/main"
            }
            &nbsp;
            {sb_button text="Back" onclick="history.go(-1)" class="cancel" id="aback" translate="modules/bans/main"}
        </td>
    </tr>
    {foreach from=$othercomments item="com"}
    <tr>
        <td colspan='3'>
            <hr>
        </td>
    </tr>
    <tr>
        <td>
            <b>{$com.comname}</b>
        </td>
        <td align="right">
            <b>{$com.added}</b>
        </td>
    </tr>
    <tr>
        <td colspan='2'>
            {$com.commenttxt}
        </td>
    </tr>
    {if $com.editname != ''}
    <tr>
        <td colspan="3">
            <span style="font-size:6pt;color:grey;">
                {$commenta.editData}
            </span>
        </td>
    </tr>
    {/if}
    {/foreach}
</table>
{else}
<h3 align="left">
    {t section="modules/bans/main" message="Banlist Overview - <i>Total Bans: [[totalbans]]</i>" totalbans=$total_bans}
</h3>
<br />
<div align="center">
	<table width="80%" cellpadding="0" class="listtable" cellspacing="0">
		<tr class="sea_open">
			<td width="2%" height="16" class="listtable_top" colspan="3">
                {t section="admin/bans" message="<b>Advanced Search</b> (Click)"}
            </td>
	  	</tr>
	  	<tr>
	  		<td>
	  		<div class="panel">
	  			<table width="100%" cellpadding="0" class="listtable" cellspacing="0">
			    <tr>
					<td class="listtable_1" width="8%" align="center">
                        <input id="name" name="search_type" type="radio" value="name">
                    </td>
			        <td class="listtable_1" width="26%">
                        {t section="admin/bans" message="Nickname"}
                    </td>
			        <td class="listtable_1" width="66%">
                        <input 
                            type="text" 
                            id="nick" 
                            value="" 
                            onmouseup="$('name').checked = true" 
                            style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 250px;">
                    </td>
				</tr>       
			    <tr>
			        <td align="center" class="listtable_1" >
                        <input id="steam_" type="radio" name="search_type" value="radiobutton">
                    </td>
			        <td class="listtable_1" >
                        {t section="admin/bans" message="SteamID"}
                    </td>
			        <td class="listtable_1" >
			            <input 
                            type="text" 
                            id="steamid" 
                            value="" 
                            onmouseup="$('steam_').checked = true"
                            style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 150px;">
                        <select 
                            id="steam_match" 
                            onmouseup="$('steam_').checked = true" 
                            style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 100px;">
                            <option value="0" selected>{t section="admin/bans" message="Exact Match"}</option>
                            <option value="1">{t section="admin/bans" message="Partial Match"}</option>
				    </select>
				</td>
			    </tr>
				{if !$hideplayerips}
				<tr>
					<td align="center" class="listtable_1" >
                        <input id="ip_" type="radio" name="search_type" value="radiobutton">
                    </td>
					<td class="listtable_1" >
                        {t section="admin/bans" message="IP Address"}
                    </td>
					<td class="listtable_1" >
                        <input 
                            type="text" 
                            id="ip" 
                            value="" 
                            onmouseup="$('ip_').checked = true"
                            style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 250px;"></td>
				</tr>
				{/if}
			    <tr>
			        <td align="center" class="listtable_1" >
                        <input id="reason_" type="radio" name="search_type" value="radiobutton">
                    </td>
			        <td class="listtable_1" >
                        {t section="admin/bans" message="Reason"}
                    </td>
			        <td class="listtable_1" >
                        <input 
                            type="text" 
                            id="ban_reason" 
                            value="" 
                            onmouseup="$('reason_').checked = true" 
                            style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 250px;">
                    </td>
			    </tr>
				<tr>
					<td align="center" class="listtable_1" >
                        <input id="date" type="radio" name="search_type" value="radiobutton">
                    </td>
			        <td class="listtable_1" >
                        {t section="admin/bans" message="Date"}
                    </td>
			        <td class="listtable_1" >
			        	<input type="text" id="day" value="DD" onmouseup="$('date').checked = true" style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 75px;">
			            <input type="text" id="month" value="MM" onmouseup="$('date').checked = true" style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 75px;">
			            <input type="text" id="year" value="YY" onmouseup="$('date').checked = true" style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 75px;"> 
			        </td>
			  	</tr>
				<tr>
			        <td align="center" class="listtable_1" >
                        <input id="length_" type="radio" name="search_type" value="radiobutton">
                    </td>
			        <td class="listtable_1" >
                        {t section="admin/bans" message="Length"}
                    </td>
			        <td class="listtable_1" >
						<table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td>
						            <select id="length_type" onmouseup="$('length_').checked = true" style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 40px;">
										<option value="e" title="{t section="admin/bans" message="equal to"}">=</option>
										<option value="h" title="{t section="admin/bans" message="greater"}">&gt;</option>
										<option value="l" title="{t section="admin/bans" message="smaller"}">&lt;</option>
										<option value="eh" title="{t section="admin/bans" message="equal to or greater"}">&gt;=</option>
										<option value="el" title="{t section="admin/bans" message="equal to or smaller"}">&lt;=</option>
									</select>
								</td>
								<td>
									<input type="text" id="other_length" name="other_length" onmouseup="$('length_').checked = true" style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 190px;display:none;">
								</td>
								<td>
									<select
                                        id="length"
                                        onmouseup="$('length_').checked = true"
                                        onchange="switch_length(this);"
                                        onmouseover="if(this.options[this.selectedIndex].value=='other')$('length').setStyle('width', '210px');if(this.options[this.selectedIndex].value=='other')this.focus();"
                                        onblur="if(this.options[this.selectedIndex].value=='other')$('length').setStyle('width', '20px');"
                                        style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 210px;">
								        {$bantimes}
										<option value="other">{t section="admin/bans" message="smaller"}</option>
									</select>
								</td>
							</tr>
						</table>
					</td>
			    </tr>
				<tr>
			        <td align="center" class="listtable_1" ><input id="ban_type_" type="radio" name="search_type" value="radiobutton"></td>
			        <td class="listtable_1" >
                        {t section="admin/bans" message="Ban Type"}
                    </td>
			        <td class="listtable_1" >
			            <select 
                            id="ban_type" 
                            onmouseup="$('ban_type_').checked = true" 
                            style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 250px;">
					        <option value="0" selected>{t section="admin/bans" message="Steam ID"}</option>
					        <option value="1">{t section="admin/bans" message="IP Address"}</option>
						</select>
					</td>
			    </tr>
                {if !$hideadminname}
			    <tr>
			    	<td class="listtable_1"  align="center">
                        <input id="admin" name="search_type" type="radio" value="radiobutton">
                    </td>
			        <td class="listtable_1" >
                        {t section="admin/bans" message="Admin"}
                    </td>
			        <td class="listtable_1" >
						<select id="ban_admin" onmouseup="$('admin').checked = true" style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 250px;">
							{foreach from=$admin_list item="admin"}
								<option label="{$admin.user}" value="{$admin.aid}">{$admin.user}</option>
							{/foreach}
						</select>           
					</td> 
				</tr>
                {/if}
			    <tr>
			    	<td class="listtable_1"  align="center">
                        <input id="where_banned" name="search_type" type="radio" value="radiobutton">
                    </td>
					<td class="listtable_1" >
                        {t section="admin/bans" message="Server"}
                    </td>
			        <td class="listtable_1" >
						<select id="server" onmouseup="$('where_banned').checked = true" style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 250px;">
						<option label="Web Ban" value="0">Web Ban</option>
							{foreach from=$server_list item="server"}
								<option value="{$server.sid}" id="ss{$server.sid}">
                                    {t section="admin/bans" message="Retrieving Hostname..."} ({$server.ip}:{$server.port})
                                </option>
							{/foreach}
						</select>            
					</td>
			    </tr>
				{if $is_admin}
				<tr>
			        <td align="center" class="listtable_1" >
                        <input id="comment_" type="radio" name="search_type" value="radiobutton">
                    </td>
			        <td class="listtable_1" >
                        {t section="admin/bans" message="Comment"}
                    </td>
			        <td class="listtable_1" >
                        <input 
                            type="text" 
                            id="ban_comment" 
                            value="" 
                            onmouseup="$('comment_').checked = true" 
                            style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 250px;">
                    </td>
			    </tr>
				{/if}
			    <tr>
				    <td> </td>
				    <td> </td>
			        <td>
                        {sb_button 
                            text="Search"
                            onclick="search_bans();"
                            class="ok"
                            id="searchbtn"
                            submit=false,
                            translate="admin/bans"
                        }
                    </td>
			    </tr>
			   </table>
			   </div>
		  </td>
		</tr>
	</table>
</div>
{$server_script}
<script>
    InitAccordion('tr.sea_open', 'div.panel', 'mainwrapper');
</script>

<br />
<div id="banlist-nav"> 
    {$ban_nav}
</div>
<a
    href="index.php?m=bans&hideinactive={if NOT $hide}false{else}true{/if}{$searchlink|htmlspecialchars}"
    title="{$hidetext}">
    {$hidetext}
</a>
<div id="banlist">
	<table width="100%" cellspacing="0" cellpadding="0" align="center" class="listtable">
		<tr>
			{if $view_bans}
			<td
                height="16"
                class="listtable_1"
                style="padding:0px;width:3px;"
                align="center">
                <div
                    class="ok"
                    style="height:16px;width:16px;cursor:pointer;"
                    title="Select All"
                    name="tickswitch"
                    id="tickswitch"
                    onclick="TickSelectAll();"></div>
            </td>
			{/if}
			<td width="12%" height="16" class="listtable_top" align="center">
                <b>{t section="modules/bans/main" message="MOD/Country"}</b>
            </td>
			<td width="14%" height="16" class="listtable_top" align="center">
                <b>{t section="modules/bans/main" message="Date"}</b>
            </td>
			<td height="16" class="listtable_top">
                <b>{t section="modules/bans/main" message="Player"}</b>
            </td>
			{if !$hideadminname}
			<td width="10%" height="16" class="listtable_top">
                <b>{t section="modules/bans/main" message="Admin"}</b>
            </td>
			{/if}
			<td width="10%" height="16" class="listtable_top" align="center">
                <b>{t section="modules/bans/main" message="Length"}</b>
            </td>  
		</tr>
		{foreach from=$ban_list item=ban name=banlist}
		<tr class="opener tbl_out" onmouseout="this.className='tbl_out'" onmouseover="this.className='tbl_hover'" 
			{if $ban.server_id != 0}
				onclick="xajax_ServerHostPlayers({$ban.server_id}, {$ban.ban_id});"
			{/if}
			>
            {if $view_bans}
            <td height="16" align="center" class="listtable_1" style="padding:0px;width:3px;">
                <input
                    type="checkbox"
                    name="chkb_{$smarty.foreach.banlist.index}"
                    id="chkb_{$smarty.foreach.banlist.index}"
                    value="{$ban.ban_id}">
            </td>
            {/if}
            <td height="16" align="center" class="listtable_1">
                {$ban.mod_icon}
            </td>
            <td height="16" align="center" class="listtable_1">
                {$ban.ban_date}
            </td>
            <td height="16" class="listtable_1">
                <div style="float:left;">
                {if empty($ban.player)}
                    <i><font color="#677882">{t section="modules/bans/main" message="no nickname present"}</font></i>
                {else}
                    {$ban.player|escape:'html'|stripslashes}
                {/if}
                </div>
                {if $ban.demo_available}
                <div style="float:right;">
                    <img src="images/demo.gif" alt="Demo" title="Demo available" style="height:14px;width:14px;" />
                </div>
                {/if}
                {if $view_comments && $ban.commentdata && $ban.commentdata|@count > 0}
                <div style="float:right;">
                    {$ban.commentdata|@count} 
                    <img
                        src="images/details.gif"
                        alt="{t section="modules/bans/main" message="Comments"}"
                        title="{t section="modules/bans/main" message="Comments"}"
                        style="height:12px;width:12px;" />
                </div>
                {/if}
            </td>
            {if !$hideadminname}
            <td height="16" class="listtable_1">
            {if !empty($ban.admin)}
                {$ban.admin|escape:'html'}
            {else}
                <i><font color="#677882">{t section="modules/bans/main" message="Admin deleted"}</font></i>
            {/if}
            </td>
            {/if}
            <td width="20%" height="16" align="center" class="{$ban.class}">
                {$ban.banlength}
            </td>
		</tr>
		<!-- ###############[ Start Sliding Panel ]################## -->
		<tr>
            <td colspan="7" align="center">
                <div class="opener"> 
                    <table width="80%" cellspacing="0" cellpadding="0" class="listtable">
                        <tr>
                            <td height="16" align="left" class="listtable_top" colspan="3">
                                <b>{t section="modules/bans/main" message="Ban Details"}</b>            
                            </td>
                        </tr>
                        <tr align="left">
                            <td width="20%" height="16" class="listtable_1">
                                {t section="modules/bans/main" message="Player"}
                            </td>
                            <td height="16" class="listtable_1">
                                {if empty($ban.player)}
                                    <i><font color="#677882">{t section="modules/bans/main" message="no nickname present"}</font></i>
                                {else}
                                    {$ban.player|escape:'html'|stripslashes}
                                {/if}
                            </td>
                            <!-- ###############[ Start Admin Controls ]################## -->
                            {if $view_bans}
                            <td width="30%" rowspan="{if $ban.unbanned}14{else}12{/if}" class="listtable_2 opener">
                                <div class="ban-edit">
                                    <ul>
                                      {if $ban.unbanned && $ban.reban_link != false}
                                      <li>{$ban.reban_link}</li>
                                      {/if}
                                      <li>{$ban.demo_link}</li>
                                      <li>{$ban.addcomment}</li>
                                      {if $ban.type == 0}
                                      {if $groupban}
                                      <li>{$ban.groups_link}</li>
                                      {/if}
                                      {if $friendsban}
                                      <li>{$ban.friend_ban_link}</li>
                                      {/if}
                                      {/if}
                                      {if ($ban.view_edit && !$ban.unbanned)} 
                                      <li>{$ban.edit_link}</li>
                                      {/if}
                                      {if ($ban.unbanned == false && $ban.view_unban)}
                                      <li>{$ban.unban_link}</li>
                                      {/if}
                                      {if $ban.view_delete}
                                      <li>{$ban.delete_link}</li>
                                      {/if}
                                    </ul>
                                </div>
                            </td>
                            {else}
                            <td width="30%" rowspan="{if $ban.unbanned}13{else}11{/if}" class="listtable_2 opener">
                                <div class="ban-edit">
                                    <ul>
                                        <li>{$ban.demo_link}</li>
                                    </ul>
                                </div>
                            </td>
                            {/if}
                            <!-- ###############[ End Admin Controls ]##################### -->
                        </tr>
                        {if NOT $hidesteamid}
                            <tr align="left">
                                <td width="20%" height="16" class="listtable_1">
                                    {t section="modules/bans/main" message="Steam ID"}
                                </td>
                                <td height="16" class="listtable_1">
                                    {if empty($ban.steamid)}
                                        <i><font color="#677882">{t section="modules/bans/main" message="no Steam ID present"}</font></i>
                                    {else}
                                        {$ban.steamid}
                                    {/if}
                                </td>
                            </tr>
                            {if $ban.type == 0}
                                <tr align="left">
                                    <td width="20%" height="16" class="listtable_1">
                                        {t section="modules/bans/main" message="Steam Community"}
                                    </td>
                                    <td height="16" class="listtable_1">
                                        <a href="http://steamcommunity.com/profiles/{$ban.communityid}" target="_blank">
                                            {$ban.communityid}
                                        </a>
                                    </td>
                                </tr>
                            {/if}
                        {/if}
                        {if !$hideplayerips}
                        <tr align="left">
                            <td width="20%" height="16" class="listtable_1">
                                {t section="modules/bans/main" message="IP address"}
                            </td>
                            <td height="16" class="listtable_1">
                                {if !$ban.ip}
                                    <i><font color="#677882">{t section="modules/bans/main" message="no IP address present"}</font></i>
                                {else}
                                    {$ban.ip}
                                {/if}
                            </td>
                        </tr>
                        {/if}
                        <tr align="left">
                            <td width="20%" height="16" class="listtable_1">
                                {t section="modules/bans/main" message="Invoked on"}
                            </td>
                            <td height="16" class="listtable_1">
                                {$ban.ban_date}
                            </td>
                        </tr>
                        <tr align="left">
                            <td width="20%" height="16" class="listtable_1">
                                {t section="modules/bans/main" message="Banlength"}
                            </td>
                            <td height="16" class="listtable_1">
                                {$ban.banlength}
                            </td>
                        </tr>
                        {if $ban.unbanned}
                        <tr align="left">
                            <td width="20%" height="16" class="listtable_1">
                                {t section="modules/bans/main" message="Unban reason"}
                            </td>
                            <td height="16" class="listtable_1">
                            {if $ban.ureason == ""}
                                <i><font color="#677882">{t section="modules/bans/main" message="no reason present"}</font></i>
                            {else}
                                {$ban.ureason}
                            {/if}
                            </td>
                        </tr>
                        <tr align="left">
                            <td width="20%" height="16" class="listtable_1">
                                {t section="modules/bans/main" message="Unbanned by Admin"}
                            </td>
                            <td height="16" class="listtable_1">
                                {if !empty($ban.removedby)}
                                    {$ban.removedby|escape:'html'}
                                {else}
                                    <i><font color="#677882">{t section="modules/bans/main" message="Admin deleted"}</font></i>
                                {/if}
                            </td>
                        </tr>
                        {/if}
                        <tr align="left">
                            <td width="20%" height="16" class="listtable_1">
                                {t section="modules/bans/main" message="Expires on"}
                            </td>
                            <td height="16" class="listtable_1">
                                {if $ban.expiresBool}
                                    <i><font color="#677882">{t section="modules/bans/main" message="Not applicable"}</font></i>
                                {else}
                                    {$ban.expires}
                                {/if}
                            </td>
                        </tr>
                        <tr align="left">
                            <td width="20%" height="16" class="listtable_1">
                                {t section="modules/bans/main" message="Reason"}
                            </td>
                            <td height="16" class="listtable_1">
                                {$ban.reason|escape:'html'}
                            </td>
                        </tr>
                        {if !$hideadminname}
                        <tr align="left">
                            <td width="20%" height="16" class="listtable_1">
                                {t section="modules/bans/main" message="Banned by Admin"}
                            </td>
                            <td height="16" class="listtable_1">
                                {if !empty($ban.admin)}
                                    {$ban.admin|escape:'html'}
                                {else}
                                    <i><font color="#677882">{t section="modules/bans/main" message="Admin deleted"}</font></i>
                                {/if}
                            </td>
                        </tr>
                        {/if}
                        <tr align="left">
                            <td width="20%" height="16" class="listtable_1">
                                {t section="modules/bans/main" message="Banned from"}
                            </td>
                            <td height="16" class="listtable_1" id="ban_server_{$ban.ban_id}">
                                {if $ban.server_id == 0}
                                    {t section="modules/bans/main" message="Web Ban"}
                                {else}
                                    {t section="modules/bans/main" message="Please Wait..."}
                                {/if}
                            </td>
                        </tr>
                        <tr align="left">
                            <td width="20%" height="16" class="listtable_1">
                                {t section="modules/bans/main" message="Total Bans"}
                            </td>
                            <td height="16" class="listtable_1">
                                {$ban.prevoff_link}
                            </td>
                        </tr>
                        <tr align="left">
                            <td width="20%" height="16" class="listtable_1">
                                {t section="modules/bans/main" message="Blocked ([[blockcount]])" blockcount=$ban.blockcount}
                            </td>
                            <td height="16" class="listtable_1">
                            {if $ban.banlog == ""}
                                <i><font color="#677882">{t section="modules/bans/main" message="never"}</font></i>
                            {else}
                                {$ban.banlog}
                            {/if}
                            </td>
                        </tr>
                        {if $view_comments}
                        <tr align="left">
                            <td width="20%" height="16" class="listtable_1">
                                {t section="modules/bans/main" message="Comments"}
                            </td>
                            <td height="60" class="listtable_1" colspan="2">
                                {if $ban.commentdata}
                                <table width="100%" border="0">
                                    {foreach from=$ban.commentdata item=commenta}
                                    {if $commenta.morecom}
                                    <tr>
                                        <td colspan="3">
                                            <hr>
                                        </td>
                                    </tr>
                                    {/if}
                                    <tr>
                                        <td>
                                            {if !empty($commenta.comname)}
                                                <b>{$commenta.comname|escape:'html'}</b>
                                            {else}
                                                <i><font color="#677882">{t section="modules/bans/main" message="Admin deleted"}</font></i>
                                            {/if}
                                        </td>
                                        <td align="right">
                                            <b>{$commenta.added}</b>
                                        </td>
                                        {if $commenta.editcomlink != ""}
                                        <td align="right">
                                            {$commenta.editcomlink} {$commenta.delcomlink}
                                        </td>
                                        {/if}
                                    </tr>
                                    <tr>
                                        <td colspan='3'>
                                            {$commenta.commenttxt}
                                        </td>
                                    </tr>
                                    {if !empty($commenta.editData)}
                                    <tr>
                                        <td colspan='3'>
                                            <span style="font-size:6pt;color:grey;">
                                                {$commenta.editData}
                                            </span>
                                        </td>
                                    </tr>
                                    {/if}
                                    {/foreach}
                                </table>
                                {else}
                                    {t section="modules/bans/main" message="None"}
                                {/if}
                            </td>
                        </tr>
                        {/if}
                    </table>	
                </div>
            </td>
        </tr>
        <!-- ###############[ End Sliding Panel ]################## -->
		{/foreach}
	</table>
	{if $general_unban || $can_delete}
	&nbsp;&nbsp;L&nbsp;&nbsp;
    <a
        href="#"
        onclick="TickSelectAll();return false;"
        title="Select All"
        name="tickswitchlink"
        id="tickswitchlink">
        {t section="modules/bans/main" message="Select All"}
    </a>
    &nbsp;&nbsp;|&nbsp;
	<select name="bulk_action" id="bulk_action" onchange="BulkEdit(this,'{$admin_postkey}');">
		<option value="-1">
            {t section="modules/bans/main" message="Action"}
        </option>
		{if $general_unban}
		<option value="U">
            {t section="modules/bans/main" message="Unban"}
        </option>
		{/if}
		{if $can_delete}
		<option value="D">
            {t section="modules/bans/main" message="Delete"}
        </option>
		{/if}
	</select>
	<hr>
	{/if}
	{if $can_export }
		<a href="./exportbans.php?type=steam" title="{t section="modules/bans/main" message="Export Permanent SteamID Bans"}">
            {t section="modules/bans/main" message="Export Permanent SteamID Bans"}
        </a>
        &nbsp;&nbsp;|&nbsp;
		<a href="./exportbans.php?type=ip" title="{t section="modules/bans/main" message="Export Permanent IP Bans"}">
            {t section="modules/bans/main" message="Export Permanent IP Bans"}
        </a>
	{/if}
</div>
{/if}
<script type="text/javascript">
{literal}
    window.addEvent('domready', function(){	
        InitAccordion('tr.opener', 'div.opener', 'mainwrapper');
{/literal}
{if $view_bans}
        $('tickswitch').value=0;
{/if}
    
{literal}
    });
    function ProcessComment()
    {
        var err = 0;
        if($('commenttext').value == "") {
{/literal}
            $('commenttext.msg').setHTML('{t section="modules/bans/main" message="You have to type your comment"}');
{literal}
            $('commenttext.msg').setStyle('display', 'block');
            err++;
        } else {
            $('commenttext.msg').setHTML('');
            $('commenttext.msg').setStyle('display', 'none');
            err = 0;
        }

        if(err) {
            return 0;
        }

        if($('cid').value == -1) {
            xajax_AddComment($('bid').value,
                         $('ctype').value,
                         $('commenttext').value,
                         $('page').value);
        } else {
            xajax_EditComment($('cid').value,
                         $('ctype').value,
                         $('commenttext').value,
                         $('page').value);
        }
    }
    function TickSelectAll()
    {
        for(var i=0;$('chkb_' + i);i++) {
            if($('tickswitch').value==0) {
                $('chkb_' + i).checked = true;
            } else {
                $('chkb_' + i).checked = false;
            }
        }
        if($('tickswitch').value==0) {
    {/literal}
            $('tickswitch').value=1;
            $('tickswitch').setProperty('title', '{t section="modules/bans/main" message="Deselect All"}');
            $('tickswitchlink').setProperty('title', '{t section="modules/bans/main" message="Deselect All"}');
            $('tickswitchlink').innerHTML = '{t section="modules/bans/main" message="Deselect All"}';
    {literal}
        } else {
    {/literal}
            $('tickswitch').value=0;
            $('tickswitch').setProperty('title', '{t section="modules/bans/main" message="Select All"}');
            $('tickswitchlink').setProperty('title', '{t section="modules/bans/main" message="Select All"}');
            $('tickswitchlink').innerHTML = '{t section="modules/bans/main" message="Select All"}';
    {literal}
        }
    }
    function BulkEdit(action, bankey)
    {
        option = action.options[action.selectedIndex].value;
        ids = new Array();
        for(var i=0;$('chkb_' + i);i++)
        {
            if($('chkb_' + i).checked===true)
                ids.push($('chkb_' + i).value);
        }
        switch(option)
        {
            case "U":
    {/literal}
                UnbanBan(ids, bankey, "", '{t section="modules/bans/main" message="Bulk Unban"}', "1", "true");
    {literal}
            break;
            case "D":
    {/literal}
                RemoveBan(ids, bankey, "", '{t section="modules/bans/main" message="Bulk Delete"}', "0", "true");
    {literal}
            break;
        }
    }
    {/literal}
</script>