{if NOT $permission_listmods}
    {t section="admin/mods" message="Access Denied!"}
{else}
	<h3>
        {t section="admin/mods" message="Server Mods ([[mod_count]])" mod_count=$mod_count}
    </h3>
	<table width="100%" cellpadding="1">
		<tr>
			<td width="35%" height="16" class="front-module-header">
                <strong>{t section="admin/mods" message="Mod Name"}</strong>
            </td>
			<td width="25%" height="16" class="front-module-header">
                <strong>{t section="admin/mods" message="Mod Folder"}</strong>
            </td>
			<td width="10%" height="16" class="front-module-header">
                <strong>{t section="admin/mods" message="Mod Icon"}</strong>
            </td>
			<td height="16" class="front-module-header">
                <strong>
                    <span title="SteamID Universe (X of STEAM_X:Y:Z)">SU</span>
                </strong>
            </td>
			{if $permission_editmods || $permission_deletemods}
			<td height="16" class="front-module-header" style="text-align: center">
                <strong>{t section="admin/mods" message="Action"}</strong>
            </td>
			{/if}
		</tr>
		{foreach from="$mod_list" item="mod" name="gaben"}
			<tr id="mid_{$mod.mid}">
				<td style="border-bottom: solid 1px #ccc" height="16">{$mod.name|htmlspecialchars}</td>
				<td style="border-bottom: solid 1px #ccc" height="16">{$mod.modfolder|htmlspecialchars}</td>
				<td style="border-bottom: solid 1px #ccc" height="16"><img src="images/games/{$mod.icon}" width="16"></td>
				<td style="border-bottom: solid 1px #ccc" height="16">{$mod.steam_universe|htmlspecialchars}</td>
				{if $permission_editmods || $permission_deletemods}
				<td style="border-bottom: solid 1px #ccc; text-align: center" height="16">
					{if $permission_editmods}
					<a href="index.php?m=servers&p=admin&c=editmod&id={$mod.mid}">
                        {t section="admin/mods" message="Edit"}
                    </a> - 
					{/if}
					{if $permission_deletemods}
					<a href="#" onclick="RemoveMod('{$mod.name|escape:'quotes'|htmlspecialchars}', '{$mod.mid}');">
                        {t section="admin/mods" message="Delete"}
                    </a>
					{/if}
				</td>
				{/if}
			</tr>
		{/foreach}
	</table>
    <script>
        {literal}
        function RemoveMod(name, id)
        {
        {/literal}
            var noPerm = confirm('{t section="admin/mods" message="Are you sure you want to delete this mod?"}');
        {literal}
            if(noPerm == false)
                return;
            xajax_RemoveMod(id);
        }
        {/literal}
    </script>
{/if}
