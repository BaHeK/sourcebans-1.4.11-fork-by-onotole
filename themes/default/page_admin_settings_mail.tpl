<form action="" method="post">
    {t section="admin/settings" message="For more information or help regarding a certain subject move your mouse over the question mark."}
	<input type="hidden" name="settingsGroup" value="emailsettings" />
	<table width="99%" border="0" style="border-collapse:collapse;" id="group.details" cellpadding="3">
        <tr>
            <td valign="top" colspan="2">
                <h3>{t section="admin/settings" message="Email Settings"}</h3>
            </td>
        </tr>

        <tr>
            <td valign="top">
                <div class="rowdesc">
                    {help_icon
                        title="Email send transport"
                        message="Select the method of sending email messages"
                        translate="admin/settings"
                    }
                    {t section="admin/settings" message="Email transport"}
                </div>
            </td>
            <td>
                <div align="left">
                    <select TABINDEX=4 name="email_transport" id="sel_email_transport" class="inputbox">
                        <option value="mail"{if $email.transport == 'mail'} selected{/if}>PHP mail</option>
                        <option value="smtp"{if $email.transport == 'smtp'} selected{/if}>SMTP</option>
                    </select>
                </div>
            </td>
        </tr>
        {if $email.transport == 'smtp'}
        <tr>
            <td valign="top">
                <div class="rowdesc">
                    {help_icon title="SMTP host" message="Enter the address of the SMTP server" translate="admin/settings"}
                    {t section="admin/settings" message="SMTP host"}
                </div>
            </td>
            <td>
                <div align="left">
                    <input type="text" TABINDEX=6 name="email_smtl_host" id="email_smtl_host" class="submit-fields" value="{$email.smtphost}" />
                </div>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <div class="rowdesc">
                    {help_icon title="SMTP port" message="Enter the port of the SMTP server" translate="admin/settings"}
                    {t section="admin/settings" message="SMTP port"}
                </div>
            </td>
            <td>
                <div align="left">
                    <input type="text" TABINDEX=6 name="email_smtp_port" id="email_smtp_port" class="submit-fields" value="{$email.smtpport}" />
                </div>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <div class="rowdesc">
                    {help_icon title="SMTP secure" message="Select secure method" translate="admin/settings"}
                    {t section="admin/settings" message="SMTP secure"}
                </div>
            </td>
            <td>
                <div align="left">
                    
                    <select TABINDEX=6 name="email_smtp_secure" id="email_smtp_secure" class="inputbox">
                        <option value=""{if NOT $email.smtpsecure} selected{/if}>{t section="admin/settings" message="No secure"}</option>
                        <option value="ssl"{if $email.smtpsecure == 'ssl'} selected{/if}>ssl</option>
                        <option value="tls"{if $email.smtpsecure == 'tls'} selected{/if}>tls</option>
                    </select>
                </div>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <div class="rowdesc">
                    {help_icon title="SMTP username" message="Enter username" translate="admin/settings"}
                    {t section="admin/settings" message="Username"}
                </div>
            </td>
            <td>
                <div align="left">
                    <input type="text" TABINDEX=6 name="email_smtp_login" id="email_smtp_login" class="submit-fields" value="{$email.smtpuser}" />
                </div>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <div class="rowdesc">
                    {help_icon title="SMTP password" message="Enter password" translate="admin/settings"}
                    {t section="admin/settings" message="Password"}
                </div>
            </td>
            <td>
                <div align="left">
                    <input type="password" TABINDEX=6 name="email_smtp_password" id="email_smtp_password" class="submit-fields" value="{$email.smtppassword}" />
                </div>
            </td>
        </tr>
        {/if}
        
        <tr>
            <td valign="top">
                <div class="rowdesc">
                    {help_icon title='From name' message='This name will be indicated in the "From" field in the sent letters' translate="admin/settings"}
                    {t section="admin/settings" message='From name'}
                </div>
            </td>
            <td>
                <div align="left">
                    <input type="text" TABINDEX=6 name="email_from_name" id="email_from_name" class="submit-fields" value="{$email.emailfromname}" />
                </div>
            </td>
        </tr>
        
        <tr>
            <td valign="top">
                <div class="rowdesc">
                    {help_icon title='From Email' message='This email will be indicated in the "From" field in the sent letters' translate="admin/settings"}
                    {t section="admin/settings" message='From Email'}
                </div>
            </td>
            <td>
                <div align="left">
                    <input type="text" TABINDEX=6 name="email_from_email" id="email_from_email" class="submit-fields" value="{$email.emailfromemail}" />
                </div>
            </td>
        </tr>
        <tr>
			<td>&nbsp;</td>
		    <td>
		      {sb_button text="Save Changes" class="ok" id="asettings" submit=true translate="admin/settings"}
		      &nbsp;
		      {sb_button text="Back" class="cancel" id="aback" translate="admin/settings"}
			</td>
		</tr>
    </table>
</form>