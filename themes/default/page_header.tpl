<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>SourceBans{if $header_title} :: {$header_title}{/if}</title>
    <link rel="Shortcut Icon" href="./images/favicon.ico" />
    <script type="text/javascript" src="./scripts/sourcebans.js"></script>
    <script type="text/javascript">
        {$rehashJs}
    </script>
    <link href="themes/{$theme_name}/css/style.css" rel="stylesheet" type="text/css" />
    <!--[if IE 6]>
    <link href="themes/{$theme_name}/css/ie6.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <script type="text/javascript" src="./scripts/mootools.js"></script>
    <script type="text/javascript" src="./scripts/contextMenoo.js"></script>
    {$xajax_functions}
</head>
<body>
<div id="mainwrapper">
	<div id="header">
		<div id="head-logo">
    		<a href="index.php">
    			<img src="images/{$header_logo}" border="0" alt="{t section="layout" message="SourceBans Logo"}" />
    		</a>
		</div>
		<div id="head-userbox">
            {t section="layout" message="Welcome [[username]]" username=$username}
            {if $logged_in}
                (<a href='index.php?p=logout'>{t section="layout" message="Logout"}</a>)
                <br />
                <div style="width: 45%; float: left">
                    <a href='index.php?p=account'>{t section="layout" message="Your account"}</a>
                </div>
            {else}
                <br />
                <div style="width: 45%; float: left">
                    (<a href='index.php?p=login'>{t section="layout" message="Login"}</a>)
                </div>
            {/if}
            {if $canUserChangeLanguage}
            <div style="width: 45%; float: right; text-align: right">
                <select name="userLanguage" id="userLanguage" onchange="xajax_SelectLanguage(this.value);">
                {foreach from=$languages key='key' item='value'}
                    <option value="{$key}"{if $key == $currentLang} selected{/if}>{$value}</option>
                {/foreach}
                </select>
            </div>
            {/if}
            <div style="clear: both"></div>
		</div>
	</div>     
	<div id="tabsWrapper">
        <div id="tabs">
            <ul>
         