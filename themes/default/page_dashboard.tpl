<div class="front-module-intro">
	<table width="100%" cellpadding="1">
		<tr>
			<td colspan="3">
				<h3>{$dashboard_title}</h3>		
			</td>
		</tr>
		<tr>
			<td>
				{$dashboard_text}
			</td>
		</tr>
	</table>
</div>

<div id="front-servers">
    <h3>{t section="servers" message="Servers List"}</h3>
    <table width="98%" cellspacing="0" cellpadding="0" align="center" class="sortable listtable" style="margin-top:3px;">
        <thead>
            <tr>
                <td class="listtable_top" style="width: 2%; height: 16px; text-align: center">
                    {t section="servers" message="MOD"}
                </td>
                <td class="listtable_top" style="width: 2%; height: 16px; text-align: center">
                    {t section="servers" message="OS"}
                </td>
                <td class="listtable_top" style="width: 2%; height: 16px; text-align: center">
                    {t section="servers" message="VAC"}
                </td>
                <td class="listtable_top" style="height: 16px; text-align: center">
                    <b>{t section="servers" message="Hostname"}</b>
                </td>
                <td class="listtable_top" style="width: 10%; height: 16px;">
                    <b>{t section="servers" message="Players"}</b>
                </td>
                <td  class="listtable_top" style="width: 10%; height: 16px;">
                    <b>{t section="servers" message="Map"}</b>
                </td>
            </tr>
        </thead>
        <tbody>
            {foreach from=$server_list item="server"}
            <tr
                id="opener_{$server.sid}"
                class="tbl_out"
                onmouseout="this.className='tbl_out'"
                onmouseover="this.className='tbl_hover'"
                onclick="{$server.evOnClick}">
                <td height="16" align="center" class="listtable_1">
                    <img src="images/games/{$server.icon}" border="0" />
                </td>
                <td height="16" align="center" class="listtable_1" id="os_{$server.sid}"></td>
                <td height="16" align="center" class="listtable_1" id="vac_{$server.sid}"></td>
                <td height="16" class="listtable_1" id="host_{$server.sid}">
                    <i>{t section="servers" message="Querying Server Data..."}</i>
                </td>
                <td height="16" class="listtable_1" id="players_{$server.sid}">
                    {t section="servers" message="N/A"}
                </td>
                <td height="16" class="listtable_1" id="map_{$server.sid}">
                    {t section="servers" message="N/A"}
                </td>
            </tr>
        {/foreach}
        </tbody>
    </table>
</div>

<div class="front-module" style="float:left">
	<table width="100%" cellpadding="1" class="listtable">
		<tr>
			<td colspan="4">
				<table width="100%" cellpadding="0" cellspacing="0" class="front-module-header">
					<tr>
						<td align="left">
							{t section="home" message="Latest Added Bans"}
						</td>
						<td align="right">
                            {t section="home" message="Total bans: [[total_bans]]" total_bans=$total_bans}
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="16" class="listtable_top">
                {t section="home" message="MOD"}
            </td>
			<td width="25%" class="listtable_top" align="center">
                <strong>{t section="home" message="Date/Time"}</strong>
            </td>
			<td class="listtable_top" align="center">
                <strong>{t section="home" message="Name"}</strong>
            </td>
			<td width="23%" class="listtable_top">
                <strong>{t section="home" message="Length"}</strong>
            </td>
		</tr>
	    {foreach from=$players_banned item=player}
        <tr onclick="{$player.link_url}" onmouseout="this.className='tbl_out'" onmouseover="this.className='tbl_hover'" style="cursor:pointer;" height="16">
            <td class="listtable_1" align="center">
                <img src="images/games/{$player.icon}" alt="MOD" title="MOD" />
            </td>
            <td class="listtable_1">
                {$player.created}
            </td>
            <td class="listtable_1">
            {if empty($player.short_name)}
                <i><font color="#677882">{t section="home" message="no nickname present"}</font></i>
            {else}
                {$player.short_name|escape:'html'}
            {/if}
            </td>
            <td class="listtable_1{if $player.unbanned}_unbanned{/if}">
                {$player.length}{if $player.unbanned} ({$player.ub_reason}){/if}
            </td>
		</tr>
		{/foreach}
	</table>
</div>
    <div class="front-module" style="float:right">
	<table width="100%" cellpadding="1" class="listtable">
		<tr>
			<td colspan="3">
				<table width="100%" cellpadding="0" cellspacing="0" class="front-module-header">
					<tr>
						<td align="left">
							{t section="home" message="Latest Players Blocked"}
						</td>
						<td align="right">
                            {t section="home" message="Total Stopped: [[total_blocked]]" total_blocked=$total_blocked}
						</td>
					</tr>
				</table>
			</td>
		</tr>				
		<tr>
			<td width="16px" class="listtable_top">&nbsp;</td>
			<td width="25%" class="listtable_top" align="center">
                <b>{t section="home" message="Date/Time"}</b>
            </td>
			<td class="listtable_top">
                <b>{t section="home" message="Name"}</b>
            </td>	  
		</tr>
		{foreach from=$players_blocked item=player}
		<tr{if $dashboard_lognopopup} onclick="{$player.link_url}"{else} onclick="{$player.popup}"{/if}
            onmouseout="this.className='tbl_out'"
            onmouseover="this.className='tbl_hover'"
            style="cursor: pointer;"
            id="{$player.server}"
            title="Querying Server Data...">
            <td width="16" align="center" class="listtable_1">
                <img src="images/forbidden.png" alt="{t section="home" message="Blocked Player"}" />
            </td>
            <td width="25%" class="listtable_1">
                {$player.date}
            </td>
            <td class="listtable_1">
                {$player.short_name|escape:'html'}
            </td>
		</tr>
		{/foreach}
	</table>
</div>
<div style="clear: both"></div>