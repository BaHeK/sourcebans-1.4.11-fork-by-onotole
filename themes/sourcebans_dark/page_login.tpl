<div id="login"> 
	<div id="login-content">
	  	<div id="loginUsernameDiv">
	    	<label for="loginUsername">-{t section="login" message="Username:"}-</label><br />
	    	<input id="loginUsername" class="loginmedium" type="text" name="username"value="" />
		</div>
		<div id="loginUsername.msg" class="badentry"></div>
  		
		<div id="loginPasswordDiv">
	    	<label for="loginPassword">-{t section="login" message="Password:"}-</label><br />
	   		<input id="loginPassword" class="loginmedium" type="password" name="password" value="" />
		</div>
		<div id="loginPassword.msg" class="badentry"></div>
	  	
		<div id="loginRememberMeDiv">
	    	<input id="loginRememberMe" type="checkbox" class="checkbox" name="remember" value="checked" vspace="5px" />
            <span
                class="checkbox"
                style="cursor:pointer;"
                onclick="($('loginRememberMe').checked?$('loginRememberMe').checked=false:$('loginRememberMe').checked=true)">
                -{t section="login" message="Remember me"}-
            </span>
  		</div>
		
  		<div id="loginSubmit">	
			-{sb_button text="Login" class="ok" id="alogin" submit=false translate="login"}-
		</div>
		
		<div id="loginOtherlinks">
			<a href="?">-{t section="login" message="Back to the Homepage"}-</a> - 
            <a href="index.php?p=lostpassword">-{t section="login" message="Lost Your Password?"}-</a>
		</div>
	</div>
</div>
	
<script>
    function DoLogin(redir)
    {
        var err = 0;
        var nopw = 0;
        if(!$('loginUsername').value)
        {
            $('loginUsername.msg').setHTML('-{t section="login" message="You must enter your loginname!"}-');
            $('loginUsername.msg').setStyle('display', 'block');
            err++;
        } else {
            $('loginUsername.msg').setHTML('');
            $('loginUsername.msg').setStyle('display', 'none');
        }

        if(!$('loginPassword').value)
        {
            $('loginPassword.msg').setHTML('-{t section="login" message="You must enter your password!"}-');
            $('loginPassword.msg').setStyle('display', 'block');
            nopw = 1;
        } else {
            $('loginPassword.msg').setHTML('');
            $('loginPassword.msg').setStyle('display', 'none');
        }

        if(err)
            return 0;

        if(redir == "undefined")
            redir = "";
        
        xajax_Plogin(document.getElementById('loginUsername').value, 
            document.getElementById('loginPassword').value,
            document.getElementById('loginRememberMe').checked,
            redir,
            nopw
        );
    }
	$E('html').onkeydown = function(event){
	    var event2 = new Event(event);
	    if (event2.key == 'enter' ) {
            DoLogin('-{$redir}-');
        }
	};
    $('loginRememberMeDiv').onkeydown = function(event){
	    var event = new Event(event);
	    if (event.key == 'space' ) {
            $('loginRememberMeDiv').checked = true;
        }
	};
    $('button').onkeydown = function(event){
	    var event2 = new Event(event);
	    if (event2.key == 'space' ) {
            DoLogin('-{$redir}-');
        }
	};
    $('alogin').addEvent('click', function(){
        DoLogin('-{$redir}-');
    });
</script>