{if NOT $permission_addadmin}
	Access Denied!
{else}
	{if $overrides_error != ""}
		<script type="text/javascript">
            ShowBox("Error", "{$overrides_error}", "red");
        </script>
	{/if}
	{if $overrides_save_success}
		<script type="text/javascript">
            ShowBox(
                "{t section="admin/admins" message="Overrides updated"}",
                "{t section="admin/admins" message="The changes have been saved successfully."}",
                "green",
                "index.php?p=admin&c=admins"
            );
        </script>
	{/if}
	<div id="add-group">
		<form action="" method="post">
		<h3>{t section="admin/admins" message="Overrides"}</h3>
		{t section="admin/admins" message="With Overrides you can change the flags or permissions on any command, either globally, or for a specific group, without editing plugin source code."}
        <br />
		<i>
            {t section="admin/admins" message="Read about"} 
            <a 
                href="http://wiki.alliedmods.net/Overriding_Command_Access_%28SourceMod%29" 
                title="{t section="admin/admins" message="Overriding Command Access (SourceMod)"}" 
                target="_blank"><b>{t section="admin/admins" message="overriding command access"}</b>
            </a> {t section="admin/admins" message="in the AlliedModders Wiki!"}
        </i>
        <br />
        <br />
		{t section="admin/admins" message="Blanking out an overrides' name will delete it."}<br /><br />
		<table align="center" cellspacing="0" cellpadding="4" id="overrides" width="90%">
			<tr>
				<td class="tablerow4">{t section="admin/admins" message="Type"}</td>
				<td class="tablerow4">{t section="admin/admins" message="Name"}</td>
				<td class="tablerow4">{t section="admin/admins" message="Flags"}</td>
			</tr>
			{foreach from=$overrides_list item=override}
			<tr>
				<td class="tablerow1">
					<select name="override_type[]">
						<option{if $override.type == "command"} selected="selected"{/if} value="command">
                            {t section="admin/admins" message="Command"}
                        </option>
						<option{if $override.type == "group"} selected="selected"{/if} value="group">
                            {t section="admin/admins" message="Group"}
                        </option>
					</select>
					<input type="hidden" name="override_id[]" value="{$override.id}" />
				</td>
				<td class="tablerow1">
                    <input name="override_name[]" value="{$override.name|htmlspecialchars}" />
                </td>
				<td class="tablerow1">
                    <input name="override_flags[]" value="{$override.flags|htmlspecialchars}" />
                </td>
			</tr>
			{/foreach}
			<tr>
				<td class="tablerow1">
					<select name="new_override_type">
						<option value="command">{t section="admin/admins" message="Command"}</option>
						<option value="group">{t section="admin/admins" message="Group"}</option>
					</select>
				</td>
				<td class="tablerow1"><input name="new_override_name" /></td>
				<td class="tablerow1"><input name="new_override_flags" /></td>
			</tr>
		</table>
		<center>
			{sb_button text="Save" class="ok" id="oversave" submit=true translate="admin/admins"}
			{sb_button text="Back" onclick="history.go(-1)" class="cancel" id="oback"  translate="admin/admins"}
		</center>
		</form>
	</div>
{/if}
