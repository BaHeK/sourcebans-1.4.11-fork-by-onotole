<div>
<h3>{t section="servers" message="Servers List"}</h3>
    {if $IN_SERVERS_PAGE && $access_bans}
    <div style="text-align:right; width:100%;">
        <small>{t section="servers" message="Hint: Rightclick on a player to open a context menu with options to kick, ban or contact the player directly."}</small>
    </div>
    {/if}
    <table width="98%" cellspacing="0" cellpadding="0" align="center" class="sortable listtable" style="margin-top:3px;">
        <thead>
            <tr>
                <td class="listtable_top" style="width: 2%; height: 16px; text-align: center">
                    {t section="servers" message="MOD"}
                </td>
                <td class="listtable_top" style="width: 2%; height: 16px; text-align: center">
                    {t section="servers" message="OS"}
                </td>
                <td class="listtable_top" style="width: 2%; height: 16px; text-align: center">
                    {t section="servers" message="VAC"}
                </td>
                <td class="listtable_top" style="height: 16px; text-align: center">
                    <b>{t section="servers" message="Hostname"}</b>
                </td>
                <td class="listtable_top" style="width: 10%; height: 16px;">
                    <b>{t section="servers" message="Players"}</b>
                </td>
                <td  class="listtable_top" style="width: 10%; height: 16px;">
                    <b>{t section="servers" message="Map"}</b>
                </td>
            </tr>
         </thead>
         <tbody>
            {foreach from=$server_list item=server}
            <tr
                id="opener_{$server.sid}"
                class="opener tbl_out"
                style="cursor:pointer;"
                onmouseout="this.className='tbl_out'"
                onmouseover="this.className='tbl_hover'"
                {if !$IN_SERVERS_PAGE} onclick="{$server.evOnClick}"{/if}>
                <td height="16" align="center" class="listtable_1">
                    <img src="images/games/{$server.icon}" border="0" />
                </td>
                <td height="16" align="center" class="listtable_1" id="os_{$server.sid}"></td>
                <td height="16" align="center" class="listtable_1" id="vac_{$server.sid}"></td>
                <td height="16" class="listtable_1" id="host_{$server.sid}">
                    <i>{t section="servers" message="Querying Server Data..."}</i>
                </td>
                <td height="16" class="listtable_1" id="players_{$server.sid}">
                    {t section="servers" message="N/A"}
                </td>
                <td height="16" class="listtable_1" id="map_{$server.sid}">
                    {t section="servers" message="N/A"}
                </td>
              </tr>
              <tr>
                <td colspan="7" align="center">
                    {if $IN_SERVERS_PAGE}
                    <div class="opener">
                        <div id="serverwindow_{$server.sid}">
                            <div id="sinfo_{$server.sid}">
                                <table width="90%" border="0" class="listtable">
                                    <tr>
                                        <td class="listtable_2" valign="top">
                                            <table width="100%" border="0" class="listtable" id="playerlist_{$server.sid}" name="playerlist_{$server.sid}"></table>
                                        </td>
                                        <td width="355px" class="listtable_2 opener" valign="top">
                                            <img id="mapimg_{$server.sid}" height='255' width='340' src='images/maps/nomap.jpg'>
                                            <br />
                                            <br />
                                            <div align='center'>
                                                <b>{t section="servers" message="IP:Port - [[ip]]:[[port]]" ip=$server.ip port=$server.port}</b>
                                                <br />
                                                <input
                                                    type="submit"
                                                    onclick="document.location = 'steam://connect/{$server.ip}:{$server.port}'"
                                                    name="button"
                                                    class="btn game"
                                                    style="margin:0px;"
                                                    id="button"
                                                    value="{t section="servers" message="Connect"}" />
                                                <input
                                                    type="button"
                                                    onclick="refreshServerInfo({$server.sid});"
                                                    name="button"
                                                    class="btn refresh"
                                                    style="margin:0;"
                                                    id="button"
                                                    value="{t section="servers" message="Refresh"}" />
                                            </div>
                                            <br />
                                        </td>
                                  </tr>
                            </table>
                          </div>
                          <div id="noplayer_{$server.sid}" name="noplayer_{$server.sid}" style="display:none;">
                            <h3>{t section="servers" message="No players in the server"}</h3>
                            <div align="center">
                                <b>{t section="servers" message="IP:Port - [[ip]]:[[port]]" ip=$server.ip port=$server.port}</b>
                                <input
                                    type="submit"
                                    onclick="document.location = 'steam://connect/{$server.ip}:{$server.port}'"
                                    name="button"
                                    class="btn game"
                                    style="margin:0;"
                                    id="button"
                                    value="{t section="servers" message="Connect"}" />
                                <input
                                    type="button"
                                    onclick="refreshServerInfo({$server.sid});"
                                    name="button"
                                    class="btn refresh"
                                    style="margin:0;"
                                    id="button"
                                    value="{t section="servers" message="Refresh"}" />
                            </div>
                          </div>
                      </div>
                    </div>
                    {/if}

                    </td>
                </tr>
            {/foreach}
            </tbody>
        </table>
	</div>


{if $IN_SERVERS_PAGE}
	<script type="text/javascript">
		InitAccordion('tr.opener', 'div.opener', 'mainwrapper');
        {literal}
        function refreshServerInfo(sid) {
        {/literal}
            ShowBox(
                '{t section="servers" message="Reloading.."}',
                '{t section="servers" message="<b>Refreshing the Serverdata...</b><br><i>Please Wait!</i>"}',
                'blue',
                '',
                true
            );
            document.getElementById('dialog-control').setStyle('display', 'none');
            xajax_RefreshServer(sid);
        {literal}
        }
        {/literal}
	</script>
{/if}
