<div id="0" style="display:none;">
    {if NOT $permission_listgroups}
        {t section="admin/groups" message="Access Denied!"}
    {else}
        <h3>{t section="admin/groups" message="Groups"}</h3>
        {t section="admin/groups" message="Click on a group to view its permissions."}
        <br />
        <br />

        <!-- Web Admin Groups -->
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="4">
                    <table width="100%" cellpadding="0" cellspacing="0" class="front-module-header" class="listtable">
                        <tr>
                            <td align="left">
                                {t section="admin/groups" message="Web Admin Groups"}
                            </td>
                            <td align="right">
                                {t section="admin/groups" message="Total: [[web_group_count]]" web_group_count=$web_group_count}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="40%" height='16' class="listtable_top">
                    <strong>{t section="admin/groups" message="Group Name"}</strong>
                </td>
                <td width="25%" height='16' class="listtable_top">
                    <strong>{t section="admin/groups" message="Admins in group"}</strong>
                </td>
                <td width="30%" height='16' class="listtable_top">
                    <strong>{t section="admin/groups" message="Action"}</strong>
                </td>
            </tr>
            {foreach from="$web_group_list" item="group" name="web_group"}
                <tr 
                    id="gid_{$group.gid}" 
                    class="opener tbl_out" 
                    onmouseout="this.className='tbl_out'" 
                    onmouseover="this.className='tbl_hover'">
                    <td style="border-bottom: solid 1px #ccc" height='16'>{$group.name}</td>
                    <td style="border-bottom: solid 1px #ccc" height='16'>{$web_admins[$smarty.foreach.web_group.index]}</td>
                    <td style="border-bottom: solid 1px #ccc" height='16'> 
                        {if $permission_editgroup}
                            <a href="index.php?p=admin&c=groups&o=edit&type=web&id={$group.gid}">
                                {t section="admin/groups" message="Edit"}
                            </a>
                        {/if}
                        {if $permission_deletegroup}
                            - <a href="#" onclick="RemoveGroup({$group.gid}, '{$group.name}', 'web');">
                                {t section="admin/groups" message="Delete"}
                            </a>
                        {/if}
                    </td>
                </tr>
                <tr>	 
                    <td colspan="7" align="center">     	
                    <div class="opener"> 
                        <table width="80%" cellspacing="0" cellpadding="0" class="listtable">
                            <tr>
                                <td height="16" align="left" class="listtable_top" colspan="3">
                                    <b>{t section="admin/groups" message="Group Details"}</b>            
                                </td>
                            </tr>
                            <tr align="left">
                                <td width="20%" height="16" class="listtable_1">
                                    {t section="admin/groups" message="Permissions"}
                                </td>
                                <td height="16" class="listtable_1">
                                    {$group.permissions}
                                </td>
                            </tr>
                            <tr align="left">
                                <td width="20%" height="16" class="listtable_1">
                                    {t section="admin/groups" message="Members"}
                                </td>
                                <td height="16" class="listtable_1">
                                    <table width="100%" cellspacing="0" cellpadding="0" class="listtable">
                                    {foreach from=$web_admins_list[$smarty.foreach.web_group.index] item="web_admin"}
                                        <tr>
                                            <td width="60%" height="16" class="listtable_1">
                                                {$web_admin.user}
                                            </td>
                                            {if $permission_editadmin}
                                            <td width="20%" height="16" class="listtable_1">
                                                <a 
                                                    href="index.php?p=admin&c=admins&o=editgroup&id={$web_admin.aid}" 
                                                    title="{t section="admin/groups" message="Edit Groups"}">
                                                    {t section="admin/groups" message="Edit"}
                                                </a>
                                            </td>
                                            <td width="20%" height="16" class="listtable_1">
                                                <a 
                                                    href="index.php?p=admin&c=admins&o=editgroup&id={$web_admin.aid}&wg=" 
                                                    title="{t section="admin/groups" message="Remove From Group"}">
                                                    {t section="admin/groups" message="Remove"}
                                                </a>
                                            </td>
                                            {/if}
                                        </tr>
                                    {/foreach}
                                    </table>
                                </td>
                            </tr>
                        </table>		
                    </div>
                </td> 	
            </tr>        
            {/foreach}
        </table>
        <br /><br />

        <!-- Server Admin Groups -->
        <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="4">
                <table width="100%" cellpadding="0" cellspacing="0" class="front-module-header" class="listtable">
                    <tr>
                        <td align="left">
                            {t section="admin/groups" message="Server Admin Groups"}
                        </td>
                        <td align="right">
                            {t section="admin/groups" message="Total: [[server_admin_group_count]]" server_admin_group_count=$server_admin_group_count}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="40%" height='16' class="listtable_top">
                <strong>{t section="admin/groups" message="Group Name"}</strong>
            </td>
            <td width="25%" height='16' class="listtable_top">
                <strong>{t section="admin/groups" message="Admins in group"}</strong>
            </td>
            <td width="30%" height='16' class="listtable_top">
                <strong>{t section="admin/groups" message="Action"}</strong>
            </td>
        </tr>
        {foreach from=$server_group_list item="group" name="server_admin_group"}
            <tr 
                id="gid_{$group.id}" 
                class="opener tbl_out" 
                onmouseout="this.className='tbl_out'" 
                onmouseover="this.setProperty('class', 'tbl_hover')">
                <td style="border-bottom: solid 1px #ccc" height='16'>
                    {$group.name}
                </td>
                <td style="border-bottom: solid 1px #ccc" height='16'>
                    {$server_admins[$smarty.foreach.server_admin_group.index]}
                </td>
                <td style="border-bottom: solid 1px #ccc" height='16'> 
                    {if $permission_editgroup}
                        <a href="index.php?p=admin&c=groups&o=edit&type=srv&id={$group.id}">
                            {t section="admin/groups" message="Edit"}
                        </a>
                    {/if}
                    {if $permission_deletegroup}
                        - <a href="#" onclick="RemoveGroup({$group.id}, '{$group.name}', 'srv');">
                            {t section="admin/groups" message="Delete"}
                        </a>
                    {/if}
                </td>
            </tr>
            <tr>	 
                <td colspan="7" align="center">     	
                    <div class="opener"> 
                        <table width="80%" cellspacing="0" cellpadding="0" class="listtable">
                            <tr>
                                <td height="16" align="left" class="listtable_top" colspan="3">
                                    <b>{t section="admin/groups" message="Group Details"}</b>            
                                </td>
                            </tr>
                            <tr align="left">
                                <td width="20%" height="16" class="listtable_1">
                                    {t section="admin/groups" message="Permissions"}
                                </td>
                                <td height="16" class="listtable_1">
                                    {$group.permissions}
                                </td>
                            </tr>
                            <tr align="left">
                                <td width="20%" height="16" class="listtable_1">
                                    {t section="admin/groups" message="Members"}
                                </td>
                                <td height="16" class="listtable_1">
                                    <table width="100%" cellspacing="0" cellpadding="0" class="listtable">
                                    {foreach from=$server_admins_list[$smarty.foreach.server_admin_group.index] item="server_admin"}
                                        <tr>
                                            <td width="60%" height="16" class="listtable_1">
                                                {$server_admin.user}
                                            </td>
                                            {if $permission_editadmin}
                                            <td width="20%" height="16" class="listtable_1">
                                                <a 
                                                    href="index.php?p=admin&c=admins&o=editgroup&id={$server_admin.aid}" 
                                                    title="{t section="admin/groups" message="Edit Groups"}">
                                                    {t section="admin/groups" message="Edit"}
                                                </a>
                                            </td>
                                            <td width="20%" height="16" class="listtable_1">
                                                <a 
                                                    href="index.php?p=admin&c=admins&o=editgroup&id={$server_admin.aid}&sg=" 
                                                    title="{t section="admin/groups" message="Remove From Group"}">
                                                    {t section="admin/groups" message="Remove"}
                                                </a>
                                            </td>
                                            {/if}
                                        </tr>
                                    {/foreach}
                                    </table>
                                </td>
                            </tr>
                            <tr align="left">
                                <td width="20%" height="16" class="listtable_1">
                                    {t section="admin/groups" message="Overrides"}
                                </td>
                                <td height="16" class="listtable_1">
                                    <table width="100%" cellspacing="0" cellpadding="0" class="listtable">
                                        <tr>
                                            <td class="listtable_top">
                                                {t section="admin/groups" message="Type"}
                                            </td>
                                            <td class="listtable_top">
                                                {t section="admin/groups" message="Name"}
                                            </td>
                                            <td class="listtable_top">
                                                {t section="admin/groups" message="Access"}
                                            </td>
                                        </tr>
                                        {foreach from=$server_overrides_list[$smarty.foreach.server_admin_group.index] item="override"}
                                        <tr>
                                            <td width="60%" height="16" class="listtable_1">
                                                {$override.type}
                                            </td>
                                            <td width="60%" height="16" class="listtable_1">
                                                {$override.name|htmlspecialchars}
                                            </td>
                                            <td width="60%" height="16" class="listtable_1">
                                                {$override.access}
                                            </td>
                                        </tr>
                                        {/foreach}
                                    </table>
                                </td>
                            </tr>
                        </table>		
                    </div>
                </td> 	
            </tr>      
        {/foreach}
        </table>
        <br />
        <br />
        <!-- Server Groups -->
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="4">
                    <table width="100%" cellpadding="0" cellspacing="0" class="front-module-header">
                        <tr>
                            <td align="left">
                                {t section="admin/groups" message="Server Groups"}
                            </td>
                            <td align="right">
                                {t section="admin/groups" message="Total: [[server_group_count]]" server_group_count=$server_group_count}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="40%" height='16' class="listtable_top">
                    <strong>{t section="admin/groups" message="Group Name"}</strong>
                </td>
                <td width="25%" height='16' class="listtable_top">
                    <strong>{t section="admin/groups" message="Servers in group"}</strong>
                </td>
                <td width="30%" height='16' class="listtable_top">
                    <strong>{t section="admin/groups" message="Action"}</strong>
                </td>
            </tr>
            {foreach from="$server_list" item="group" name="server_group"}
                <tr 
                    id="gid_{$group.gid}" 
                    class="opener tbl_out" 
                    onmouseout="this.className='tbl_out'" 
                    onmouseover="this.setProperty('class', 'tbl_hover')">
                    <td style="border-bottom: solid 1px #ccc" height='16'>{$group.name}</td>
                    <td style="border-bottom: solid 1px #ccc" height='16'>{$server_counts}</td>
                    <td style="border-bottom: solid 1px #ccc" height='16'>   
                    {if $permission_editgroup}
                        <a href="index.php?p=admin&c=groups&o=edit&type=server&id={$group.gid}">
                            {t section="admin/groups" message="Edit"}
                        </a>
                    {/if}
                    {if $permission_deletegroup}
                        - <a href="#" onclick="RemoveGroup({$group.gid}, '{$group.name}', 'server');">
                            {t section="admin/groups" message="Delete"}
                        </a>
                    {/if}        
                    </td>
                </tr>
                <tr>	 
                    <td colspan="7" align="center">     	
                        <div class="opener"> 
                            <table width="80%" cellspacing="0" cellpadding="0" class="listtable">
                                <tr>
                                    <td height="16" align="left" class="listtable_top" colspan="3">
                                        <b>{t section="admin/groups" message="Servers in this group"}</b>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td width="20%" height="16" class="listtable_1">{t section="admin/groups" message="Server Names"}</td>
                                    <td height="16" class="listtable_1" id="servers_{$group.gid}">
                                        {t section="admin/groups" message="Please wait..."}
                                    </td>
                                </tr>
                            </table>		
                        </div>
                    </td> 	
                </tr> 
            {/foreach}
        </table>
    {/if}
</div>
<script type="text/javascript">
    {literal}
    function RemoveGroup(id, name, type)
    {
    {/literal}
        var noPerm = confirm('{t section="admin/groups" message="Are you sure you want to delete this group?"}');
    {literal}
        if(noPerm == false)
        {
            return;
        }
        xajax_RemoveGroup(id, type);
    }
    {/literal}
</script>