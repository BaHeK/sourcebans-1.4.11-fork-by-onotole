<div id="admin-page-content">
    <div id="add-group">
        <h3>{t section="admin/admins/edit" message="Web Admin Permissions"}</h3>
        <input type="hidden" id="admin_id" value="{$aid}">
        {$webperms}
        <br />
        <h3>{t section="admin/admins/edit" message="Server Admin Permissions"}</h3>
        {$srvperms}
        <table width="100%">
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr align="center">
                <td>&nbsp;</td>
                <td>
                    <div align="center">
                        {sb_button text="Save Changes" onclick="ProcessEditAdminPermissions();" class="ok" id="editadmingroup" translate="admin/admins/edit"}
                        &nbsp;
                        {sb_button text="Back" onclick="history.go(-1)" class="cancel" id="aback" translate="admin/admins/edit"}
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
<script>
    {literal}
    function ProcessEditAdminPermissions()
    {
        var Mask = BoxToMask();
        var srvMask = BoxToSrvMask();
        var aid = $('admin_id').value;

        if($('immunity')) {
            if(IsNumeric($('immunity').value)) {
                xajax_EditAdminPerms(aid, Mask, srvMask);
            } else {
    {/literal}
                ShowBox(
                    '{t section="admin/admins/edit" message="Error"}',
                    '{t section="admin/admins/edit" message="Immunity must be a numerical value (0-9)"}',
                    "red",
                    "",
                    true
                );
    {literal}
            }
        }else {
            xajax_EditAdminPerms(aid, Mask, srvMask);
        }
    }
    {/literal}
</script>