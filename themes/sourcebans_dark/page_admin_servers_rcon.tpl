-{if NOT $permission_rcon}-
	-{t section="admin/rcon" message="Access Denied!"}-
-{else}-
<div id="admin-page-content">
<div id="1">


<h3>-{t section="admin/rcon" message="RCON Console"}-</h3>
<div align="center" width="90%">
<div id="rcon" style="overflow:auto;
			background-color:#efefef;
			border: 1px solid #999;
			padding: 3px;
			height: 450px;
			width: 90%;" align="left">
    <pre><div id="rcon_con">**********************************************************************************<br />*&nbsp;-{t section="admin/rcon" message="SourceBans RCON console"}-<br>*&nbsp;-{t section="admin/rcon" message="Type your comand in the box below and hit enter"}-<br>*&nbsp;-{t section="admin/rcon" message="Type 'clr' to clear the console"}-<br />**********************************************************************************<br /></div></pre>
</div>
<br />
-{t section="admin/rcon" message="Command"}-: 
<input type="text" style="font-family:verdana, tahoma, arial;font-size:10px;width:500px" id="cmd"> 
<input type="button" onclick="SendRcon();" id="rcon_btn" value="-{t section="admin/rcon" message="Send"}-">
</div>
</div></div>
<script>

$E('html').onkeydown = function(event){
    var event = new Event(event);
    if (event.key == 'enter' ) {
        SendRcon();
    }
};

function SendRcon()
{
	xajax_SendRcon('-{$id}-', $('cmd').value, true);
	$('cmd').value='-{t section="admin/rcon" message="Executing, Please Wait..."}-';
    $('cmd').disabled='true';
    $('rcon_btn').disabled='true';
}
</script>
-{/if}-