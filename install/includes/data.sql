INSERT INTO `{prefix}_mods` (`mid`, `name`, `icon`, `modfolder`, `steam_universe`) VALUES
(1, 'Web', 'web.png', 'NULL', '0'),
(2, 'Half-Life 2 Deathmatch', 'hl2dm.png', 'hl2mp', '0'),
(3, 'Counter-Strike: Source', 'csource.png', 'cstrike', '0'),
(4, 'Day of Defeat: Source', 'dods.png', 'dod', '0'),
(5, 'Insurgency: Source', 'ins.gif', 'insurgency', '0'),
(6, 'Dystopia', 'dys.gif', 'dystopia_v1', '0'),
(7, 'Hidden: Source', 'hidden.png', 'hidden', '0'),
(8, 'Half-Life 2 Capture the Flag', 'hl2ctf.png', 'hl2ctf', '0'),
(9, 'Pirates Vikings and Knights II', 'pvkii.gif', 'pvkii', '0'),
(10, 'Perfect Dark: Source', 'pdark.gif', 'pdark', '0'),
(11, 'The Ship', 'ship.gif', 'ship', '0'),
(12, 'Fortress Forever', 'hl2-fortressforever.gif', 'FortressForever', '0'),
(13, 'Team Fortress 2', 'tf2.gif', 'tf', '0'),
(14, 'Zombie Panic', 'zps.gif', 'zps', '0'),
(15, "Garry's Mod", 'gmod.png', 'garrysmod', '0'),
(16, "Left 4 Dead", 'l4d.png', 'left4dead', '1'),
(17, "Left 4 Dead 2", 'l4d2.png', 'left4dead2', '1'),
(18, "CSPromod", 'cspromod.png', 'cspromod', '0'),
(19, "Alien Swarm", 'alienswarm.png', 'alienswarm', '0'),
(20, "E.Y.E: Divine Cybermancy", 'eye.png', 'eye', '0'),
(21, "Nuclear Dawn", 'nucleardawn.png', 'nucleardawn', '0'),
(22, "Counter-Strike: Global Offensive", 'csgo.png', 'csgo', '1');

UPDATE `{prefix}_mods` SET `mid` = '0' WHERE `name` = 'Web';

INSERT INTO `{prefix}_settings` (`setting`, `value`) VALUES
('dash.intro.text', '{dash_intro_text}'),
('dash.intro.title', '{dash_intro_title}'),
('dash.lognopopup', '0'),
('banlist.bansperpage', '30'),
('banlist.hideadminname', '0'),
('banlist.nocountryfetch', '0'),
('banlist.hideplayerips', '0'),
('bans.customreasons', ''),
('config.password.minlength', '4'),
('config.debug', '0 '),
('template.logo', 'logos/sb-large.png'),
('template.title', 'SourceBans'),
('config.enableprotest', '1'),
('config.enablesubmit', '1'),
('config.exportpublic', '0'),
('config.enablekickit', '1'),
('config.dateformat', ''),
('config.theme', 'default'),
('config.defaultpage', '1'),
('config.timezone', '0'),
('config.summertime', '0'),
('config.enablegroupbanning', '0'),
('config.enablefriendsbanning', '0'),
('config.enableadminrehashing', '1'),
('protest.emailonlyinvolved', '0'),
('config.version', '356'),

('config.language', '{language}'),
('banlist.hidesteamid', '1'),
('config.enableclientlanguage', '1'),
('config.email.transport', 'mail'),
('config.email.smtp.login', ''),
('config.email.smtp.password', ''),
('config.email.smtp.host', ''),
('config.email.smtp.port', '25'),
('config.email.from.email', 'noreply@example.com'),
('config.email.from.name', 'SourceBans'),
('config.email.smtp.secure', '');


INSERT INTO `{prefix}_admins` (`aid` ,	`user` , `authid` ,	`password` , `gid` , `email` ,	`validate` , `extraflags`, `immunity`)
VALUES (NULL , 'CONSOLE', 'STEAM_ID_SERVER', '', '0', '', NULL, '0', 0);

UPDATE `{prefix}_admins` SET `aid` = '0' WHERE `authid` = 'STEAM_ID_SERVER';

INSERT INTO `{prefix}_links` (`id`, `label`, `url`, `title`, `position`, `active`, `hideGuests`) VALUES
(1, 'Dashboard', 'index.php?p=home', 'This page shows an overview of your bans and servers.', 1, 1, 0),
(2, 'Ban List', 'index.php?p=banlist', 'All of the bans in the database can be viewed from here.', 2, 1, 0),
(3, 'Servers', 'index.php?p=servers', 'All of your servers and their status can be viewed here', 3, 1, 0),
(4, 'Submit a ban', 'index.php?p=submit', 'You can submit a demo or screenshot of a suspected cheater here. It will then be up for review by one of the admins', 4, 1, 0),
(5, 'Protest a ban', 'index.php?p=protest', 'Here you can protest your ban. And prove your case as to why you should be unbanned.', 5, 1, 0),
(6, 'Admin Panel', 'index.php?p=admin', 'This is the control panel for SourceBans where you can setup new admins, add new server, etc.', 6, 1, 1);