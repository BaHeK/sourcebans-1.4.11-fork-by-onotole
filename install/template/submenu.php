<?php
if(!defined("IN_SB")){
    echo "You should not be here. Only follow links!";
    die();
}

global $language;
$currentLang = $language->getLanguage();
$langs  = $language->getLanguages();
if(isset($_POST['sys_language'])) {
    $language->setClientLanguage($_POST['sys_language']);
    $protocol = strtolower( preg_replace('/\/[^\/]*$/', '', $_SERVER['SERVER_PROTOCOL']) );
    $url = $protocol . '://' .$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
    header("Location: " . $url);
}
?>
        </ul>
        <ul style="float: right">
            <form method="post" name="languageForm" id="languageForm" action="" onchange="this.submit();">
                <select id="sys_language" name="sys_language" class="submit-fields">
                    <?php foreach($langs as $lang):?>
                    <option value="<?php echo $lang?>"<?php if($lang == $currentLang) echo ' selected'?>><?php echo ucfirst($lang)?></option>
                    <?php endforeach;?>
                </select>
            </form>
        </ul>
        <div style="clear: both"></div>
    </div>
</div>
<div id="innerwrapper">
	<div id="navigation">
		<div id="nav">
		</div>
	</div>
	