<?php
if(!defined("IN_SB")) {
    echo "You should not be here. Only follow links!";
    die();
}
ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SourceBans</title>
<link href="includes/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="./scripts/mootools.js"></script>
<script type="text/javascript" src="./scripts/sourcebans.js"></script>
<link rel="Shortcut Icon" href="./images/favicon.ico">
</head>
<body>
<div id="mainwrapper">
	<div id="header">
		<div id="head-logo"><!-- Container for the logo, left aligned -->
            <img src="images/logos/sb-large.png" border="0" alt="SourceBans Logo" />
		</div>
	</div>     
	<div id="tabsWrapper">
        <div id="tabs" style="width: 100%">
          <ul style="float: left">
          
