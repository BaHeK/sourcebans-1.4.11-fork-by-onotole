<?php if(!defined("IN_SB")){echo "You should not be here. Only follow links!";die();} ?>
	
	</div></div>
	<div id="footer">
		<div id="gc">
            <?php echo t('footer', 'By')?> <a href="http://www.interwavestudios.com" target="_blank" class="footer_link">InterWave Studios</a>
        </div>
		<div id="sb"><br/>
            <a href="http://www.sourcebans.net" target="_blank">
                <img src="images/sb.png" alt="SourceBans" border="0" />
            </a>
            <br/>
            <div id="footqversion"><?php echo t('footer', 'Version')?> <?php echo SB_VERSION;?></div>
            <div id="footquote"><?php echo CreateQuote() ?></div>
		</div>
		<div id="sm">
            <?php echo t('footer', 'Powered by')?> 
            <a class="footer_link" href="http://www.sourcemod.net" target="_blank">SourceMod</a>
		</div>
	</div>
<?php
if(isset($_GET['debug']) && $_GET['debug'] == 1):?>
<h3><?php echo t('footer', 'Session Data')?></h3>
<pre><?php echo print_r($_SESSION, true);?></pre>
<h3><?php echo t('footer', 'Post Data')?></h3>
<pre><?php echo print_r($_POST, true);?></pre>
<h3><?php echo t('footer', 'Cookie Data')?></h3>
<pre><?php echo print_r($_COOKIE, true);?></pre>
<?php endif;?>
</div>
<script type="text/javascript">
    window.addEvent('domready', function() {
        var Tips2 = new Tips($$('.tip'), {
            initialize:function(){
                this.fx = new Fx.Style(this.toolTip, 'opacity', {duration: 300, wait: false}).set(0);
            },
            onShow: function(toolTip) {
                this.fx.start(1);
            },
            onHide: function(toolTip) {
                this.fx.start(0);
            }
        });
        var Tips4 = new Tips($$('.perm'), {
            className: 'perm'
        });
    });
</script>
<!--[if lt IE 7]>
<script defer type="text/javascript" src="./scripts/pngfix.js"></script>
<![endif]-->

</body>
</html>