<?php
return array(
    'To use this webpanel software, you are required to read and accept the following license. If you do not agree with the license, then go and make your own ban/admin system.<br /><br /> An explanation  of this license is available <a href="http://creativecommons.org/licenses/by-nc-sa/3.0" target="_blank">here</a>' => 'Чтобы использовать данное программное обеспечение, Вы должны прочитать и принять условия лицензионного соглашения. Полная и самая последняя версия текста соглашения находится по <a href="http://creativecommons.org/licenses/by-nc-sa/3.0" target="_blank">этой ссылке</a>',
    'I have read, and accept the license' => 'Я прочел соглашение и принимаю его',
    'If you do not accept the license, you may NOT install this web panel' => 'Если Вы не принимаете условия лицензии, то Вы не сможете использовать данное программное обеспечение',
    'Error' => 'Ошибка',
);