<?php
return array(
    'Error' => 'Ошибка',
    'There is some missing data. All feilds are required.' => 'Не все поля были заполнены',
    'Hover your mouse over the \'?\' buttons to see an explanation of the field.' => 'Наведите курсор на кнопку \'?\', чтобы увидеть пояснения поля.',
    'Type the database information for the AMXBans mysql server you wish to import from.' => 'Введите данные от базы данных AmxBans',
);
