<?php  
/**
 * =============================================================================
 * Page footer
 * 
 * @author SteamFriends Development Team
 * @version 1.0.0
 * @copyright SourceBans (C)2007 SteamFriends.com.  All rights reserved.
 * @package SourceBans
 * @link http://www.sourcebans.net
 * 
 * @version $Id: footer.php 219 2009-02-24 21:09:11Z peace-maker $
 * =============================================================================
 */

if(!defined("IN_SB")){echo "You should not be here. Only follow links!";die();} 

global $theme, $language;
$theme->assign('SB_REV', 		defined('SB_SVN')?" Rev: ".GetSVNRev():'');
$theme->assign('SB_VERSION', 	SB_VERSION);
$theme->assign('SB_QUOTE', 		CreateQuote());
$theme->display('page_footer.tpl');

if (isset($_GET['p'])) {
    $_SESSION['p'] = $_GET['p'];
}
if (isset($_GET['c'])) {
    $_SESSION['c'] = $_GET['c'];
}
if (isset($_GET['p']) && $_GET['p'] != "login") {
    $_SESSION['q'] = $_SERVER['QUERY_STRING'];
}

if(defined('DEVELOPER_MODE')) {
    global $start;
    $time = microtime();
    $time = explode(" ", $time);
    $time = $time[1] + $time[0];
    $finish = $time;
    $totaltime = ($finish - $start);
    
    echo '<h3>';
    echo t('layout', 'Page took [[totaltime]] seconds to load.', array(
        '[[totaltime]]' => sprintf('%f', $totaltime)
    ));
    echo '</h3>';
    
    echo '<h3>'.t('layout', 'User Manager Data').'</h3>';
    PrintArray($userbank); 
    
    echo '<h3>'.t('layout', 'Post Data').'</h3>';
    PrintArray($_POST); 
    
    echo '<h3>'.t('layout', 'Session Data').'</h3>'; 
    PrintArray($_SESSION);
    
    echo '<h3>'.t('layout', 'Cookie Data').'</h3>'; 
    PrintArray($_COOKIE);
    
    echo '<h3>'.t('layout', 'Language Data').'</h3>'; 
    PrintArray($language->getFiles());
}
?>
</div>

<script type="text/javascript">
    
    function ProcessAdminTabs()
    {
        var url = window.location.toString();
        var pos = url.indexOf('^')+1;
        var tabNo = url.charAt(pos);
        SwapPane(tabNo);

        var upos = url.indexOf('~')+1;
        var utabNo = url.charAt(upos+1);
        var utabType = url.charAt(upos);
        Swap2ndPane(utabNo, utabType);

        if(parseInt(pos) == 0)
        {
            return -1;
        }
        else
        {
            return tabNo;
        }
    }
    
    var settab = ProcessAdminTabs();
    window.addEvent('domready', function(){	
        <?php    if (isset($GLOBALS['server_qry'])) {
            echo $GLOBALS['server_qry'];
        }
        ?>	

        var Tips2 = new Tips($$('.tip'), {
            initialize:function(){
                this.fx = new Fx.Style(this.toolTip, 'opacity', {duration: 300, wait: false}).set(0);
            },
            onShow: function(toolTip) {
                this.fx.start(1);
            },
            onHide: function(toolTip) {
                this.fx.start(0);
            }
        });
        var Tips4 = new Tips($$('.perm'), {
            className: 'perm'
        });
    }); 
    <?php if (isset($GLOBALS['NavRewrite'])):?>
    $('nav').setHTML('<?php echo $GLOBALS['NavRewrite']?>');
    <?php endif;?>
	
	<?php if(isset($GLOBALS['enable'])):?>
	if($('<?php echo $GLOBALS['enable']?>'))
	{
		if(settab != -1)
			$(settab).setStyle('display', 'block');
		else
			$('<?php echo $GLOBALS['enable']?>').setStyle('display', 'block');
		
	}
	<?php endif;
	if(isset($_GET['o']) && $_GET['o'] == "rcon"):?>
		var scroll = new Fx.Scroll($('rcon'),{
            duration: 500,
            transition: Fx.Transitions.Cubic.easeInOut
        });	
		if(scroll) {
            scroll.toBottom();
        }
	<?php endif;?>
	
	</script>
<?php if (isset($log) && is_object($log)) {
    $log->WriteLogEntries();
}
?>
<!--[if lt IE 7]>
<script defer type="text/javascript" src="./scripts/pngfix.js"></script>
<![endif]-->

</body>
</html>