<?php
/**
 * =============================================================================
 * SourceBans Webkick feature
 * 
 * @author SteamFriends Development Team
 * @version 1.0.0
 * @copyright SourceBans (C)2007 SteamFriends.com.  All rights reserved.
 * @package SourceBans
 * @link http://www.sourcebans.net
 * 
 * @version $Id$
 * =============================================================================
 */
include_once '../init.php';

if(!$userbank->HasAccess(ADMIN_OWNER|ADMIN_ADD_BAN))
{
	echo "No Access";
	die();
}
require_once(INCLUDES_PATH . 'xajax.inc.php');
$xajax = new xajax();
//$xajax->debugOn();
$xajax->setRequestURI("./admin.kickit.php");
$xajax->registerFunction("KickPlayer");
$xajax->registerFunction("LoadServers");
$xajax->processRequests();
$username = $userbank->GetProperty("user");

function LoadServers($check, $type) {
	$objResponse = new xajaxResponse();
	global $userbank, $username;
	if(!$userbank->HasAccess(ADMIN_OWNER|ADMIN_ADD_BAN))
	{
		$objResponse->redirect("index.php?p=login&n=no_access", 0);
        new CSystemLog(
            "w",
            t('log', "Hacking Attempt"),
            t('log', '[[user]] tried to use kickit, but doesnt have access.', array(
                '[[user]]' => $username,
            ))
        );
		return $objResponse;
	}
	$id = 0;
	$servers = $GLOBALS['db']->Execute("SELECT sid, rcon FROM ".DB_PREFIX."_servers WHERE enabled = 1 ORDER BY modid, sid;");
	while(!$servers->EOF) {
		//search for player
		if(!empty($servers->fields["rcon"])) {
			$text = '<font size="1">'.t('admin/bans', 'Searching...').'</font>';
			$objResponse->addScript("xajax_KickPlayer('".$check."', '".$servers->fields["sid"]."', '".$id."', '".$type."');");
		}
		else { //no rcon = servercount + 1 ;)
			$text = '<font size="1">'.t('admin/bans', 'No rcon password.').'</font>';
			$objResponse->addScript('set_counter(1);');
		}		
		$objResponse->addAssign("srv_".$id, "innerHTML", $text);
		$id++;
		$servers->MoveNext();
	}
	return $objResponse;
}

function KickPlayer($check, $sid, $num, $type) {
	$objResponse = new xajaxResponse();
	global $userbank, $username;
	$sid = (int)$sid;

	if(!$userbank->HasAccess(ADMIN_OWNER|ADMIN_ADD_BAN)) {
		$objResponse->redirect("index.php?p=login&n=no_access", 0);
        new CSystemLog(
            "w",
            t('log', "Hacking Attempt"),
            t('log', '[[user]] tried to process a playerkick, but doesnt have access.', array(
                '[[user]]' => $username,
            ))
        );
		return $objResponse;
	}
	

    // show hostname instead of the ip, but leave the ip in the title
    require_once("../includes/system-functions.php");
    
    $players = checkMultiplePlayers($sid);
    
    $gothim = false;
    
    foreach($players as $player) {
        if($type == 0) {
            if(substr($player['steamid'], 8) == substr($check, 8)) {
                // gotcha!!! kick him!
                $gothim = true;
                $GLOBALS['db']->Execute(
                    "UPDATE `".DB_PREFIX."_bans` SET sid = '".$sid."' WHERE authid = '".$check."' AND RemovedBy IS NULL;"
                );
                $requri = substr(
                    $_SERVER['REQUEST_URI'],
                    0,
                    strrpos($_SERVER['REQUEST_URI'], "pages/admin.kickit.php")
                );
                $kickmsg = t(
                    'admin/bans',
                    'You have been banned by this server, check http://[[url]] for more info.',
                    array(
                        '[[url]]' => $_SERVER['HTTP_HOST'].$requri
                    )
                );
                if(steamidVersion($player['steamid']) == 3) {
                    $player['steamid'] = '"'.$player['steamid'].'"';
                }
                SendRconSilent('kickid '.$player['steamid'].' "'.$kickmsg.'"', $sid);
                $objResponse->addAssign(
                    "srv_{$num}",
                    "innerHTML",
                    '<font color="green" size="1"><b><u>'.t('admin/bans', 'Player Found & Kicked!!!').'</u></b></font>'
                );
                $objResponse->addScript("set_counter('-1');");
                return $objResponse;
            }
        } elseif($type == 1) {
            if($player['ip'] == $check) {
                
                // gotcha!!! kick him!
                
                $gothim = true;
                
                $GLOBALS['db']->Execute(
                    "UPDATE `".DB_PREFIX."_bans` SET sid = '".$sid."' WHERE ip = '".$check."' AND RemovedBy IS NULL;"
                );
                
                $requri = substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], "pages/admin.kickit.php"));
                
                $kickmsg = t(
                    'admin/bans',
                    'You have been banned by this server, check http://[[url]] for more info.',
                    array(
                        '[[url]]' => $_SERVER['HTTP_HOST'].$requri
                    )
                );
                if(steamidVersion($player['steamid']) == 3) {
                    $player['steamid'] = '"'.$player['steamid'].'"';
                }
                SendRconSilent('kickid '.$player['steamid'].' "'.$kickmsg.'"', $sid);
                
                $objResponse->addAssign(
                    "srv_{$num}",
                    "innerHTML",
                    '<font color="green" size="1"><b><u>'.t('admin/bans', 'Player Found & Kicked!!!').'</u></b></font>'
                );
                $objResponse->addScript("set_counter('-1');");
                return $objResponse;
            }
        }
    }

    if(!$gothim) {
        $objResponse->addAssign(
            "srv_{$num}",
            "innerHTML",
            '<font size="1">'.t('admin/bans', 'Player not found.').'</font>'
        );
        $objResponse->addScript('set_counter(1);');
    }
    return $objResponse;
}
$servers = $GLOBALS['db']->Execute("SELECT ip, port, rcon FROM ".DB_PREFIX."_servers WHERE enabled = 1 ORDER BY modid, sid;");
$theme->assign('total', $servers->RecordCount());
$serverlinks = array();
$num = 0;
while(!$servers->EOF) {
	$info = array();
	$info['num'] = $num;
	$info['ip'] = $servers->fields["ip"];
	$info['port'] = $servers->fields["port"];
	array_push($serverlinks, $info);
	$num++;
	$servers->MoveNext();
}
$theme->assign('servers', $serverlinks);
$theme->assign('xajax_functions',  $xajax->printJavascript("../scripts", "xajax.js"));
$theme->assign('check', $_GET["check"]);// steamid or ip address
$theme->assign('type', $_GET['type']);

$theme->left_delimiter = "-{";
$theme->right_delimiter = "}-";
$theme->display('page_kickit.tpl');
$theme->left_delimiter = "{";
$theme->right_delimiter = "}";
