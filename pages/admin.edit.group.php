<?php  
/**
 * =============================================================================
 * Edit a group
 * 
 * @author SteamFriends Development Team
 * @version 1.0.0
 * @copyright SourceBans (C)2007 SteamFriends.com.  All rights reserved.
 * @package SourceBans
 * @link http://www.sourcebans.net
 * 
 * @version $Id: admin.edit.group.php 195 2008-12-30 17:26:40Z peace-maker $
 * =============================================================================
 */

if(!defined("IN_SB")){echo "You should not be here. Only follow links!";die();} 


if(!isset($_GET['id'])) {
	echo '<div id="msg-red" >
	<i><img src="./images/warning.png" alt="Warning" /></i>
	<b>'.t('admin/bans', 'Error').'</b>
	<br />
	'.t('admin/bans', 'No group id specified. Please only follow links').'
</div>';
	die();
}

if(!isset($_GET['type']) || ($_GET['type'] != 'web' && $_GET['type'] != 'srv' && $_GET['type'] != 'server')) {
	echo '<div id="msg-red" >
	<i><img src="./images/warning.png" alt="Warning" /></i>
	<b>'.t('admin/bans', 'Error').'</b>
	<br />
	'.t('admin/bans', 'No valid group type specified. Please only follow links').'
</div>';
	die();
}

$_GET['id'] = (int)$_GET['id'];

$overrides_list = $GLOBALS['db']->GetAll("SELECT * FROM `" . DB_PREFIX . "_srvgroups_overrides` WHERE group_id = ?", array($_GET['id']));

$web_group = $GLOBALS['db']->GetRow("SELECT flags, name FROM ".DB_PREFIX."_groups WHERE gid = {$_GET['id']}");
$srv_group = $GLOBALS['db']->GetRow("SELECT flags, name, immunity FROM ".DB_PREFIX."_srvgroups WHERE id = {$_GET['id']}");


$web_flags = intval($web_group[0]);
$srv_flags = isset($srv_group[0]) ? $srv_group[0] : '';

global $userbank, $theme;

if($_GET['type'] == "web") {
    $name = RemoveCode($web_group['name']);
    $permissions = '<h3>'.t('admin/groups', 'Web Admin Permissions').'</h3>';
    $permFile = 'groups.web.perm.php';
} elseif($_GET['type'] == "srv") {
    $name = RemoveCode($srv_group['name']);
    $permissions = '<h3>'.t('admin/groups', 'Server Admin Permissions').'</h3>';
    $permFile = 'groups.web.perm.php';
} else {
    $permissions = '';
    $permFile = false;
}
if($permFile) {
    $title = t('admin/groups', 'Permissions of group [[groupname]]', array('[[groupname]]' => $name));
    ob_start();
    include TEMPLATES_PATH . "{$permFile}";
    $permissions .= ob_get_clean();
}
$theme->assign('gid' ,$_GET['id']);
$theme->assign('overrides_list' ,$overrides_list);
$theme->assign('group_type' ,$_GET['type']);
$theme->left_delimiter = "-{";
$theme->right_delimiter = "}-";
$theme->display('page_admin_edit_groups.tpl');
$theme->left_delimiter = "{";
$theme->right_delimiter = "}";
?>
<script>
<?php if($_GET['type'] == "web" || $_GET['type'] == "server"):?>
	$('groupname').value = "<?php echo $web_group['name']?>";
<?php endif?>
<?php if(!$userbank->HasAccess(ADMIN_OWNER)): ?>
	if($("wrootcheckbox")) { 
		$("wrootcheckbox").setStyle('display', 'none');
	}
	if($("srootcheckbox")) { 
		$("srootcheckbox").setStyle('display', 'none');
	}
<?php endif?>
<?php if($_GET['type'] == "web"):?>	
$('p2').checked = <?php echo check_flag($web_flags, ADMIN_OWNER) ? "true" : "false"?>;

$('p4').checked = <?php echo check_flag($web_flags, ADMIN_LIST_ADMINS) ? "true" : "false"?>;
$('p5').checked = <?php echo check_flag($web_flags, ADMIN_ADD_ADMINS) ? "true" : "false"?>;
$('p6').checked = <?php echo check_flag($web_flags, ADMIN_EDIT_ADMINS) ? "true" : "false"?>;
$('p7').checked = <?php echo check_flag($web_flags, ADMIN_DELETE_ADMINS) ? "true" : "false"?>;

$('p9').checked = <?php echo check_flag($web_flags, ADMIN_LIST_SERVERS) ? "true" : "false"?>;
$('p10').checked = <?php echo check_flag($web_flags, ADMIN_ADD_SERVER) ? "true" : "false"?>;
$('p11').checked = <?php echo check_flag($web_flags, ADMIN_EDIT_SERVERS) ? "true" : "false"?>;
$('p12').checked = <?php echo check_flag($web_flags, ADMIN_DELETE_SERVERS) ? "true" : "false"?>;

$('p14').checked = <?php echo check_flag($web_flags, ADMIN_ADD_BAN) ? "true" : "false"?>;
$('p16').checked = <?php echo check_flag($web_flags, ADMIN_EDIT_OWN_BANS) ? "true" : "false"?>;
$('p17').checked = <?php echo check_flag($web_flags, ADMIN_EDIT_GROUP_BANS) ? "true" : "false"?>;
$('p18').checked = <?php echo check_flag($web_flags, ADMIN_EDIT_ALL_BANS) ? "true" : "false"?>;
$('p19').checked = <?php echo check_flag($web_flags, ADMIN_BAN_PROTESTS) ? "true" : "false"?>;
$('p20').checked = <?php echo check_flag($web_flags, ADMIN_BAN_SUBMISSIONS) ? "true" : "false"?>;
$('p33').checked = <?php echo check_flag($web_flags, ADMIN_DELETE_BAN) ? "true" : "false"?>;
$('p32').checked = <?php echo check_flag($web_flags, ADMIN_UNBAN) ? "true" : "false"?>;
$('p34').checked = <?php echo check_flag($web_flags, ADMIN_BAN_IMPORT) ? "true" : "false"?>;
$('p38').checked = <?php echo check_flag($web_flags, ADMIN_UNBAN_OWN_BANS) ? "true" : "false"?>;
$('p39').checked = <?php echo check_flag($web_flags, ADMIN_UNBAN_GROUP_BANS) ? "true" : "false"?>;

$('p36').checked = <?php echo check_flag($web_flags, ADMIN_NOTIFY_SUB) ? "true" : "false"?>;
$('p37').checked = <?php echo check_flag($web_flags, ADMIN_NOTIFY_PROTEST) ? "true" : "false"?>;

$('p22').checked = <?php echo check_flag($web_flags, ADMIN_LIST_GROUPS) ? "true" : "false"?>;
$('p23').checked = <?php echo check_flag($web_flags, ADMIN_ADD_GROUP) ? "true" : "false"?>;
$('p24').checked = <?php echo check_flag($web_flags, ADMIN_EDIT_GROUPS) ? "true" : "false"?>;
$('p25').checked = <?php echo check_flag($web_flags, ADMIN_DELETE_GROUPS) ? "true" : "false"?>;

$('p26').checked = <?php echo check_flag($web_flags, ADMIN_WEB_SETTINGS) ? "true" : "false"?>;

$('p28').checked = <?php echo check_flag($web_flags, ADMIN_LIST_MODS) ? "true" : "false"?>;
$('p29').checked = <?php echo check_flag($web_flags, ADMIN_ADD_MODS) ? "true" : "false"?>;
$('p30').checked = <?php echo check_flag($web_flags, ADMIN_EDIT_MODS) ? "true" : "false"?>;
$('p31').checked = <?php echo check_flag($web_flags, ADMIN_DELETE_MODS) ? "true" : "false"?>;

<?php elseif($_GET['type'] == "srv"):?>
$('groupname').value = "<?php echo $srv_group['name']?>";
$('s14').checked = <?php echo strstr($srv_flags, SM_ROOT) ? "true" : "false"?>;
$('s1').checked = <?php echo strstr($srv_flags, SM_RESERVED_SLOT) ? "true" : "false"?>;
$('s23').checked = <?php echo strstr($srv_flags, SM_GENERIC) ? "true" : "false"?>;
$('s2').checked = <?php echo strstr($srv_flags, SM_KICK) ? "true" : "false"?>;
$('s3').checked = <?php echo strstr($srv_flags, SM_BAN) ? "true" : "false"?>;
$('s4').checked = <?php echo strstr($srv_flags, SM_UNBAN) ? "true" : "false"?>;
$('s5').checked = <?php echo strstr($srv_flags, SM_SLAY) ? "true" : "false"?>;
$('s6').checked = <?php echo strstr($srv_flags, SM_MAP) ? "true" : "false"?>;
$('s7').checked = <?php echo strstr($srv_flags, SM_CVAR) ? "true" : "false"?>;
$('s8').checked = <?php echo strstr($srv_flags, SM_CONFIG) ? "true" : "false"?>;
$('s9').checked = <?php echo strstr($srv_flags, SM_CHAT) ? "true" : "false"?>;
$('s10').checked = <?php echo strstr($srv_flags, SM_VOTE) ? "true" : "false"?>;
$('s11').checked = <?php echo strstr($srv_flags, SM_PASSWORD) ? "true" : "false"?>;
$('s12').checked = <?php echo strstr($srv_flags, SM_RCON) ? "true" : "false"?>;
$('s13').checked = <?php echo strstr($srv_flags, SM_CHEATS) ? "true" : "false"?>;

$('s17').checked = <?php echo strstr($srv_flags, SM_CUSTOM1) ? "true" : "false"?>;
$('s18').checked = <?php echo strstr($srv_flags, SM_CUSTOM2) ? "true" : "false"?>;
$('s19').checked = <?php echo strstr($srv_flags, SM_CUSTOM3) ? "true" : "false"?>;
$('s20').checked = <?php echo strstr($srv_flags, SM_CUSTOM4) ? "true" : "false"?>;
$('s21').checked = <?php echo strstr($srv_flags, SM_CUSTOM5) ? "true" : "false"?>;
$('s22').checked = <?php echo strstr($srv_flags, SM_CUSTOM6) ? "true" : "false"?>;

$('immunity').value = <?php echo $srv_group['immunity'] ? (int)$srv_group['immunity'] : "0"?>;
<?php endif?>
</script>