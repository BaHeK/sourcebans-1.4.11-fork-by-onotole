<?php 
/**
 * =============================================================================
 * Lost password page
 * 
 * @author SteamFriends Development Team
 * @version 1.0.0
 * @copyright SourceBans (C)2007 SteamFriends.com.  All rights reserved.
 * @package SourceBans
 * @link http://www.sourcebans.net
 * 
 * @version $Id: page.lostpassword.php 24 2007-11-06 18:17:05Z olly $
 * =============================================================================
 */

global $theme, $userbank;

if(isset($_GET['validation'],$_GET['email']) && !empty($_GET['email']) && !empty($_GET['validation'])) {
	$email = $_GET['email'];
	$validation = $_GET['validation'];
	
	if(strlen($validation) < 60) {
		echo '<div id="msg-red" style="">
			<i><img src="./images/warning.png" alt="Warning" /></i>
			<b>'.t('lostpassword', 'Error').'</b>
			<br />
			'.t('lostpassword', 'The validation string is too short.').'
			</div>';
		exit();
	}
	
	$q = $GLOBALS['db']->GetRow(
        "SELECT
            aid,
            user
        FROM
            `" . DB_PREFIX . "_admins`
        WHERE
            `email` = ? && `validate` IS NOT NULL && `validate` = ?",
        array($email, $validation)
    );
	if($q) {
		$newpass = generate_salt(MIN_PASS_LENGTH+8);
		$query = $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_admins` SET `password` = '" . $userbank->encrypt_password($newpass) . "', validate = NULL WHERE `aid` = ?", array($q['aid']));
        
        $message = t('mail/lostpasswd', 'Hello [[user]]', array('[[user]]' => RemoveCode($q['user']))) . PHP_EOL;
        $message .= t('mail/lostpasswd', 'Your password reset was successful.') . PHP_EOL;
        $message .= t('mail/lostpasswd', 'Your password was changed to: [[newpassword]]', array('[[newpassword]]' => $newpass)) . PHP_EOL;
        $message .= t('mail/lostpasswd', 'Login to your SourceBans account and change your password in Your Account.') . PHP_EOL;

        $subject = t('mail/lostpasswd', 'SourceBans Password Reset');
        
		$userbank->sendMail($email, $subject, $message);
		
		echo '<div id="msg-blue" style="">
			<i><img src="./images/info.png" alt="Info" /></i>
			<b>'.t('lostpassword', 'Password Reset').'</b>
			<br />
			'.t('lostpassword', 'Your password has been reset and sent to your email.<br />Please check your spam folder too.<br />Please login using this password, <br />then use the change password link in Your Account.').'
			</div>';
	}
	else 
	{
		echo '<div id="msg-red" style="">
			<i><img src="./images/warning.png" alt="Warning" /></i>
			<b>'.t('lostpassword', 'Error').'</b>
			<br />
			'.t('lostpassword', 'The validation string does not match the email for this reset request.').'
			</div>';
	}
}else {
    $sbViewParams['pageTitle'] = t('lostpassword', 'Restore password');
    $sbViewParams['title'] =  t('lostpassword', 'Lost Password Form');
    $sbViewParams['breadCrumbs'][] = t('breadcrumbs', 'Restore password');
	$theme->display('page_lostpassword.tpl');
}
