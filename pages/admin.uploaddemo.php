<?php
/**
 * =============================================================================
 * Upload a demo
 * 
 * @author SteamFriends Development Team
 * @version 1.0.0
 * @copyright SourceBans (C)2007 SteamFriends.com.  All rights reserved.
 * @package SourceBans
 * @link http://www.sourcebans.net
 * 
 * @version $Id: admin.uploaddemo.php 179 2008-12-11 20:37:10Z peace-maker $
 * =============================================================================
 */


include_once("../init.php");
include_once("../includes/system-functions.php");
global $theme, $userbank;

if (!$userbank->HasAccess(ADMIN_OWNER|ADMIN_ADD_BAN|ADMIN_EDIT_OWN_BANS|ADMIN_EDIT_GROUP_BANS|ADMIN_EDIT_ALL_BANS)) {
    new CSystemLog(
        "w",
        t('log', "Hacking Attempt"),
        t('log', '[[user]] tried to upload a demo, but doesn\'t have access.', array(
            '[[user]]' => $userbank->GetProperty("user"),
        ))
    );
	echo t('admin', 'You don\'t have access to this!');
	die();
}

$message = "";

$approwedExts = array(
    "zip",
    "rar",
    "dem",
    "7z",
    "bz2",
    "gz"
);
$approwedExtsStr = implode(', ', $approwedExts);

if(isset($_POST['upload'])) {
    $checkExt = CheckExt($_FILES['demo_file']['name'], $approwedExts);
	if($checkExt) {
		$filename = md5(time().rand(0, 1000));
		move_uploaded_file($_FILES['demo_file']['tmp_name'],SB_DEMOS.$filename);
		$message =  "<script>window.opener.demo('" . $filename . "','" . $_FILES['demo_file']['name'] . "');self.close()</script>";
        new CSystemLog(
            "m",
            t('log', "Demo Uploaded"),
            t('log', 'A new demo has been uploaded: [[demofile]]', array(
                '[[demofile]]' => htmlspecialchars($_FILES['demo_file']['name']),
            ))
        );
	} else {
        $message = t('admin', 'The file must have the following extension: [[extensions]]', array(
            '[[extensions]]' => $approwedExtsStr
        ));
	}
}

$theme->assign('title', t('admin', 'Upload Demo'));
$theme->assign('message', $message);
$theme->assign('input_name', 'demo_file');
$theme->assign('form_name', 'demup');
$theme->assign("formats", $approwedExtsStr);

$theme->display('page_uploadfile.tpl');
