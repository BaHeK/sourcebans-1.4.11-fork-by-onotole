<?php
return array(
    
    /**
     * Links labels
     */
    'Dashboard' => 'Главная',
    'Ban List' => 'Банлист',
    'Servers' => 'Серверы',
    'Submit a ban' => 'Предложить',
    'Protest a ban' => 'Протест',
    'Admin Panel' => 'Админцентр',
    
    /**
     * Links titles
     */
    'This page shows an overview of your bans and servers.' => 'Главная страница',
    'All of the bans in the database can be viewed from here.' => 'Список всех банов',
    'All of your servers and their status can be viewed here' => 'Страница, которая отображает все серверы и информацию с них',
    'You can submit a demo or screenshot of a suspected cheater here. It will then be up for review by one of the admins' => 'На этой странице Вы можете предложить бан игрока',
    'Here you can protest your ban. And prove your case as to why you should be unbanned.' => 'Тут Вы можете опротестовать Ваш бан',
    'This is the control panel for SourceBans where you can setup new admins, add new server, etc.' => 'Администраторский раздел',
);