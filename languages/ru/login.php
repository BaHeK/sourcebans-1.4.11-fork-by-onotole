<?php

return array(
    'Login form' => 'Форма входа',
    
    'Error - No Access' => 'Ошибка доступа',
    'You dont have permission to access this page.<br />Please login with an account that has access.' => 'У Вас нет доступа к этой странице<br>Если у Вас есть другой аккаунт, попробуйте войти под ним, или обратитесь к администратору',
    
    'Admin Login' => 'Авторизация',
    'Guest' => 'гость',
    'Username:' => 'Логин:',
    'Password:' => 'Пароль:',
    'Remember me' => 'Запомнить',
    'Back to the Homepage' => 'На главную',
    'Lost Your Password?' => 'Забыли пароль?',
    'Login' => 'Войти',
    
    'Login Failed' => 'Ошибка входа',
    'The email address you supplied is not registered on the system' => 'Такой E-mail не найден в базе',
    'The username or password you supplied was incorrect.<br \> If you have forgotten your password, use the <a href=\"index.php?p=lostpassword\" title=\"Lost password\">Lost Password</a> link.' => 'Имя пользователя или пароль, введенные Вами, неверны.<br \>Если Вы забыли свой пароль, то перейдите по ссылке: <a href=\"index.php?p=lostpassword\" title=\"Забыл пароль\">Забыл пароль</a>',
    
    'You must enter your password!' => 'Введите пароль',
    'You must enter your loginname!' => 'Введите логин',
    
    // AJAX
    'Information' => 'Информация',
    'You can\'t login. No password set.' => 'Вы не можете войти: для Вашего аккаунта не задан пароль',
);
