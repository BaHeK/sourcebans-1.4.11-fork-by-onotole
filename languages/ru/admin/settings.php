<?php

return array(
    // admin.settings.php
    'prev' => 'пред.',
    'next' => 'след.',
    'Page [[page]] of [[pages]] - [[prevlink]] | [[nextlink]]' => 'Страница [[page]] из [[pages]] - [[prevlink]] | [[nextlink]]',
    'Page [[page]] of [[pages]]' => 'Страница [[page]] из [[pages]]',
    'Info' => 'Информация',
    'Warning' => 'Предупреждение',
    'Error' => 'Ошибка',
    'Wrong language code!' => 'Неверный код языка',
    'Access Denied!' => 'Доступ запрещен',
    'Min password length must be a number' => 'Минимальная длина пароля может быть только числом',
    'Bans per page must be a number' => 'Банов на страницу может быть только числом',
    'Settings updated' => 'Настройки сохранены',
    'The changes have been successfully updated' => 'Настройки успешно сохранены',
    'Clear Log' => 'Очистить лог',
    'You can\'t use these features. You need to set PHP safe mode off.' => 'Для того, чтобы использовать эти функции, необходимо установить "safe_mode off" в настройках PHP',
    
    // page_admin_settings_settings.tpl
    'For more information or help regarding a certain subject move your mouse over the question mark.' => 'Для получения более подробной информации или справки по определенной теме, наведите курсор мыши на знак вопроса.',
    'Main Settings' => 'Главные настройки',
    'Title' => 'Заголовок',
    'Define the title shown in the title of your browser.' => 'Заголовок страницы в браузере',
    'Path to logo' => 'Путь к логотипу',
    'Here you can define a new location for the logo, so you can use your own image.' => 'Тут вы можете указать путь к логотипу',
    'Min password length' => 'Минимальная длина паролей',
    'Define the shortest length a password can be.' => 'Задает минимально допустимую длину вводимых паролей',
    'Date format' => 'Формат даты',
    'Here you can change the date format, displayed in the banlist and other pages.' => 'Тут вы можете изменить формат даты, выводимой в банлисте и в других страницах',
    'See: PHP date()' => 'Подробнее: PHP date()',
    'Timezone' => 'Часовой пояс',
    'Here you can change the default timezone that SourceBans displays times in' => 'Установите часовой пояс вашего сайта',
    
    'Language' => 'Язык',
    'Select system language' => 'Выберите язык системы',
    
    'Enable Client Language' => 'Пользовательский язык',
    'Select the check box to allow users to change the site language' => 'Установите этот флажок, чтобы разрешить пользоватерям менять язык сайта для себя',
    
    'Summertime' => 'Летнее время',
    'Enable Summertime' => 'Включить летнее время',
    'Check this box to enable summertime.' => 'Установите галочку, если сейчас действует летнее время',
    'Debugmode' => 'Режим разработчика',
    'Enable Debugmode' => 'Включить режим разработчика',
    'Check this box to enable the debugmode permanently.' => 'Установите галочку, если хотите включить режим разработчика',
    'Dashboard Settings' => 'Настройки главной страницы',
    'Intro Title' => 'Заголовок панели',
    'Set the title for the dashboard introduction.' => 'Введите заголовок для панели главной страницы',
    'Intro Text' => 'Текст панели',
    'Set the text for the dashboard introduction.' => 'Введите текст, который будет отображаться в панели главной страницы',
    'Disable Log Popup' => 'Отключить информационные окна',
    'Check this box to disable the log info popup and use direct link.' => 'Установите этот флажок, если хотите отключить показ всплывающих окон с информацией',
    'Page Settings' => 'Настройки страниц',
    'Enable Protest Ban' => 'Протесты банов',
    'Check this box to enable the protest ban page.' => 'Установите флажок, чтобы включить систему протестов банов',
    'Only Send One Email' => 'Email админу',
    'Check this box to only send the protest notification email to the admin who banned the protesting player.' => 'Установите этоу галочку, чтобы сообщение о новом протесте бана отправлялось только тому админу, который забанил протестующего',
    'Enable Submit Ban' => 'Предложения банов',
    'Check this box to enable the submit ban page.' => 'Установите галочку, чтобы включить систему предложения банов',
    'Default Page' => 'Главная страница',
    'Choose the page that will be the first page people will see.' => 'Выберите страницу, на которую будет переведен пользователь, впервые открывший сайт',
    'Clear Cache' => 'Очистить кэш',
    'Click this button, to clean the themes_c folder.' => 'Нажмите на кнопку, чтобы очистить системный кэш (папка themes_c)',
    'Banlist Settings' => 'Настройки банлиста',
    'Items Per Page' => 'Банов на страницу',
    'Choose how many items to show on each page.' => 'Укажите количество выводимых на странице банов',
    'Hide Admin Name' => 'Скрывать ник админа',
    'Check this box, if you want to hide the name of the admin in the baninfo.' => 'Установите этот флажок, если вы не хотите показывать ник админа в деталях бана',
    'No Country Research' => 'Отключить флаги стран',
    'Check this box, if you don\'t want to display the country out of an IP in the banlist. Use if you encounter display problems.' => 'Установите этот флажок, если не хотите показывать флаги стран забаненных, или имеются проблемы с отображением банлиста',
    'Hide Player IP' => 'Скрывать IP забаненного',
    
    'Hide Player SteamID' => 'Не показывать SteamID',
    'Check this box, if you want to hide the player SteamID and CommunityId from the public.' => 'Установите галочку, чтобы не показывать SteamID и CommunityId забаненных игроков',
    
    'Check this box, if you want to hide the player IP from the public.' => 'Установите флажок, чтобы скрыть IP забаненного игрока для пользователей',
    'Custom Banreasons' => 'Свои причины бана',
    'Type the custom banreasons you want to appear in the dropdown menu.' => 'Введите собственные причины бана',
    'Save Changes' => 'Сохранить',
    'Back' => 'Назад',
    
    // page_admin_settings_email.tpl
    'Select the method of sending email messages' => 'Выберите метод отправки сообщений',
    'Email send transport' => 'Метод отправки сообщений',
    'Email transport' => 'Метод отправки сообщений',
    'Enter the address of the SMTP server' => 'Введите адрес сервера SMTP',
    'SMTP host' => 'Адрес SMTP',
    'Enter the port of the SMTP server' => 'Введите порт SMTP',
    'SMTP port' => 'Порт SMTP',
    'Enter username' => 'Введите логин',
    'Username' => 'Логин',
    'SMTP password' => 'Пароль',
    'Enter password' => 'Введите пароль',
    'Password' => 'Пароль',
    'Email Settings' => 'Настройки отправки E-mail',
    'SMTP secure' => 'Шифрование',
    'Select secure method' => 'Выберите тип шифрования.',
    'No secure' => 'Без шифрования',
    '' => '',
    
    // page_admin_settings_features.tpl
    'Make Export Bans Public' => 'Экспорт банов',
    'Enable Public Bans' => 'Разрешить экспорт банов',
    'Check this box to enable the entire ban list to be publically downloaded and shared.' => 'Установите флажок, чтобы включить возможность всем пользователям экспортировать списмок банов в файл и скачивать его себе',
    'Enable KickIt' => 'Включить кик',
    'Check this box to kick a player when a ban is posted.' => 'Установите галочку, чтобы включить кик игрока при бане',
    'Enable Group Banning' => 'Включить бан групп',
    'Check this box, if you want to enable banning of whole steam community groups.' => 'Установите галочку, чтобы включить возможность бана групп',
    'Enable Friends Banning' => 'Бан друзей',
    'Check this box, if you want to enable banning all steam community friends of a player.' => 'Установите галочку, чтобы включить возможность бана друзей',
    'Enable Admin Rehashing' => 'Обновление админов',
    'Check this box, if you want to enable the admin rehashing everytime an admin/group has been changed.' => 'Установите флажок, чтобы включить автоматическое обновление админов на серверах при добавлении или изменении админа',
    
    // page_admin_settings_themes.tpl
    'Themes' => 'Темы оформления',
    'Selected Theme' => 'Текущая тема',
    'Theme Author' => 'Автор темы',
    'Theme Version' => 'Версия темы',
    'Theme Link' => 'Ссылка',
    'Available Themes' => 'Доступные темы',
    'Click a theme below to see details about it.' => 'Кликните по теме для просмотра деталей',
    
    // page_admin_settings_log.tpl
    'System Log' => 'Системный лог',
    'Click on a row to see more details about the event.' => 'Кликните на строке, чтобы увидеть более подробную информацию о событии.',
    'Type' => 'Тип',
    'Event' => 'Событие',
    'User' => 'Пользователь',
    'Date/Time' => 'Дата/Время',
    'Event Details' => 'Подробности события',
    'Details' => 'Описание',
    'Parent Function' => 'Родительская функция',
    'Query String' => 'Строка запроса',
    'IP Address' => 'IP адрес',
    'Are you sure you want to delete all of the log entries?' => 'Удалить все записи системного лога?',
    
    // page_admin_settings_mail.tpl
    'This name will be indicated in the "From" field in the sent letters' => 'Это имя будет указано в поле "От" в отправляемых с сайта письмах',
    'From name' => 'Имя для ответа',
    'From Email' => 'Адрес для ответа',
    'This email will be indicated in the "From" field in the sent letters' => 'Этот адрес будет указан в поле "От" в отправляемых с сайта письмах',
);
