<?php

return array(
    // Details
    // .php
    'No admin id specified. Please only follow links' => 'Не передан ID админа!',
    'You are not allowed to edit other profiles.' => 'У Вас недостаточно прав',
    'You must type a name for the admin.' => 'Введите ник админа',
    "An admin name can not contain a \" \' \"." => 'В нике админа не должно быть символа \" \' \".',
    'An admin with this name already exists.' => 'Админ с таким ником уже существует',
    'You must type a Steam ID or Community ID for the admin.' => 'Введите SteamID или CommunityID админа',
    'Please enter a valid Steam ID or Community ID.' => 'Steam ID или Community ID введен неверно',
    'Admin [[adminname]] already uses this Steam ID.' => 'Такой SteamID уже используется админом [[adminname]]',
    'You must type an e-mail address.' => 'Введите E-mail адрес',
    'This email address is already being used by [[adminname]]' => 'Этот E-mail уже используется админом [[adminname]]',
    'Your password must be at-least [[minpasslength]] characters long.' => 'Пароль не может быть короче [[minpasslength]] символов',
    'You must confirm the password.' => 'Введите подтверждение пароля',
    'Your passwords don\'t match.' => 'Пароли не совпадают',
    'You must type a server password or uncheck the box.' => 'Введите пароль сервера, или снимите галочку',
    'Admin details updated' => 'Админ обновлен',
    'The admin details has been updated successfully' => 'Детали админа были успешно обновлены',
    // .tpl
    'Admin Details' => 'Детали админа',
    'This is the username the admin will use to login-to their admin panel. Also this will identify the admin on any bans they make.' => 'Логин администратора для доступа к админцентру SourceBans',
    'Admin Login' => 'Логин',
    'Steam ID' => 'SteamID',
    'This is the admins \'STEAM\' id. This must be set so that admins can use their admin rights ingame.' => 'SteamID админа. Необходим для привязки серверных прав к админу',
    'Admin STEAM ID' => 'SteamID',
    'Admin Email' => 'E-mail',
    'Set the admins e-mail address. This will be used for sending out any automated messages from the system, and for use when you forget your password.' => 'E-mail админа. Необходим для отправки сообщений SourceBans. Например для смены пароля',
    'Password' => 'Пароль',
    'The password the admin will need to access the admin panel.' => 'Пароль доступа к админцентру',
    'Admin Password' => 'Пароль',
    'Type your password again to confirm.' => 'Введите пароль снова для подтверждения',
    'Admin Password (confirm)' => 'Подтверждение',
    'Server Admin Password' => 'Пароль сервера',
    'If this box is checked, you will need to specify this password in the game server before you can use your admin rights.' => 'Если галочка установлена, то необходимо задать пароль сервера, для валидации SteamID админа на сервере',
    'Server Password' => 'Пароль сервера',
    'SourceMod Password Info' => 'Информация о пароле SourceMod',
    'More' => 'Подробнее',
    'Save Changes' => 'Сохранить',
    'Back' => 'Назад',
    'Immunity must be a numerical value (0-9)' => 'Иммунитет может быть только числом от 0 до 99',
    
    // Groups
    // .php
        // Экраны двойных кавычек обязательны
    'You are not allowed to edit other admin\'s groups.' => 'У Вас недостаточно прав для изменения групп админа',
    'Admins have to have a password and email set in order to get web permissions.<br /><a href=\"index.php?p=admin&c=admins&o=editdetails&id=[[adminid]]\" title=\"Edit Admin Details\">Set the details</a> first and try again.' => 'Для выдачи админу веб разрешений, необходимо указать для него пароль и E-mail.<br /><a href=\"index.php?p=admin&c=admins&o=editdetails&id=[[adminid]]\" title=\"Редактировать детали админа\">Отредактируйте детали админа</a>',
    'Error' => 'Ошибка',
    'Admin updated' => 'Админ обновлен',
    'The admin has been updated successfully' => 'Админ был успешно обновлен',
    // .tpl
    'Admin Groups' => 'Группы админа',
    'For more information or help regarding a certain subject move your mouse over the question mark.' => 'Для получения более подробной информации или справки по определенной теме, наведите курсор мыши на знак вопроса.',
    'Choose the new groups that you want <b>[[group_admin_name]]</b> to appear in.' => 'Выберите группы, которые Вы хотите назначить админу <b>[[group_admin_name]]</b>',
    'Web Group' => 'WEB группа',
    'Web Admin Group' => 'Группа WEB разрешений',
    'Choose the group you want this admin to appear in for web permissions' => 'Выберите группу WEB разрешений для админа',
    'No Group' => 'Без группы',
    'Groups' => 'Группы',
    'Server Group' => 'Серверные разрешения',
    'Choose the group you want this admin to appear in for server admin permissions' => 'Выберите группу серверных разрешений',
    'Server Admin Group' => 'Серверные разрешения',
    
    // Perms
    // .php
    'You are not allowed to edit other admin\'s permissions.' => 'У Вас недостаточно прав для редактирования разрешений админа',
    'Select WEB permissions for admin "[[admin]]"' => 'Выберите WEB разрешения для админа [[admin]]',
    'Select server permissions for admin "[[admin]]"' => 'Выберите серверные разрешения для админа [[admin]]',
    // .tpl
    'Web Admin Permissions' => 'WEB разрешения',
    'Server Admin Permissions' => 'Серверные разрешения',
    
    // Servers
    // .php
    'You are not allowed to edit admins server access.' => 'У Вас недостаточно прав для редактирования серверов админа',
    'Admin server access updated' => 'Доступ к серверам изменен',
    'The admin server access has been updated successfully' => 'Доступ админа к серверам был успешно изменен.',
    // .tpl
    'Admin Server Access' => 'Доступ админа к серверам',
    'Please select the servers and/or groups of servers you want this admin to have access to.' => 'Выберите серверы и/или группы серверов, к которым Вы хотите дать доступ админу',
    'You need to add a server or a server group, before you can setup admin server permissions' => 'Вы должны добавить сервер или группу серверов, прежде чем вы сможете настроить доступ админа к серверам',
    'Server Groups' => 'Группы серверов',
    'Servers' => 'Серверы',
    'Please Wait...' => 'Ждите...',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
);
