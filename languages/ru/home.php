<?php

return array(
    // header.php
    'Rehashing Admin and Group data on all related servers...' => 'Обновление админов на серверах',
    'Are you sure you want to delete the comment?' => 'Уверены, что хотите удалить комментарий?',
    
    // page.home.php
    'Home' => 'Главная',
    '[[playername]] tried to enter' => 'Игрок [[playername]] пытался зайти на сервер:',  //
    'at [[date]]' => '[[date]]',                                                         // Pop-up window with the details of the lock
    'Click here for ban details.' => 'Подробности бана',                                 //
    'Blocked player: [[playername]]' => 'Блокированный игрок: [[playername]]',                                 //
    
    'Permanent' => 'Навсегда',
    'D' => 'У',
    'U' => 'Р',
    'E' => 'И',
    
    // page_dashboard.tpl
    'Latest Players Blocked' => 'Последние блокировки',
    'Total Stopped: [[total_blocked]]' => 'Всего блокировок: [[total_blocked]]',
    'Blocked Player' => 'Блокированный игрок',
    'Latest Added Bans' => 'Последние баны',
    'Total bans: [[total_bans]]' => 'Всего банов: [[total_bans]]',
    'MOD' => 'Мод',
    'Date/Time' => 'Дата/Время',
    'Name' => 'Ник',
    'Length' => 'Срок',
    'no nickname present' => 'Нет ника',
);
