<?php
return array(
    'Hacking Attempt' => 'Ошибка доступа',
    '[[username]] tried to Add a new group, but doesnt have access.' => '[[username]] пытался добавить новую группу, не имея на это прав',
    '[[username]] tried to change [[admin]]\'s server password, but doesn\'t have access.' => '[[username]] пытался сменить серверный пароль админа "[[admin]]", не имея на это прав',
    '[[username]] tried to check [[admin]]\'s server password, but doesn\'t have access.' => '[[username]] пытался проверить серверный пароль админа "[[admin]]", не имея на это прав',
    'Srv Password Changed' => 'Серверный пароль сменен',
    'Password changed for admin ([[adminid]])' => 'Пароль сменен для админа [[adminid]]',
    '[[username]] tried to change [[admin]]\'s email, but doesn\'t have access.' => '[[username]] пытался сменить Email админа "[[admin]]", не имея на это прав',
    'Group Created' => 'Группа создана',
    'A new group was created ([[name]])' => 'Добавлена новая группа: [[name]]',
    'Group Deleted' => 'Группа удалена',
    'Group ([[group]]) has been deleted' => 'Группа "[[group]]" была удалена',
    'There was a problem deleting the group from the database. Check the logs for more info' => 'Произошла ошибка при удалении группы. Для подробностей смотрите логи',
    '[[username]] tried to remove a submission, but doesnt have access.' => '[[username]] пытался удалить предложение бана, не имея на это прав',
    'Submission Archived' => 'Предложение бана отправлено в архив',
    'Submission ([[submission]]) has been moved to the archive' => 'Предложение бана "[[submission]]" было успешно отправлено в архив',
    'Submission Deleted' => 'Предложение бана удалено',
    'Submission ([[submission]]) has been deleted' => 'Предложение бана "[[submission]]" было удалено',
    'Submission Restored' => 'Предложение бана восстановлено',
    'Submission ([[submission]]) has been restored from the archive' => 'Предложение бана "[[submission]]" было восстановлено из архива',
    'Protest Deleted' => 'Протест удален',
    'Protest ([[protest]]) has been deleted' => 'Протест "[[protest]]" был удален',
    'Protest Archived' => 'Протест отправлен в архив',
    'Protest ([[protest]]) has been moved to the archive' => 'Протест бана "[[protest]]" был перемещен в архив',
    'Protest Restored' => 'Протест восстановлен',
    'Protest ([[protest]]) has been restored from the archive' => 'Протест "[[protest]]" восстановлен из архива',
    'Server Deleted' => 'Сервер удален',
    'Server ([[server]]) has been deleted' => 'Сервер "[[server]]" был удален из базы',
    'Admin Deleted' => 'Админ удален',
    'Admin ([[admin]]) has been deleted' => 'Админ "[[admin]]" был удален',
    'Server Added' => 'Сервер добавлен',
    'Server ([[server]]) has been added' => 'Добавлен новый сервер "[[server]]"',
    '[[username]] tried to add an admin, but doesnt have access.' => '[[username]] пытался добавить админа, не имея на это прав',
    '[[username]] tried to edit group name, but doesnt have access.' => '[[username]] пытался изменить группу, не имея на это прав',
    'Admin added' => 'Добавлен админ',
    'Admin ([[admin]]) has been added' => 'Добавлен новый админ "[[admin]]"',
    '[[username]] kicked player [[player]] ([[steam]]) from [[server]].' => '[[username]] кикнул игрока "[[player]] ([[steam]])" с сервера [[server]].',
    'Player kicked' => 'Игрок кикнут',
    '[[username]] tried paste a ban, but doesn\'t have access.' => '[[username]] пытался разместить бан, не имея на это прав',
    '[[username]] tried to add a ban, but doesnt have access.' => '[[username]] пытался добавить бан, не имея на это прав',
    'Ban Added' => 'Бан добавлен',
    'Ban against ([[banid]]) has been added, reason: [[reason]], length: [[length]]' => '"[[banid]]" забанен. Причина: [[reason]], срок: [[length]]',
    '[[address]] tried to change a password that doesn\'t have permissions.' => 'Клиент с адреса [[address]] пытался сменить пароль, не имея на это прав',
    'Password Changed' => 'Пароль сменен',
    'Password changed for admin ([[admin]])' => 'Пароль для админа "[[admin]]" был успешно изменен',
    'Mod Added' => 'Добавлен мод',
    'Mod ([[mod]]) has been added' => 'Мод "[[mod]]" был успешно добавлен',
    '[[username]] tried to gain OWNER admin permissions, but doesnt have access.' => '',
    '[[username]] tried to edit admin permissions, but doesnt have access.' => '',
    'Permissions Changed' => '',
    'Permissions have been changed for ([[admin]])' => '',
    '[[username]] tried to edit group details, but doesnt have access.' => '',
    '[[username]] tried to set group\'s name to nothing. This isn\'t possible with the normal form.' => '',
    'Group Updated' => '',
    'Group ([[group]]) has been updated' => '',
    '[[username]] tried to send an rcon command, but doesnt have access.' => '[[username]] пытался отправить RCON команду, не имея на это прав',
    'RCON Sent' => 'RCON команда отправлена',
    'RCON Command was sent to server ([[server]]): [[command]]' => 'На сервер "[[server]]" была отправлена команда: [[command]]',
    '[[username]] tried to send an email, but doesnt have access.' => '[[username]] пытался отправить E-mail, не имея на это прав',
    'Email Sent' => 'Email отправлен',
    '[[username]] send an email to [[email]].<br />Subject: [SourceBans] [[subject]]<br />Message: [[message]]' => '[[username]] отправил сообщение на E-mail: [[email]].<br />Тема: [SourceBans] [[subject]]<br />Сообщение: [[message]]',
    '[[username]] tried to execute SelTheme() function, but doesnt have access.' => '[[username]] пытался выполнить функцию SelTheme(), не имея на это прав',
    '[[username]] tried to change the theme to [[theme]], but doesnt have access.' => '[[username]] пытался сменить тему сайта на "[[theme]]", не имея на это прав',
    '[[username]] tried to add a comment, but doesnt have access.' => '[[username]] пытался добавить комментарий, не имея на это прав',
    'Comment Added' => 'Комментарий добавлен',
    '[[username]] added a comment for ban #[[banid]]' => '[[username]] добавил коментарий для бана #[[banid]]',
    'Comment Edited' => 'Отредактирован комментарий',
    '[[username]] tried to remove a comment, but doesnt have access.' => '[[username]] пытался удалить комментарий, не имея на это прав',
    'Comment Deleted' => 'Комментарий удален',
    '[[username]] deleted comment #[[commentid]]' => '[[username]] удалил комментарий №[[commentid]]',
    'There was a problem deleting the comment from the database. Check the logs for more info' => 'Произошла ошибка при удалении комментария из базы. Проверьте логи',
    '[[username]] tried to clear the cache, but doesnt have access.' => '[[username]] пытался очистить кэш, не имея на это прав',
    '[[username]] tried to rehash admins, but doesnt have access.' => '[[username]] пытался обновить список админов, не имея на это прав',
    '[[username]] tried to initiate a groupban "[[group]]".' => '[[username]] пытался инициализировать бан группы "[[group]]", не имея на это прав',
    'Banned [[totalbanned]]/[[total]] players of group "[[group]]". [[bannedbefore]] were banned already. [[errors]] failed.' => 'Забанено [[totalbanned]] из [[total]] участников группы "[[group]]". [[bannedbefore]] участников уже были забанены ранее. [[errors]] участников не забанено (ошибка)',
    '[[username]] tried to ban friends of "[[friendid]]", but doesnt have access.' => '[[username]] пытался забанить зрузей игрока "[[friendid]]", не имея на это прав',
    'Friends Banned' => 'Друзья забанены',
    'Banned [[banned]]/[[total]] friends of "[[name]]".<br>[[bannedbefore]] were banned already.' => 'Забанено [[banned]] из [[total]] друзей игрока "[[name]]".<br>[[bannedbefore]] его друзей уже были забанены ранее.',
    '[[username]] tried to send ingame message to  "[[player]]" (Message: "[[message]]"), but doesnt have access.' => '[[username]] пытался отправить сообщение в игроу для игрока "[[player]]", не имея на это прав. Сообщение: [[message]]',
    'Message sent to player' => 'Сообщение отправлено игроку',
    'The following message was sent to [[player]] on server [[server]]. Message: [[message]]' => 'Отправлено сообщение для игрока "[[player]]" на сервер [[server]]. Сообщение: [[message]]',
    '[[username]] tried to kick [[player]], but doesnt have access.' => '[[username]] пытался кикнуть игрока "[[player]]", не имея на это прав',
    'E-mail Changed' => 'E-mail изменен',
    'E-mail changed for admin ([[adminid]])' => 'E-mail был изменен для админа [[adminid]]',
    '[[username]] tried to remove a protest, but doesnt have access.' => '[[username]] пытался удалить протест бана, не имея на это прав',
    '[[username]] tried to add a mod, but doesnt have access.' => '[[username]] пытался удалить протест бана, не имея на это прав',
    '[[username]] tried to ban group "[[group]]", but doesnt have access.' => '[[username]] пытался забанить участников группы "[[group]]", не имея на это прав',
    '[[username]] tried to list groups of "[[group]]", but doesnt have access.' => '[[username]] пытался получить участников группы "[[group]]", не имея на это прав',
    
    'Bans imported' => 'Импортированы баны',
    '[[bancnt]] Ban(s) imported' => 'Импортировано [[bancnt]] банов',
    
    // Edit admin details
    'Can\'t find data for admin with id "[[admin]]"' => 'Ошибка получения данных админа с ID "[[admin]]"',
    '[[user]] tried to edit [[admin]]\'s details, but doesnt have access.' => '[[user]] пытался редактировать детали админа [[admin]], не имея на это прав.',
    'Admin Details Updated' => 'Редактирован админ',
    'Admin ([[adminname]]) details has been changed' => 'Редактированы детали админа [[adminname]]',
    
    // Edit admin groups
    '[[user]] tried to edit [[admin]]\'s groups, but doesnt have access.' => '[[user]] пытался редактировать группы админа [[admin]], не имея на это прав.',
    'Admin ([[adminname]]) groups has been updated' => 'Редактированы группы админа [[adminname]]',
    'Admin\'s Groups Updated' => 'Редактированы группы админа',
    '[[user]] tried to edit [[admin]]\'s permissions, but doesnt have access.' => '[[user]] пытался редактировать разрешения админа [[admin]], не имея на это прав.',
    
    // Edit ban
    'Ban length edited' => 'Изменен срок бана',
    
    // Edit mod
    '[[user]] tried to edit a mod, but doesnt have access.' => '[[user]] пытался отредактировать мод, не имея на это прав',
    
    // Edit server
    '[[user]] tried to edit a server, but doesnt have access.' => '[[user]] пытался отредактировать сервер, не имея на это прав',
    'Getting server data failed' => 'Ошибка получения информации',
    'Can\'t find data for server with id "[[serverid]]"' => 'Сервер с ID "[[serverid]]" не найден',
    'Server updated' => 'Сервер обновлен',
    '[[user]] updated a server id [[serverid]]' => '[[user]] отредактировал сервер с ID [[serverid]]',
    
    // Kickit
    '[[user]] tried to use kickit, but doesnt have access.' => '[[user]] пытался кикнуть, не имея на это прав',
    'Can not connect to server. Error: [[errormessage]]' => 'Нет соединения с сервером. Ошибка: [[errormessage]]',
    'Can not connect to server' => 'Нет соединения с сервером.',
    
    // Search log
    '<b>Advanced Search</b> (Click)' => '<b>Расширенный поиск</b>',
    'Admin' => 'Админ',
    'Message' => 'Сообщение',
    'Date' => 'Дата',
    'DD' => 'ДД',
    'MM' => 'ММ',
    'YYYY' => 'ГГГГ',
    'Type' => 'Тип',
    'Message' => 'Сообщение',
    'Warning' => 'Внимание',
    'Error' => 'Ошибка',
    'Search' => 'Искать',
    
    // settings
    '[[user]] tried to clear the logs, but doesn\'t have access.' => '[[user]] пытался очистить системный лог, не имея на это прав',
    
    //admin.uploaddemo.php
    '[[user]] tried to upload a demo, but doesn\'t have access.' => '[[user]] пытался загрузить демо, не имея на это прав',
    'Demo Uploaded' => 'Демо загружено',
    'A new demo has been uploaded: [[demofile]]' => 'Новое демо загружено: [[demofile]]',
    // admin.uploadicon.php
    '[[user]] tried to upload a mod icon, but doesn\'t have access.' => '[[user]] пытался загрузить иконку мода, не имея на это прав',
    'Mod Icon Uploaded' => 'Загружена иконка мода',
    'A new mod icon has been uploaded: [[iconfile]]' => 'Загружена новая иконка мода: [[iconfile]]',
    // admin.uploadmapimg.php
    '[[user]] tried to upload a mapimage, but doesn\'t have access.' => '[[user]] пытался загрузить изображение карты, не имея на это прав',
    'Map Image Uploaded' => 'Загружено изображение карты',
    'A new map image has been uploaded: [[mapimagefile]]' => 'Загружено новое изображение карты: [[mapimagefile]]',
    
    // page.bans.php
    'Player Unbanned' => '',
    '[[playername]] ([[steam_or_ip]]) has been unbanned' => '',
    'Ban Deleted' => '',
    
    // page.submit.php
    'Demo Upload Failed' => '',
    'A demo failed to upload for a submission from ([[email]])' => '',
    
    // Mail send
    'Email was not sent' => 'Сообщение не отправлено',
    'An error occurred while sending an email to the address "[[address]]". Error: [[error]]' => 'Произошла ошибка при отправке сообщения на адрес(а) "[[address]]". Ошибка: [[error]]',
    'Email sent' => 'Отправлено сообщение',
    '<b>Transport:</b> [[transport]]<br /><b>Destination:</b> [[addresses]]<br /><b>Subject:</b> [[subject]]<br /><b>Message:</b> [[message]]<br /><b>Error:</b> [[error]]' => '<b>Метод отправки:</b> [[transport]]<br /><b>Адрес(а) назначения:</b> [[addresses]]<br /><b>Тема:</b> [[subject]]<br /><b>Сообщение:</b> [[message]]<br /><b>Ошибка:</b> [[error]]',
    
    
    'Admin Servers Updated' => 'Обновлены сервера админа',
    'Admin ([[admin]]) server access has been changed' => 'Для админа [[admin]] были обновлены серверы',
    
    'Language change' => 'Смена языка',
    'User tried a change client language, but this function is disabled' => 'Пользователь пытался сменить язык системы, но эта функция отключена',
    
    // Site links
    '[[username]] tried to create or edit a link, but doesnt have access.' => '',
    'New link created' => '',
    'Link updated' => '',
    '[[username]] has added a new link "[[linkLabel]]"' => '',
    '[[username]] has edited the link "[[linkLabel]]"' => '',
    
    'Group Banned' => 'Группа забанена',
    '' => '',
    '' => '',
);
