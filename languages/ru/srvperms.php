<?php
return array(
    'New Group Permissions' => 'Новая группа разрешений',
    'Server Permissions' => 'Серверные разрешения',
    
    'None' => 'Без разрешений',
    
    'Name' => 'Имя',
    'Flag' => 'Флаг',
    'Purpose' => 'Описание',
    
    'Full Admin' => 'Полные права',
    'Root Admin (Full Admin Access)' => 'Главный админ (Полные права)',
    'Magically enables all flags.' => 'Магически включает все флаги',
    
    'Standard Admin Server Permissions' => 'Стандартные разрешения',
    'Reserved slot' => 'Резервный слот',
    'Reserved slot access.' => 'Использование резервного слота',
    'Generic admin' => 'Доступ к меню (админ)',
    'Generic admin; required for admins.' => 'Этот флаг должен быть у всех админов',
    'Kick' => 'Кик',
    'Kick other players.' => 'Кикать других игроков',
    'Ban' => 'Бан',
    'Ban other players.' => 'Банить других игроков',
    'Unban' => 'Разбан',
    'Remove bans.' => 'Удалять/разбанивать баны',
    'Slay' => 'Убивать',
    'Slay/harm other players.' => 'Убивать/наносить урон другим игрокам',
    'Map change' => 'Смена карты',
    'Change the map or major gameplay features.' => 'Смена карты и изменение некоторых других характеристик геймплея',
    'Change cvars' => 'Настройка кваров',
    'Change most cvars.' => 'Изменение большинства серверных переменных',
    'Run configs' => 'Запускать конфиги',
    'Execute config files.' => 'Запускать конфигурационные файлы',
    'Admin chat' => 'Админский чат',
    'Special chat privileges.' => 'Специальные привилегии в чате',
    'Start votes' => 'Запуск голосований',
    'Start or create votes.' => 'Запуск/создание голосований',
    'Password server' => 'Пароль сервера',
    'Set a password on the server.' => 'Устанавливать пароль на сервер',
    'RCON' => 'Доступ к RCON',
    'Use RCON commands.' => 'Использование RCON управления',
    'Enable Cheats' => 'Управление sv_cheats',
    'Change sv_cheats or use cheating commands.' => 'Изменение sv_cheats или использование читерских команд',
    
    'Custom Admin Server Permissions' => 'Дополнительные флаги',
    'Custom flag 1' => 'Дополнительный флаг 1',
    'Custom flag 2' => 'Дополнительный флаг 2',
    'Custom flag 3' => 'Дополнительный флаг 3',
    'Custom flag 4' => 'Дополнительный флаг 4',
    'Custom flag 5' => 'Дополнительный флаг 5',
    'Custom flag 6' => 'Дополнительный флаг 6',
    
    'Immunity' => 'Иммунитет',
    'Choose the immunity level. The higher the number, the more immunity.' => 'Уровень иммунитета. Чем выше число, тем выше иммунитет',
);
