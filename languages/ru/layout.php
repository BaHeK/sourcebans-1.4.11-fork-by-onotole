<?php
/**
 * Main layout (header and footer)
 */
return array(
    // page_header.tpl
    'SourceBans Logo' => '',
    'Logout' => 'Выйти',
    'Your account' => 'Профиль',
    'Login' => 'Войти',
    
    // footer.php
    'Page took [[totaltime]] seconds to load.' => 'Время генерации страницы: [[totaltime]] секунд.',
    'User Manager Data' => '',
    'Post Data' => '',
    'Session Data' => '',
    'Cookie Data' => '',
    // page_footer.tpl
    'By <a href="http://www.gameconnect.net" target="_blank" class="footer_link">GameConnect.net</a>' => '',
    'Version [[sbVersion]] [[sbRev]]' => 'Версия [[sbVersion]] [[sbRev]]',
    'Welcome [[username]]' => 'Привет, [[username]]',
    
    'Search Bans...' => 'Поиск банов...',
    
    '<h1>The requested page was not found</h1>' => '<h1>Запрашиваемая страница не найдена</h1>',
    
    'Error' => 'Ошибка',
);
