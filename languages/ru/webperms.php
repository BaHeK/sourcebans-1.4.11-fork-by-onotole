<?php
return array(
    'Root Admin (Full Admin Access)' => 'Главный админ (Полный доступ)',
    
    'None' => 'Без разрешений',
    
    'New Group Permissions' => 'Новая группа разрешений',
    'Web Permissions' => 'WEB разрешения',
    
    'Manage Admins' => 'Управление админами',
    'View admins' => 'Просмотр админов',
    'Add admins' => 'Добавлять админа',
    'Edit admins' => 'Редактировать админов',
    'Delete admins' => 'Удалять админов',
    
    'Server Management' => 'Управление серверами',
    'View servers' => 'Просмотр серверов',
    'Add servers' => 'Добавлять серверы',
    'Edit servers' => 'Редактировать серверы',
    'Delete servers' => 'Удалять серверы',
    
    'Ban Management' => 'Управление банами',
    'Add bans' => 'Добавлять баны',
    'Edit own bans' => 'Редактировать свои баны',
    'Edit groups bans' => 'Редактировать групповые баны',
    'Edit all bans' => 'Редактировать все баны',
    'Ban protests' => 'Управление протестами',
    'Ban submissions' => 'Управление прпедложениями банов',
    'Unban own bans' => 'Разбанивать свои баны',
    'Unban group bans' => 'Разбанивать групповые баны',
    'Unban all bans' => 'Разбанивать все баны',
    'Delete All bans' => 'Удалять все баны',
    'Import bans' => 'Импортировать баны',
    
    'Groups Management' => 'Управление группами',
    'List groups' => 'Просмотр групп',
    'Add groups' => 'Добавлять группы',
    'Edit groups' => 'Редактировать группы',
    'Delete groups' => 'Удалять группы',
    
    'Email Notification' => 'E-mail уведомления',
    'Submission email notifying' => 'Email уведомления предложений банов',
    'Protest email notifying' => 'Email уведомления протестов банов',
    
    'Web settings' => 'Настройки сайта',
    
    'Manage Mods' => 'Управление модами',
    'View mods' => 'Просмотр модов',
    'Add mods' => 'Добавление модов',
    'Edit mods' => 'Редактировать моды',
    'Delete mods' => 'Удалять моды',
    'Owner' => 'Главный админ',
);
