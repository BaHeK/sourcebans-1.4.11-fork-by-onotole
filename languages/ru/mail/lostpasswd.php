<?php
return array(
    "Hello [[user]]" => "Привет, [[user]]",
    "You have requested to have your password reset for your SourceBans account." => "Вы запросили сброс пароля от аккаунта SourceBans",
    "To complete this process, please click the following link." => "Для совершения сброса пароля перейдите по ссылке ниже",
    "NOTE: If you didnt request this reset, then simply ignore this email." => "ВНИМАНИЕ: Если Вы не запрашивали сброс пароля, то просто проигнорируйте это письмо",
    "SourceBans Password Reset" => "Сброс пароля администратора SourceBans",
    'Check E-Mail' => 'Проверьте E-mail',
    'Please check your email inbox (and spam) for a link which will help you reset your password.' => 'На Ваш электронный почтовый ящик была отправлена ссылка для сброса пароля. Перейдите по этой ссылке для завершения операции.',
    
    'Your password reset was successful.' => 'Ваш пароль успешно изменен',
    'Your password was changed to: [[newpassword]]' => 'Ваш новый пароль: [[newpassword]]',
    'Login to your SourceBans account and change your password in Your Account.' => 'Вы можете войти в Ваш аккаунт с этим паролем, и, при необходимости, сменить его',
    'SourceBans Password Reset' => 'Сброс пароля SourceBans',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
);
