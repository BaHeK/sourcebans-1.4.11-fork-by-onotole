<?php

return array(
    // page.lostpassword.php
    'The validation string is too short.' => 'Проверочнаый код слишком короткий',
    'Password Reset' => 'Сброс пароля',
    'Your password has been reset and sent to your email.<br />Please check your spam folder too.<br />Please login using this password, <br />then use the change password link in Your Account.' => 'Ваш пароль был сброшен и отправлен на ваш E-mail<br />Проверьте письмо так же в папке со спамом',
    'Error' => 'Ошибка',
    'The validation string does not match the email for this reset request.' => 'Неверный проверочный код',
    
    'The email address you supplied is not registered on the system.' => 'Введенный вами E-mail отсутствует в базе',
    'Information' => 'Информация',
    'Please check your email inbox (and spam) for a link which will help you reset your password.' => 'Проверьте письмо в папке входящих сообщений (в папке Спам тоже). В письме будет содержаться ссылка, по которой вам нужно будет перейти, чтобы сбросить пароль',
    'Your E-Mail Address:' => 'Ваш E-mail адрес',
    'Please type your email address in the box below to have your password reset.' => 'Введите E-mail адрес, который указан в вашем аккаунте',
    'Ok' => 'Ок',
    'Please enter a valid E-mail address' => 'Введен неверный адрес E-mail',
    'Restore password' => 'Восстановить пароль',
    'Lost Password Form' => 'Форма восстановления пароля',
);
